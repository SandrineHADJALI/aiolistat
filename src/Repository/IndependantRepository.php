<?php

namespace App\Repository;

use App\Entity\Independant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Independant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Independant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Independant[]    findAll()
 * @method Independant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IndependantRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Independant::class);
    }

    public function findArray($id)
    {
        $qb = $this->createQueryBuilder('i')
            ->where('i.id = :id')
            ->setParameter('id',$id);

        $query =$qb->getQuery();
        return $query->getArrayResult();

    }

    public function findAllArray()
    {
        $qb = $this->createQueryBuilder('i')
        ->orderBy('i.prenom');
        $query =$qb->getQuery();
        return $query->getArrayResult();

    }


    public function  findByFiltres($filtres,$dateDebut,$dateFin)
    {
        $qb = $this->createQueryBuilder('i')
            ->innerJoin('i.heures','h')
            ->leftJoin('h.sousProjet','sp')
            ->leftJoin('sp.categorie','c')
            ->where('i.id = :idInde')
            ->setParameter('idInde',$filtres["independant"]);


        //filtre date
        $qb->andWhere('sp.dateAction >= :dateDebut')
            ->andWhere('sp.dateAction <= :dateFin')
            ->setParameter('dateDebut',$dateDebut)
            ->setParameter('dateFin',$dateFin);

        //gestion periodicite
        $perio = $filtres["perio"];

        switch ($perio){
            case 'MENSUEL':
            case 'ONESHOT':
                $qb->andWhere('sp.periodicite = :perio')
                    ->setParameter('perio',$perio);
                break;
        }

        //ajout filtre mot cle
        if (isset($filtres["mot-cle"]))
        {
            $mots = explode(' ',$filtres['mot-cle']);
            foreach ($mots as $mot)
            {
                $qb->andWhere('sp.designation LIKE :mot')
                    ->setParameter('mot','%'.$mot.'%');
            }
        }

        //ajout filtre categorie
        if (isset($filtres["categorie"]))
        {
            $qb->andWhere('c.id = :idCate')
                ->setParameter('idCate',$filtres["categorie"]);
        }


        //filtre previsionnel
        $previ = $filtres["previsionnel"];
        if ($previ==='non-previ')
        {
            $qb->andWhere('sp.isPrevisionnel = false');
        }else if ($previ==='previ')
        {
            $qb->andWhere('sp.isPrevisionnel = true');
        }



        $qb->addSelect('h');
        $query = $qb->getQuery();
        return $query->getResult();



    }
}
