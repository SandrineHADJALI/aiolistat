<?php

namespace App\Repository;

use App\Entity\Heure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Heure|null find($id, $lockMode = null, $lockVersion = null)
 * @method Heure|null findOneBy(array $criteria, array $orderBy = null)
 * @method Heure[]    findAll()
 * @method Heure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HeureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Heure::class);
    }

    /**Retourne une heure en tableau en fonction de son id
     * @param $id
     * @return array|int|string
     */
    public function findInArray($id)
    {
        $qb = $this->createQueryBuilder('h')
            ->innerJoin('h.independant','i')
            ->innerJoin('h.sousProjet','spj')
            ->where('h.id = :id')
            ->setParameter('id',$id)
            ->addSelect('i','spj');

        $query = $qb->getQuery();
        return $query->getArrayResult();
    }
}
