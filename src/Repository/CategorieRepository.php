<?php

namespace App\Repository;

use App\Entity\Categorie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Categorie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categorie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categorie[]    findAll()
 * @method Categorie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategorieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Categorie::class);
    }

  public function findAllInArray()
  {
      $qb = $this->createQueryBuilder('c');
      $query =$qb->getQuery();
      return $query->getArrayResult();
  }

    public function nameCategorie($idCategorie)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT c.libelle FROM categorie c WHERE c.id = :idCategorie';
        $requetePreparee = $conn->prepare($sql);
        $requetePreparee->execute(['idCategorie' => $idCategorie]);
        return $requetePreparee->fetchAll();

    }

}
