<?php

namespace App\Repository;

use App\Entity\AutrePrestation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AutrePrestation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AutrePrestation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AutrePrestation[]    findAll()
 * @method AutrePrestation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AutrePrestationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AutrePrestation::class);
    }

    /**
     * @param $id
     * @return array|int|string
     */
    public function findArray($id)
    {
        $qb = $this->createQueryBuilder('ap')
            ->where('ap.id=:id')
            ->setParameter('id',$id);
        $query = $qb->getQuery();
        return $query->getArrayResult();
    }
}
