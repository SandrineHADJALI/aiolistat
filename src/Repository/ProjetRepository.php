<?php

namespace App\Repository;

use App\Entity\Projet;
use App\Entity\SousProjet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method Projet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Projet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Projet[]    findAll()
 * @method Projet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjetRepository extends ServiceEntityRepository
{
    /**
     * ProjetRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Projet::class);
    }

    /**
     * @param $periodicite ('ONE SHOT' ou 'MENSUEL')
     * @param $typeRetour ('array'=>getArrayResult ou 'objet'=>getResult
     * @return array|int|mixed|string
     */
    public function findByPeriodicite($periodicite,$typeRetour)
    {


        $qb = $this->createQueryBuilder('p')
            ->innerJoin('p.sousProjets','sp')
            ->addSelect('sp')
            ->where('sp.periodicite = :periodicite')
            ->setParameter('periodicite',$periodicite);

        $query = $qb->getQuery();

        if ($typeRetour === 'array')
        {
           return $query->getArrayResult();
        }elseif ($typeRetour === 'objet')
        {
            return $query->getResult();

        }
    }


    public function findOneInArray($id)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.id = :id')
            ->setParameter('id',$id);
        $query=$qb->getQuery();
        return $query->getArrayResult();
    }


    public function findOneByPeriodicite($id,$perio)
    {
        $qb=$this->createQueryBuilder('p')
            ->innerJoin('p.sousProjets','sp')
            ->where('p.id = :id')
            ->setParameter('id',$id)
            ->andWhere('sp.periodicite = :perio')
            ->setParameter('perio',$perio)
            ->addSelect('sp');
        $query = $qb->getQuery();

        return $query->getArrayResult();
    }



    public function findByFiltres($filtres, $dateDebut,$dateFin,$perio)
    {


        $qb=$this->createQueryBuilder('p')
            ->innerJoin('p.sousProjets','sp')
            ->addSelect('sp')
            ->orderBy('p.nom','DESC');

        //filtre date
        $qb->where('sp.dateAction >= :dateDebut')
            ->andWhere('sp.dateAction <= :dateFin')
            ->setParameter('dateDebut',$dateDebut)
            ->setParameter('dateFin',$dateFin);

        //gestion periodicite
        if ($perio!=='VIDE')
        {
            $qb->andWhere('sp.periodicite = :perio')
                ->setParameter('perio',$perio);
            //ajout filtre mot cle
            if (isset($filtres["mot-cle"]))
            {
                $mots = explode(' ',$filtres['mot-cle']);
                foreach ($mots as $mot)
                {
                    $qb->andWhere('p.nom LIKE :mot OR sp.designation LIKE :mot')

                        ->setParameter('mot','%'.$mot.'%');
                }
            }

            //ajout filtre categorie
            if (isset($filtres["categorie"]))
            {
                $qb->innerJoin('sp.categorie','c')
                    ->andWhere('c.id = :idCate')
                    ->setParameter('idCate',$filtres["categorie"]);
            }

            //ajout filtre independant
            if (isset($filtres["independant"]))
            {
                $qb->innerJoin('sp.heures','h')
                    ->innerJoin('h.independant','i')
                    ->andWhere('i.id = :idInde')
                    ->setParameter('idInde',$filtres["independant"]);
            }

            //filtre previsionnel
            $previ = $filtres["previsionnel"];
            if ($previ==='non-previ')
            {
                $qb->andWhere('sp.isPrevisionnel = false');
            }else if ($previ==='previ')
            {
                $qb->andWhere('sp.isPrevisionnel = true');
            }
        }


        $query = $qb->getQuery();
        return $query->getResult();

    }





    public function findOneByFiltres($filtres, $dateDebut,$dateFin,$perio,$idProjet)
    {


        $qb=$this->createQueryBuilder('p')
            ->andWhere('p.id = :idProjet')
            ->setParameter('idProjet',$idProjet)
            ->innerJoin('p.sousProjets','sp')
            ->addSelect('sp');

        //filtre date
        $qb->andwhere('sp.dateAction >= :dateDebut')
            ->andWhere('sp.dateAction <= :dateFin')
            ->setParameter('dateDebut',$dateDebut)
            ->setParameter('dateFin',$dateFin);

        //gestion periodicite
        if ($perio!=='VIDE')
        {
            $qb->andWhere('sp.periodicite = :perio')
                ->setParameter('perio',$perio);
            //ajout filtre mot cle
            if (isset($filtres["mot-cle"]))
            {
                $mots = explode(' ',$filtres['mot-cle']);
                foreach ($mots as $mot)
                {
                    $qb->andWhere('p.nom LIKE :mot OR sp.designation LIKE :mot')
                        ->setParameter('mot','%'.$mot.'%');
                }
            }

            //ajout filtre categorie
            if (isset($filtres["categorie"]))
            {
                $qb->innerJoin('sp.categorie','c')
                    ->andWhere('c.id = :idCate')
                    ->setParameter('idCate',$filtres["categorie"]);
            }

            //ajout filtre independant
            if (isset($filtres["independant"]))
            {
                $qb->innerJoin('sp.heures','h')
                    ->innerJoin('h.independant','i')
                    ->andWhere('i.id = :idInde')
                    ->setParameter('idInde',$filtres["independant"]);
            }

            //filtre previsionnel
            $previ = $filtres["previsionnel"];
            if ($previ==='non-previ')
            {
                $qb->andWhere('sp.isPrevisionnel = false');
            }else if ($previ==='previ')
            {
                $qb->andWhere('sp.isPrevisionnel = true');
            }
        }

        $query = $qb->getQuery();
        return $query->getResult();
    }
   
//fonction qui permet de retourner une SUM du prix de ventes des sous projet regrouper par  projet et par date action  en fonction d'une date de debut et d'une date de fin 
public function findProjet6mois($dateDebut,$dateFin)
{
    $conn = $this->getEntityManager()->getConnection();
    $sql = '
    SELECT p0_.id AS id, p0_.nom AS nom ,SUM( s1_.prix_vente) AS prixVente, s1_.cout AS cout, s1_.date_action AS date_action FROM projet p0_ INNER JOIN sous_projet s1_ ON p0_.id = s1_.projet_id WHERE s1_.date_action >= :dateDebut AND s1_.date_action <= :dateFin GROUP BY s1_.date_action, p0_.id ORDER BY p0_.nom ASC
    ';
    
    
       
    $stmt = $conn->prepare($sql);
    $stmt->execute(['dateDebut' =>$dateDebut,'dateFin' =>$dateFin]);
    

        // returns an array of arrays (i.e. a raw data set)
    return $stmt->fetchAll();
    
}
//fonction qui permet de retourner une SUM du prix de ventes des sous projet regrouper par  projet et par date action  en fonction d'une date de debut , d'une date de fin , et du filtre previsionnel et catégorie 
public function findProjetfilter($dateDebut,$dateFin,$categorie,$previsionel)
{
    $conn = $this->getEntityManager()->getConnection();
    $sql = '
    SELECT p0_.id AS id, p0_.nom AS nom ,SUM( s1_.prix_vente) AS prixVente, s1_.cout AS cout, s1_.date_action AS date_action FROM projet p0_ INNER JOIN sous_projet s1_ ON p0_.id = s1_.projet_id WHERE s1_.date_action >= :dateDebut AND s1_.date_action <= :dateFin  AND s1_.categorie_id= :categorie AND is_previsionnel= :previsionnel GROUP BY s1_.date_action, p0_.id ORDER BY p0_.nom ASC
    ';
    
       
    $stmt = $conn->prepare($sql);
    $stmt->execute(['dateDebut' =>$dateDebut,'dateFin' =>$dateFin,'categorie' =>$categorie,'previsionnel' =>$previsionel]);
   
        // returns an array of arrays (i.e. a raw data set)
    return $stmt->fetchAll();
    
}
//fonction qui permet de retourner une SUM du prix de ventes des sous projet regrouper par  projet et par date action  en fonction d'une date de debut ,d'une date de fin et du filtre categorie
public function findProjetfilterC($dateDebut,$dateFin,$categorie)
{
    $conn = $this->getEntityManager()->getConnection();
    $sql = '
    SELECT p0_.id AS id, p0_.nom AS nom ,SUM( s1_.prix_vente) AS prixVente, s1_.cout AS cout, s1_.date_action AS date_action FROM projet p0_ INNER JOIN sous_projet s1_ ON p0_.id = s1_.projet_id WHERE s1_.date_action >= :dateDebut AND s1_.date_action <= :dateFin  AND s1_.categorie_id= :categorie GROUP BY s1_.date_action, p0_.id ORDER BY p0_.nom ASC
    ';
    
       
    $stmt = $conn->prepare($sql);
    $stmt->execute(['dateDebut' =>$dateDebut,'dateFin' =>$dateFin,'categorie' =>$categorie]);
   
        // returns an array of arrays (i.e. a raw data set)
    return $stmt->fetchAll();
    
}
//fonction qui permet de retourner une SUM du prix de ventes des sous projet regrouper par  projet et par date action  en fonction d'une date de debut , d'une date de fin et du filtre previsionnel 
public function findProjetfilterP($dateDebut,$dateFin,$previsionel)
{
    $conn = $this->getEntityManager()->getConnection();
    $sql = '
    SELECT p0_.id AS id, p0_.nom AS nom ,SUM( s1_.prix_vente) AS prixVente, s1_.cout AS cout, s1_.date_action AS date_action FROM projet p0_ INNER JOIN sous_projet s1_ ON p0_.id = s1_.projet_id WHERE s1_.date_action >= :dateDebut AND s1_.date_action <= :dateFin  AND  is_previsionnel= :previsionnel GROUP BY s1_.date_action, p0_.id ORDER BY p0_.nom ASC
    ';
    
       
    $stmt = $conn->prepare($sql);
    $stmt->execute(['dateDebut' =>$dateDebut,'dateFin' =>$dateFin,'previsionnel' =>$previsionel]);
   
        // returns an array of arrays (i.e. a raw data set)
    return $stmt->fetchAll();
    
}

}
