<?php

namespace App\Repository;

use App\Entity\SousProjet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SousProjet|null find($id, $lockMode = null, $lockVersion = null)
 * @method SousProjet|null findOneBy(array $criteria, array $orderBy = null)
 * @method SousProjet[]    findAll()
 * @method SousProjet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SousProjetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SousProjet::class);
    }

    public function findInArray($idProjet, $type)
    {
        $qb = $this->createQueryBuilder('sp')
            ->where('sp.projet = :id')
            ->setParameter('id', $idProjet)
            ->andWhere('sp.periodicite = :type')
            ->setParameter('type', $type)
            ->innerJoin('sp.categorie', 'c')
            ->leftJoin('sp.autrePrestations', 'ap')
            ->leftJoin('sp.heures', 'h')
            ->leftJoin('h.independant', 'i')
            ->addSelect('c', 'ap', 'h', 'i')
            ->orderBy('sp.id', 'DESC');


        $query = $qb->getQuery();
        return $query->getArrayResult();
    }

    public function findOneInArray($id)
    {
        $qb = $this->createQueryBuilder('spj')
            ->andWhere('spj.id = :id')
            ->innerJoin('spj.categorie', 'c')
            ->leftJoin('spj.autrePrestations', 'ap')
            ->leftJoin('spj.heures', 'h')
            ->leftJoin('h.independant', 'i')
            ->addSelect('c', 'h', 'ap', 'i')
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        return $query->getArrayResult();
    }

    public function findByFiltre($idProjet, $perio, $filtres, $dateDebut, $dateFin)
    {

        $qb = $this->createQueryBuilder('sp')
            ->where('sp.projet = :id')
            ->setParameter('id', $idProjet)
            ->innerJoin('sp.categorie', 'c')
            ->leftJoin('sp.autrePrestations', 'ap')
            ->leftJoin('sp.heures', 'h')
            ->leftJoin('h.independant', 'i')
            ->addSelect('c', 'ap', 'h', 'i')
            ->orderBy('sp.dateAction', 'DESC');


        //filtre date
        $qb->andWhere('sp.dateAction >= :dateDebut')
            ->andWhere('sp.dateAction <= :dateFin')
            ->setParameter('dateDebut', $dateDebut)
            ->setParameter('dateFin', $dateFin);

        //gestion periodicite
        $qb->andWhere('sp.periodicite = :perio')
            ->setParameter('perio', $perio);

        //ajout filtre categorie
        if (isset($filtres["categorie"])) {
            $qb->andWhere('c.id = :idCate')
                ->setParameter('idCate', $filtres["categorie"]);
        }

        //filtre previsionnel
        $previ = $filtres["previsionnel"];
        if ($previ === 'non-previ') {
            $qb->andWhere('sp.isPrevisionnel = false');
        } else if ($previ === 'previ') {
            $qb->andWhere('sp.isPrevisionnel = true');
        }

        $query = $qb->getQuery();
        return $query->getArrayResult();
    }


    public function findAllByFiltre($perio, $filtres, $dateDebut, $dateFin)
    {

        $qb = $this->createQueryBuilder('sp')
            ->innerJoin('sp.projet', 'p')
            ->innerJoin('sp.categorie', 'c')
            ->leftJoin('sp.autrePrestations', 'ap')
            ->leftJoin('sp.heures', 'h')
            ->leftJoin('h.independant', 'i')
            ->addSelect('c', 'p');


        //filtre date
        $qb->andWhere('sp.dateAction >= :dateDebut')
            ->andWhere('sp.dateAction <= :dateFin')
            ->setParameter('dateDebut', $dateDebut)
            ->setParameter('dateFin', $dateFin);

        //gestion periodicite

        $qb->andWhere('sp.periodicite = :perio')
            ->setParameter('perio', $perio);

        if (isset($filtres["independant"])) {
            $qb->andWhere('i.id = :idInde')
                ->setParameter('idInde', $filtres["independant"]);
        }

        //ajout filtre categorie
        if (isset($filtres["categorie"])) {
            $qb->andWhere('c.id = :idCate')
                ->setParameter('idCate', $filtres["categorie"]);
        }

        //ajout filtre mot cle
        if (isset($filtres["mot-cle"])) {
            $mots = explode(' ', $filtres['mot-cle']);
            foreach ($mots as $mot) {
                $qb->andWhere('p.nom LIKE :mot OR sp.designation LIKE :mot')
                    ->setParameter('mot', '%' . $mot . '%');
            }
        }
        //filtre previsionnel
        $previ = $filtres["previsionnel"];
        if ($previ === 'non-previ') {
            $qb->andWhere('sp.isPrevisionnel = false');
        } else if ($previ === 'previ') {
            $qb->andWhere('sp.isPrevisionnel = true');
        }
        $query = $qb->getQuery();
        return $query->getArrayResult();
    }

//retourne le total de prix de vente et de cout du mois dans une intervalle de date 
    public function findTotalmois($dateDebut, $dateFin)
    {

        $qb = $this->createQueryBuilder('sp')
            ->select('SUM(sp.prixVente) as prixVente,SUM(sp.cout) as cout,sp.dateAction')
            ->groupBy('sp.dateAction');


        //filtre date
        $qb->andWhere('sp.dateAction >= :dateDebut')
            ->andWhere('sp.dateAction <= :dateFin')
            ->setParameter('dateDebut', $dateDebut)
            ->setParameter('dateFin', $dateFin);


        $query = $qb->getQuery();
        return $query->getResult();
    }

//retourne le total de prix de vente et de cout du mois dans une intervalle de date et de la categorie
    public function findTotalmoisC($dateDebut, $dateFin, $categorie)
    {

        $qb = $this->createQueryBuilder('sp')
            ->select('SUM(sp.prixVente) as prixVente,SUM(sp.cout) as cout,sp.dateAction')
            ->innerJoin('sp.categorie', 'c')
            ->groupBy('sp.dateAction');


        //filtre date
        $qb->andWhere('sp.dateAction >= :dateDebut')
            ->andWhere('sp.dateAction <= :dateFin')
            ->setParameter('dateDebut', $dateDebut)
            ->setParameter('dateFin', $dateFin);

        $qb->andWhere('c.id = :idCate')
            ->setParameter('idCate', $categorie);

        $query = $qb->getQuery();
        return $query->getResult();
    }

//retourne le total de prix de vente et de cout du mois dans une intervalle de date et du filtre previsionel
    public function findTotalmoisP($dateDebut, $dateFin, $previsionel)
    {

        $qb = $this->createQueryBuilder('sp')
            ->select('SUM(sp.prixVente) as prixVente,SUM(sp.cout) as cout,sp.dateAction')
            ->groupBy('sp.dateAction');


        //filtre date
        $qb->andWhere('sp.dateAction >= :dateDebut')
            ->andWhere('sp.dateAction <= :dateFin')
            ->setParameter('dateDebut', $dateDebut)
            ->setParameter('dateFin', $dateFin);


        if ($previsionel === 0) {
            $qb->andWhere('sp.isPrevisionnel = false');
        } else {
            $qb->andWhere('sp.isPrevisionnel = true');
        }


        $query = $qb->getQuery();
        return $query->getResult();
    }

//retourne le total de prix de vente et de cout du mois dans une intervalle de date et des filtre prévisionel et categorie
    public function findTotalall($dateDebut, $dateFin, $categorie, $previsionel)
    {

        $qb = $this->createQueryBuilder('sp')
            ->select('SUM(sp.prixVente) as prixVente,SUM(sp.cout) as cout,sp.dateAction')
            ->innerJoin('sp.categorie', 'c')
            ->groupBy('sp.dateAction');


        //filtre date
        $qb->andWhere('sp.dateAction >= :dateDebut')
            ->andWhere('sp.dateAction <= :dateFin')
            ->setParameter('dateDebut', $dateDebut)
            ->setParameter('dateFin', $dateFin);

        $qb->andWhere('c.id = :idCate')
            ->setParameter('idCate', $categorie);


        if ($previsionel === 0) {
            $qb->andWhere('sp.isPrevisionnel = false');
        } else {
            $qb->andWhere('sp.isPrevisionnel = true');
        }


        $query = $qb->getQuery();
        return $query->getResult();
    }

    //Statistiques : Requete SQL pour graphiques piechart par categorie
    public function totalVenteCategorie($categorie, $dateDebut, $dateFin, $previsionnel, $periodicite)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(s.prix_vente) as prix_vente
                FROM sous_projet s
                WHERE s.categorie_id = :notreCategorie
                AND s.date_action>= :notreDateDebut
                AND s.date_action <= :notreDateFin
                AND s.is_previsionnel = :notrePrevisionnel
                AND s.periodicite = :notrePeriodicite';
        $requetePreparee = $conn->prepare($sql);
        $requetePreparee->execute(['notreCategorie' => $categorie,
            'notreDateDebut' => $dateDebut,
            'notreDateFin' => $dateFin,
            'notrePrevisionnel' => $previsionnel,
            'notrePeriodicite' => $periodicite]);
        return $requetePreparee->fetchAll();

    }

    //Statistiques : Requete SQL pour graphiques Recuperer la liste des périodicités
    public function listePeriodicite()
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT DISTINCT s.periodicite
                FROM sous_projet s';

        $requetePreparee = $conn->executeQuery($sql);
        return $requetePreparee->fetchAll();
    }

    //Statistiques : Requete SQL pour graphiques piechart toutes les categories
    public function totalVenteCategorieVenduEtPrevi($categorie, $dateDebut, $dateFin)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(s.prix_vente) as prix_vente
                FROM sous_projet s
                WHERE s.categorie_id = :notreCategorie
                AND s.date_action>= :notreDateDebut
                AND s.date_action <= :notreDateFin';

        $requetePreparee = $conn->prepare($sql);
        $requetePreparee->execute(['notreCategorie' => $categorie,
            'notreDateDebut' => $dateDebut,
            'notreDateFin' => $dateFin]);
        return $requetePreparee->fetchAll();
    }

    //Statistiques : Requete SQL pour graphiques piechart toutes les categories
    public function totalVenteCategorieVendusSeulOuPreviSeul($categorie, $dateDebut, $dateFin, $previsionnel)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(s.prix_vente) as prix_vente
                FROM sous_projet s
                WHERE s.categorie_id = :notreCategorie
                AND s.date_action>= :notreDateDebut
                AND s.date_action <= :notreDateFin
                AND s.is_previsionnel = :notrePrevisionnel'
        ;

        $requetePreparee = $conn->prepare($sql);
        $requetePreparee->execute(['notreCategorie' => $categorie,
            'notreDateDebut' => $dateDebut,
            'notreDateFin' => $dateFin,
            'notrePrevisionnel' => $previsionnel]);

        return $requetePreparee->fetchAll();
    }

    //Statistiques : Requete SQL pour graphiques columnchart toutes les categories,projets vendus et previsionnels
    //selon dates et périodicité
    public function totalVenteEtCout($dateDebut, $dateFin,$periodicite)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(s.prix_vente) as prixVente, SUM(s.cout) as cout
                FROM sous_projet s
                WHERE s.date_action>= :notreDateDebut
                AND s.date_action <= :notreDateFin
                AND s.periodicite = :notrePeriodicite'
        ;

        $requetePreparee = $conn->prepare($sql);
        $requetePreparee->execute([
            'notreDateDebut' => $dateDebut,
            'notreDateFin' => $dateFin,
            'notrePeriodicite' => $periodicite]);

        return $requetePreparee->fetchAll();
    }

    //Statistiques : Requete SQL pour graphiques columnchart toutes les categories,projets vendus seuls
    //selon dates et périodicité
    public function totalVenteEtCoutPV($dateDebut, $dateFin,$periodicite)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(s.prix_vente) as prixVente, SUM(s.cout) as cout
                FROM sous_projet s
                WHERE s.date_action>= :notreDateDebut
                AND s.date_action <= :notreDateFin
                AND s.periodicite = :notrePeriodicite
                AND s.is_previsionnel = 0'
        ;

        $requetePreparee = $conn->prepare($sql);
        $requetePreparee->execute([
            'notreDateDebut' => $dateDebut,
            'notreDateFin' => $dateFin,
            'notrePeriodicite' => $periodicite]);

        return $requetePreparee->fetchAll();
    }

    //Statistiques : Requete SQL pour graphiques columnchart toutes les categories,projets previsionnels seuls
    //selon dates et périodicité
    public function totalVenteEtCoutPrevisionnel($dateDebut, $dateFin,$periodicite)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(s.prix_vente) as prixVente, SUM(s.cout) as cout
                FROM sous_projet s
                WHERE s.date_action>= :notreDateDebut
                AND s.date_action <= :notreDateFin
                AND s.periodicite = :notrePeriodicite
                AND s.is_previsionnel = 1'
        ;

        $requetePreparee = $conn->prepare($sql);
        $requetePreparee->execute([
            'notreDateDebut' => $dateDebut,
            'notreDateFin' => $dateFin,
            'notrePeriodicite' => $periodicite]);

        return $requetePreparee->fetchAll();
    }
}
