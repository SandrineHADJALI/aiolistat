<?php

namespace App\Entity;

use App\Repository\HeureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=HeureRepository::class)
 */
class Heure
{
    /**
     * @Groups("heure")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("heure")
     * @ORM\Column(type="float")
     * @Assert\Positive(message="Le nombre d'heures doit être positif")
     */
    private $heure;

    /**
     * @Groups("sous_projet")
     * @ORM\ManyToOne(targetEntity=SousProjet::class, inversedBy="heures", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="L'heure doit être associée à un sous-projet")
     */
    private $sousProjet;

    /**
     * @Groups("independant")
     * @ORM\ManyToOne(targetEntity=Independant::class, inversedBy="heures", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="L'heure doit être associée à un indépendant")
     */
    private $independant;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $cout;

    /** @Groups("heure")
     * @ORM\Column (type="string", length=255, nullable=true)
     */
    private $commentaire;




    public function __construct()
    {
        $this->sousProjet = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHeure(): ?float
    {
        return $this->heure;
    }

    public function setHeure(float $heure): self
    {
        $this->heure = $heure;

        return $this;
    }

    public function getSousProjet(): ?SousProjet
    {
        return $this->sousProjet;
    }

    public function setSousProjet(?SousProjet $sousProjet): self
    {
        $this->sousProjet = $sousProjet;
        $sousProjet->addHeure($this);
        return $this;
    }

    public function getIndependant(): ?Independant
    {
        return $this->independant;
    }

    public function setIndependant(?Independant $independant): self
    {
        $this->independant = $independant;
        $independant->addHeure($this);
        return $this;
    }

    public function getCout(): ?float
    {
        return $this->cout;
    }

    public function setCout(?float $cout): self
    {
        $this->cout = $cout;

        return $this;
    }

    public function updateCout()
    {
        $this->cout= $this->heure*$this->getIndependant()->getSalaire();
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     */
    public function setCommentaire($commentaire): void
    {
        $this->commentaire = $commentaire;
    }


}
