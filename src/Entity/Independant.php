<?php

namespace App\Entity;

use App\Repository\IndependantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=IndependantRepository::class)
 * @UniqueEntity(fields={"nom","prenom"},message="Cet indépendant existe déjà")
 */
class Independant
{
    /**
     * @Groups("independant")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("independant")
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le nom doit être renseigné", normalizer="trim")
     */
    private $nom;

    /**
     * @Groups("independant")
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le prénom doit être renseigné",normalizer="trim")
     */
    private $prenom;

    /**
     * @Groups("independant")
     * @Assert\Positive()
     * @ORM\Column(type="float")
     * @Assert\Positive(message="Le salaire doit être positif")
     */
    private $salaire;

    /**
     * @Groups("heure")
     *
     * @ORM\OneToMany(targetEntity=Heure::class, mappedBy="independant", orphanRemoval=true)
     */
    private $heures;

    public function __construct()
    {
        $this->heures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getSalaire(): ?float
    {
        return $this->salaire;
    }

    public function setSalaire(float $salaire): self
    {
        $this->salaire = $salaire;

        return $this;
    }

    /**
     * @return Collection|Heure[]
     */
    public function getHeures(): Collection
    {
        return $this->heures;
    }

    public function addHeure(Heure $heure): self
    {
        if (!$this->heures->contains($heure)) {
            $this->heures[] = $heure;
            $heure->setIndependant($this);
        }

        return $this;
    }

    public function removeHeure(Heure $heure): self
    {
        if ($this->heures->contains($heure)) {
            $this->heures->removeElement($heure);
            // set the owning side to null (unless already changed)
            if ($heure->getIndependant() === $this) {
                $heure->setIndependant(null);
            }
        }

        return $this;
    }




}
