<?php

namespace App\Entity;

use App\Repository\SousProjetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SousProjetRepository::class)
 */
class SousProjet
{
    public static function getPeriodicity()
    {
        return ['ONESHOT','MENSUEL'];
    }

    /**
     * @Groups("sous_projet")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("sous_projet")
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="La désignation doit être renseignée",normalizer="trim")
     */
    private $designation;

    /**
     * @Groups("sous_projet")
     * @ORM\Column(type="float")
     * @Assert\PositiveOrZero(message="Le prix de vente doit être supérieur ou égal à 0")
     */
    private $prixVente;

    /**
     * @Groups("sous_projet")
     * @ORM\Column(type="float", nullable=true)
     * @Assert\PositiveOrZero(message="Le coût ne peut pas être négatif")
     */
    private $cout;

    /**
     * @Groups("sous_projet")
     * @ORM\Column(type="date")
     */
    private $dateAction;

    /**
     * @Groups("categorie")
     * @ORM\ManyToOne(targetEntity=Categorie::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull(message="La catégorie doit être renseignée")
     */
    private $categorie;


    /**
     * @Groups("projet")
     * @ORM\ManyToOne(targetEntity=Projet::class, inversedBy="sousProjets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $projet;

    /**
     * @Groups("heure")
     * @ORM\OneToMany(targetEntity=Heure::class, mappedBy="sousProjet",cascade={"remove","persist"})
     */
    private $heures;

    /**
     * @Groups("sous_projet")
     * @Assert\Choice(callback="getPeriodicity")
     * @ORM\Column(type="string")
     * @Assert\NotNull(message="La périodicité doit être renseignée")
     */
    private $periodicite;

    /**
     * @ORM\OneToMany(targetEntity=AutrePrestation::class, mappedBy="sousProjet", cascade={"persist","remove"})
     */
    private $autrePrestations;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPrevisionnel;

    public function __construct()
    {
        $this->heures = new ArrayCollection();
        $this->autrePrestations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDesignation(): ?string
    {
        return $this->designation;
    }

    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    public function getPrixVente(): ?float
    {
        return $this->prixVente;
    }

    public function setPrixVente(float $prixVente): self
    {
        $this->prixVente = $prixVente;

        return $this;
    }

    public function getCout(): ?float
    {
        return $this->cout;
    }

    public function setCout(?float $cout): self
    {
        $this->cout = $cout;

        return $this;
    }

    public function getDateAction(): ?\DateTimeInterface
    {
        return $this->dateAction;
    }

    public function setDateAction(\DateTimeInterface $dateAction): self
    {
        $this->dateAction = $dateAction;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getProjet(): ?Projet
    {
        return $this->projet;
    }

    public function setProjet(?Projet $projet): self
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * @return Collection|Heure[]
     */
    public function getHeures(): Collection
    {
        return $this->heures;
    }

    public function addHeure(Heure $heure): self
    {
        if (!$this->heures->contains($heure)) {
            $this->heures->add($heure);
        }

        return $this;
    }



    public function removeHeure(Heure $heure): self
    {
        if ($this->heures->contains($heure)) {
            $this->heures->removeElement($heure);
        }
        return $this;
    }

    public function getPeriodicite()
    {
        return $this->periodicite;
    }

    public function setPeriodicite($periodicite): self
    {
        $this->periodicite = $periodicite;

        return $this;
    }

    /**
     * @return Collection|AutrePrestation[]
     */
    public function getAutrePrestations(): Collection
    {
        return $this->autrePrestations;
    }

    public function addAutrePrestation(AutrePrestation $autrePrestation): self
    {
        if (!$this->autrePrestations->contains($autrePrestation)) {
            $this->autrePrestations->add($autrePrestation);
        }

        return $this;
    }

    public function removeAutrePrestation(AutrePrestation $autrePrestation): self
    {
        if ($this->autrePrestations->contains($autrePrestation)) {
            $this->autrePrestations->removeElement($autrePrestation);
            // set the owning side to null (unless already changed)
            if ($autrePrestation->getSousProjet() === $this) {
                $autrePrestation->setSousProjet(null);
            }
        }

        return $this;
    }

    /**Retourne le ROI du sous-projet
     * @return float
     */
    public function getROI():float
    {

        if ($this->getCout()==0)
        {
            return 0;
        }else{
            return ($this->getCout()/$this->getPrixVente());
        }

    }


    public function updateCout()
    {
        $cout = 0;
        foreach ($this->getHeures() as $heure)
        {
            $cout = $cout + $heure->getCout();
        }
        foreach ($this->getAutrePrestations() as $prestation)
        {
            $cout = $cout +$prestation->getMontant();
        }
        $this->setCout($cout);
    }

    public function getIsPrevisionnel(): ?bool
    {
        return $this->isPrevisionnel;
    }

    public function setIsPrevisionnel(?bool $isPrevisionnel): self
    {
        $this->isPrevisionnel = $isPrevisionnel;

        return $this;
    }
}
