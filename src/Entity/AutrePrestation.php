<?php

namespace App\Entity;

use App\Repository\AutrePrestationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AutrePrestationRepository::class)
 */
class AutrePrestation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le détail de lla prestation doit être rennnsigné", normalizer="trim")
     */
    private $detail;

    /**
     * @ORM\Column(type="float")
     * @Assert\Positive(message="Le montant de la prestation doit être positif")
     */
    private $montant;

    /**
     * @ORM\ManyToOne(targetEntity=SousProjet::class, inversedBy="autrePrestations")
     */
    private $sousProjet;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id=$id;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDetail(string $detail): self
    {
        $this->detail = $detail;

        return $this;
    }

    public function getMontant(): ?float
    {
        return $this->montant;
    }

    public function setMontant(float $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getSousProjet(): ?SousProjet
    {
        return $this->sousProjet;
    }

    public function setSousProjet(?SousProjet $sousProjet): self
    {
        $this->sousProjet = $sousProjet;

        return $this;
    }
}
