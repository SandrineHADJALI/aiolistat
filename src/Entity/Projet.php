<?php

namespace App\Entity;

use App\Repository\ProjetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass=ProjetRepository::class)
 */
class Projet
{
    /**
     * @Groups("projet")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("projet")
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le nom doit être renseigné",normalizer="trim")
     */
    private $nom;

    /**
     * @Groups("sous_projet")
     * @ORM\OneToMany(targetEntity=SousProjet::class, mappedBy="projet", orphanRemoval=true)
     */
    private $sousProjets;


    public function __construct()
    {
        $this->sousProjets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|SousProjet[]
     */
    public function getSousProjets(): Collection
    {
        return $this->sousProjets;
    }

    public function addSousProjet(SousProjet $sousProjet): self
    {
        if (!$this->sousProjets->contains($sousProjet)) {
            $this->sousProjets->add($sousProjet);

        }

        return $this;
    }

    public function removeSousProjet(SousProjet $sousProjet): self
    {
        if ($this->sousProjets->contains($sousProjet)) {
            $this->sousProjets->removeElement($sousProjet);
            // set the owning side to null (unless already changed)
            if ($sousProjet->getProjet() === $this) {
                $sousProjet->setProjet(null);
            }
        }

        return $this;
    }

    /**
     * @return float
     */
    public function getRoi():float
    {
        $coutTotal = $this->getCoutTotal();
        $prixVente = $this->getPrixVente();
        if ($coutTotal==0 || $prixVente==0)
        {
            return 0;
        }else{
            return ($coutTotal/$prixVente);
        }
    }

    function getCoutTotal()
    {
        $coutTotal =0;
        foreach ($this->getSousProjets() as $sousProjet)
        {
            $coutTotal= $coutTotal + $sousProjet->getCout();
        }
        return $coutTotal;
    }


    /**
     * @return mixed
     */
    public function getPrixVente()
    {
        $pv = 0;
        foreach ($this->getSousProjets() as $sousProjet)
        {
            $pv =$pv + $sousProjet->getPrixVente();
        }
        return $pv;
    }
    

    public function getPrixVentePrevi()
    {
        $pv = 0;
        foreach ($this->getSousProjets() as $sousProjet)
        {
            if ($sousProjet->getIsPrevisionnel())
            {
                $pv =$pv + $sousProjet->getPrixVente();
            }

        }
        return $pv;
    }
    function getCoutTotalPrevi()
    {
        $coutTotal =0;
        foreach ($this->getSousProjets() as $sousProjet)
        {
            if ($sousProjet->getIsPrevisionnel())
            {
            $coutTotal= $coutTotal + $sousProjet->getCout();
            }
        }
        return $coutTotal;
    }

    public function getRoiPrevi():float
    {
        $coutTotal = $this->getCoutTotalPrevi();
        $prixVente = $this->getPrixVentePrevi();
        if ($coutTotal==0 || $prixVente==0)
        {
            return 0;
        }else{
            return ($coutTotal/$prixVente);
        }
    }

    public function getPrixVenteNonPrevi()
    {
        $pv = 0;
        foreach ($this->getSousProjets() as $sousProjet)
        {
            if (!$sousProjet->getIsPrevisionnel())
            {
                $pv =$pv + $sousProjet->getPrixVente();
            }

        }
        return $pv;
    }
    function getCoutTotalNonPrevi()
    {
        $coutTotal =0;
        foreach ($this->getSousProjets() as $sousProjet)
        {
            if (!$sousProjet->getIsPrevisionnel())
            {
                $coutTotal= $coutTotal + $sousProjet->getCout();
            }
        }
        return $coutTotal;
    }

    public function getRoiNonPrevi():float
    {
        $coutTotal = $this->getCoutTotalNonPrevi();
        $prixVente = $this->getPrixVenteNonPrevi();
        if ($coutTotal==0 || $prixVente==0)
        {
            return 0;
        }else{
            return ($coutTotal/$prixVente);
        }
    }


}
