<?php

namespace App\ChartService;

use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ColumnChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;

class ChartService
{


    public function CreerCategoriePieChart($categorie, $pvm, $pm, $pvo, $po): PieChart
    {
        $pieChart = new PieChart();

        $pieChart->getData()->setArrayToDataTable(
            [['type', 'Proportion'],
                ['PV MENSUEL', $pvm],
                ['PREVISIONNEL MENSUEL', $pm],
                ['PV ONESHOT', $pvo],
                ['PREVISIONNEL ONESHOT', $po]
            ]
        );
        $pieChart->getOptions()->setTitle($categorie . " : ");
        $pieChart->getOptions()->setHeight(250);
        $pieChart->getOptions()->setWidth(600);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(15);

        return $pieChart;
    }

    public function CreerToutesLesCategoriesPVEtPrevisionnelPieChart($SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers): PieChart
    {
        $pieChart = new PieChart();

        $pieChart->getData()->setArrayToDataTable(
            [['type', 'Proportion'],
                ['COMMUNICATION', $SommeComm],
                ['DEVELOPPEMENT', $SommeDev],
                ['PRESTATIONS DE CONSEIL', $SommeConseil],
                ['FORMATION', $SommeFormation],
                ['PRINT', $SommePrint],
                ['DIVERS', $SommeDivers]
            ]
        );
        $pieChart->getOptions()->setTitle("Toutes les catégories, Projets vendus et prévisionnels, MENSUEL et ONESHOT  : ");
        $pieChart->getOptions()->setHeight(250);
        $pieChart->getOptions()->setWidth(600);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(15);

        return $pieChart;
    }

    public function CreerToutesLesCategoriesVenduSeulOuPrevisionnelSeulPieChart($previsionnel, $SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers): PieChart
    {
        $pieChart = new PieChart();

        $pieChart->getData()->setArrayToDataTable(
            [['type', 'Proportion'],
                ['COMMUNICATION', $SommeComm],
                ['DEVELOPPEMENT', $SommeDev],
                ['PRESTATIONS DE CONSEIL', $SommeConseil],
                ['FORMATION', $SommeFormation],
                ['PRINT', $SommePrint],
                ['DIVERS', $SommeDivers]
            ]
        );


        if ($previsionnel=="0") {
            $namePrevisionnel = "Projets vendus";
        }else{
            $namePrevisionnel = "Projets prévisionnels";
        }

        $pieChart->getOptions()->setTitle("Toutes les catégories, $namePrevisionnel, MENSUEL et ONESHOT  : ");
        $pieChart->getOptions()->setHeight(250);
        $pieChart->getOptions()->setWidth(600);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(15);

        return $pieChart;
    }





    public function CreerVenteCoutColumnChartPV($totVenteMensuel,$totCoutMensuel,$roiMensuel,$totVenteOneshot,$totCoutOneshot,$roiOneshot,$totVente,$totCout, $roi, $is_previsionnel): ColumnChart
    {
        $columnChart = new ColumnChart();

            $columnChart->getData()->setArrayToDataTable([
                    ['Périodicité', 'Prix Vente', 'Coût'],
                    ['Mensuel ROI : '.$roiMensuel.'%', $totVenteMensuel, $totCoutMensuel],
                    ['Oneshot ROI : '.$roiOneshot.'%', $totVenteOneshot,$totCoutOneshot],
                    ['TOTAL ROI : '.$roi.'%',$totVente,$totCout]
            ]
        );

        if ($is_previsionnel=="0") {
            $columnChart->getOptions()->setTitle("Toutes catégories projets vendus seuls");
        }elseif($is_previsionnel=="1"){
            $columnChart->getOptions()->setTitle("Toutes catégories projets prévisionnels");
        }else{
            $columnChart->getOptions()->setTitle("Toutes catégories projets vendus et prévisionnels");
        }
        $columnChart->getOptions()->setHeight(250);
        $columnChart->getOptions()->setWidth(600);
        $columnChart->getOptions()->getTitleTextStyle()->setBold(true);
        $columnChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $columnChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $columnChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $columnChart->getOptions()->getTitleTextStyle()->setFontSize(15);
        //$columnChart->getOptions()->getAnnotations()->getTextStyle()->setColor('#000');

        return $columnChart;

    }

}