<?php


namespace App\DataFixtures;


use App\Entity\Projet;
use App\Repository\CategorieRepository;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ProjetFixture extends \Doctrine\Bundle\FixturesBundle\Fixture
{
    const NB_PROJET = 20;

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i=0;$i<self::NB_PROJET;$i++)
        {
            $pj = new Projet();
            $pj ->setNom($faker->company);
            //creation des refernces vers chaque projet cree
            $this->addReference(Projet::class.($i+1),$pj);

            $manager->persist($pj);
        }
        $manager->flush();
    }
}