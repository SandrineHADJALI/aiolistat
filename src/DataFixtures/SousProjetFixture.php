<?php


namespace App\DataFixtures;


use App\Entity\AutrePrestation;
use App\Entity\Categorie;
use App\Entity\Projet;
use App\Entity\SousProjet;
use App\Repository\CategorieRepository;
use App\Types\CategorieType;
use App\Types\PeriodiciteType;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SousProjetFixture extends \Doctrine\Bundle\FixturesBundle\Fixture implements DependentFixtureInterface
{
    private const NB_SOUS_PROJET = 50;

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('fr_FR');

        for ($i=0;$i<self::NB_SOUS_PROJET;$i++)
        {

            $spj = new SousProjet();
            $spj->setCout(0);
            $spj->setDesignation($faker->sentence(1,false));

            $spj->setPrixVente($faker->numberBetween(4000,9000));

            $spj->setDateAction($faker->dateTimeBetween('-2months','+12months'));


            //gestion perdiodicite

            if ($i<(self::NB_SOUS_PROJET/2))
            {
                $spj->setPeriodicite('ONESHOT');
            }else{
                $spj->setPeriodicite('MENSUEL');
            }

            //gestion des categories
            $cateRepo = $manager->getRepository(Categorie::class);

            if ($i<(self::NB_SOUS_PROJET/4))
            {
                $spj->setCategorie($cateRepo->findOneBy(['libelle'=>'Communications']));
            }elseif ($i<(self::NB_SOUS_PROJET/3))
            {
                $spj->setCategorie($cateRepo->findOneBy(['libelle'=>'Développements']));
            }elseif ($i<(self::NB_SOUS_PROJET/2))
            {
                $spj->setCategorie($cateRepo->findOneBy(['libelle'=>'Prestations de conseils']));
            }else{
                $spj->setCategorie($cateRepo->findOneBy(['libelle'=>'Formations']));
            }
            //ajout previsionnel
            if ($i<=self::NB_SOUS_PROJET/2)
            {
                $spj->setIsPrevisionnel(false);
            }else{
                if ($spj->getDateAction()>new \DateTime('now')) {
                    $spj->setIsPrevisionnel(true);
                }else{
                    $spj->setIsPrevisionnel(false);
                }
            }
            //gestion des projets
            $spj->setProjet($this->getReference(Projet::class.$faker->numberBetween(1,20)));

            //ajout de reference
            $this->addReference(SousProjet::class.($i+1),$spj);
            $manager->persist($spj);

            $alea = $faker->numberBetween(1,6);
            if ($alea == 5 || $alea == 4)
            {
                $ap = new AutrePrestation();
                $ap->setDetail('Achat divers');
                $ap->setMontant($faker->numberBetween('10',150));
                $ap->setSousProjet($spj);
                $spj->addAutrePrestation($ap);
            }
            $spj->updateCout();
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [CategorieFixture::class,ProjetFixture::class];
    }
}