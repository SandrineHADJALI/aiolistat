<?php


namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends \Doctrine\Bundle\FixturesBundle\Fixture implements FixtureGroupInterface
{

    private $encoder;


    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder=$encoder;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('cecile@aioli-digital.com');
        $user->setPassword($this->encoder->encodePassword($user,'admin'));
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        $user1 = new User();
        $user1->setEmail('candice@aioli-digital.com');
        $user1->setPassword($this->encoder->encodePassword($user,'admin'));
        $user1->setRoles(['ROLE_ADMIN']);
        $manager->persist($user1);
        $manager->flush();
    }
    public static function getGroups(): array
    {
        return ['lancement'];
    }
}