<?php


namespace App\DataFixtures;


use App\Entity\Independant;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class IndependantFixture extends \Doctrine\Bundle\FixturesBundle\Fixture
{

    private const  NB_INDEPENDANT = 10;
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i=0;$i<self::NB_INDEPENDANT;$i++)
        {
            $inde = new Independant();
            $inde ->setNom($faker->lastName);
            $inde->setPrenom($faker->firstName());
            $inde->setSalaire($faker->numberBetween(15,40));

            $this->addReference(Independant::class.($i+1),$inde);
            $manager->persist($inde);
        }

        $manager->flush();
    }
}