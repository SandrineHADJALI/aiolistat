<?php


namespace App\DataFixtures;


use App\Entity\Heure;
use App\Entity\Independant;
use App\Entity\SousProjet;
use App\Types\CategorieType;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class HeureFixture extends \Doctrine\Bundle\FixturesBundle\Fixture implements  DependentFixtureInterface
{
    private const  NB_HEURES = 60;
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for($i=0; $i<self::NB_HEURES;$i++)
        {
            $h = new Heure();
            $h->setHeure($faker->numberBetween(10,20));
            $inde = $this->getReference(Independant::class.($faker->numberBetween(1,10)));
            if ($inde instanceof Independant)
            {
                $h->setIndependant($inde);
            }
            $sp = $this->getReference(SousProjet::class.$faker->numberBetween(1,40));
            $h->updateCout();
            if ($sp instanceof  SousProjet)
            {
                $h->setSousProjet($sp);
                $sp->addHeure($h);

            }
            $sp->updateCout();


            $manager->persist($h);
            $manager->persist($sp);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return[IndependantFixture::class,SousProjetFixture::class];
    }
}