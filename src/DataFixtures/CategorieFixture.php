<?php


namespace App\DataFixtures;


use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;

class CategorieFixture extends \Doctrine\Bundle\FixturesBundle\Fixture implements FixtureGroupInterface
{

    private $categories;

    public function __construct()
    {
        $this->categories= Categorie::getCategories();

    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {

        foreach ($this->categories as $category)
        {
            $categorie = new Categorie();
            $categorie->setLibelle($category);
            $manager->persist($categorie);
        }
        $manager->flush();
    }


    public static function getGroups(): array
    {
        return ['lancement'];
    }
}