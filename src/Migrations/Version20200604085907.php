<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200604085907 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE projet (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE heure (id INT AUTO_INCREMENT NOT NULL, sous_projet_id INT NOT NULL, independant_id INT NOT NULL, heure DOUBLE PRECISION NOT NULL, INDEX IDX_1173E8B81BC8693D (sous_projet_id), INDEX IDX_1173E8B8CF20177B (independant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, libelle ENUM(\'DEV\', \'COMM\', \'STRAT\', \'FORMATION\') NOT NULL COMMENT \'(DC2Type:enumCategorie)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE independant (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, salaire DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sous_projet (id INT AUTO_INCREMENT NOT NULL, categorie_id INT NOT NULL, projet_id INT NOT NULL, designation VARCHAR(255) NOT NULL, prix_vente DOUBLE PRECISION NOT NULL, roi DOUBLE PRECISION DEFAULT NULL, date_action DATE NOT NULL, periodicite ENUM(\'ONE SHOT\', \'MENSUEL\') NOT NULL COMMENT \'(DC2Type:enumPeriodicite)\', INDEX IDX_800A6E5EBCF5E72D (categorie_id), INDEX IDX_800A6E5EC18272 (projet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE heure ADD CONSTRAINT FK_1173E8B81BC8693D FOREIGN KEY (sous_projet_id) REFERENCES sous_projet (id)');
        $this->addSql('ALTER TABLE heure ADD CONSTRAINT FK_1173E8B8CF20177B FOREIGN KEY (independant_id) REFERENCES independant (id)');
        $this->addSql('ALTER TABLE sous_projet ADD CONSTRAINT FK_800A6E5EBCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE sous_projet ADD CONSTRAINT FK_800A6E5EC18272 FOREIGN KEY (projet_id) REFERENCES projet (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sous_projet DROP FOREIGN KEY FK_800A6E5EC18272');
        $this->addSql('ALTER TABLE sous_projet DROP FOREIGN KEY FK_800A6E5EBCF5E72D');
        $this->addSql('ALTER TABLE heure DROP FOREIGN KEY FK_1173E8B8CF20177B');
        $this->addSql('ALTER TABLE heure DROP FOREIGN KEY FK_1173E8B81BC8693D');
        $this->addSql('DROP TABLE projet');
        $this->addSql('DROP TABLE heure');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE independant');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE sous_projet');
    }
}
