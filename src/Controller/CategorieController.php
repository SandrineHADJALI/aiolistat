<?php


namespace App\Controller;


use App\Entity\Categorie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategorieController
 * @package App\Controller
 * @Route("/categorie",name="categorie_")
 */
class CategorieController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * @Route("/list",name="list")
     */
    public function list(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                $cateRepo = $this->getDoctrine()->getRepository(Categorie::class);
                $categories = $cateRepo->findAllInArray();
                return new JsonResponse($categories);
            }
        }else{
            return$this->redirectToRoute('app_login');
        }
    }
}