<?php

namespace App\Controller;

use App\Entity\AutrePrestation;
use App\Entity\SousProjet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class PrestationController
 * @package App\Controller
 * @Route("/prestation", name="prestation_")
 */
class PrestationController extends AbstractController
{
    /**
     * @Route("/add/{id}",name="add",requirements={"id":"\d+"})
     * @param $id
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public  function add($id, EntityManagerInterface $manager,Request $request)
    {
            if ($request->isXmlHttpRequest())
            {
                $spRepo = $this->getDoctrine()->getRepository(SousProjet::class);

                $sp = $spRepo->find($id);
                if (isset($sp))
                {
                    $encoders = [new JsonEncoder()];
                    $normalizers = [new ObjectNormalizer()];
                    $serializer = new Serializer($normalizers, $encoders);
                    $prestaAjout = $serializer->deserialize($request->getContent(), AutrePrestation::class, 'json');
                    if (isset($prestaAjout))
                    {
                        $prestaAjout->setSousprojet($sp);
                        $sp->addAutrePrestation($prestaAjout);
                        $manager->persist($sp);
                        $manager->flush();
                        return new JsonResponse($prestaAjout->getId());
                    }else{
                        return new JsonResponse("Prestation invalide",Response::HTTP_FORBIDDEN);
                    }
                }else{
                    return  new JsonResponse("Sous-Projet inconnu",Response::HTTP_NOT_FOUND);
                }
            }
             return $this->redirectToRoute('app_login');

    }


    /**
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return JsonResponse|RedirectResponse
     * @Route("/delete/{id}",name="delete",requirements={"id":"\d+"})
     */
    public function delete($id,Request $request,EntityManagerInterface $manager)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                $prestaRepo = $this->getDoctrine()->getRepository(AutrePrestation::class);
                $presta = $prestaRepo->find($id);
                if (isset($presta))
                {
                    $manager->remove($presta);
                    $manager->flush();
                    return  new JsonResponse("Suppression effectuée avec succès");
                }else{
                    return new JsonResponse("Prestation inconnue",Response::HTTP_NOT_FOUND);
                }
            }
        }
        return $this->redirectToRoute("app_login");
    }


    /**
     * @param $id
     * @param Request $request
     * @Route("/{id}",name="detail",requirements={"id":"\d+"})
     * @return JsonResponse|RedirectResponse
     */
    public function detail($id,Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                $prestarepo = $this->getDoctrine()->getRepository(AutrePrestation::class);
                $presta = $prestarepo->findArray($id);
                if (isset($presta))
                {
                    return new JsonResponse($presta);
                }else{
                    return new JsonResponse("Prestation inocnnue",Response::HTTP_NOT_FOUND);
                }
            }
        }
        return $this->redirectToRoute('app_login');
    }


    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return JsonResponse|RedirectResponse
     * @Route("/update",name="update")
     */
    public function update(Request$request,EntityManagerInterface $manager)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                //traitement pour requete ajax
                $prestaRepo = $this->getDoctrine()->getRepository(AutrePrestation::class);

                $encoders = [new JsonEncoder()];
                $normalizers = [new ObjectNormalizer()];
                $serializer = new Serializer($normalizers, $encoders);
                $prestaUpdate = $serializer->deserialize($request->getContent(), AutrePrestation::class, 'json');
                $presta = $prestaRepo->find($prestaUpdate->getId());
                if (isset($presta))
                {
                    //hydratation manuelle ==> faire formulaire de preference
                    $presta->setMontant($prestaUpdate->getMontant());
                    $presta->setDetail($prestaUpdate->getDetail());

                    $manager->persist($presta);
                    $manager->flush();
                    return new JsonResponse("Modifcations effectuées avec succès");
                }else{
                    return  new JsonResponse("Prestation inconnue",Response::HTTP_NOT_FOUND);
                }
            }
        }
        return $this->redirectToRoute('app_login');
    }
}
