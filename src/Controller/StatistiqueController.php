<?php

namespace App\Controller;

use App\ChartService\ChartService;
use App\Entity\Categorie;
use App\Entity\Projet;
use App\Entity\SousProjet;
use DateTime;
use Detection\MobileDetect;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StatistiqueController
 * @package App\Controller
 * @Route("/statistique",name="statistique_")
 */
class StatistiqueController extends AbstractController
{
    /**
     * @Route("/graphique", name="graphique")
     */

    public function statistique(ChartService $pieChart): Response
    {
        $detect = new MobileDetect();
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            $projetRepo = $this->getDoctrine()->getRepository(Projet::class);
            //traitement date;
            $dateDebut = new DateTime();
            $dateDebut->setDate($dateDebut->format('Y'), $dateDebut->format('m'), 1);
            $dateDebut->setTime(0, 0, 0);
            $dateFin = new DateTime();
            $dateFin->setDate($dateFin->format('Y'), $dateFin->format('m'), 1);
            $dateFin->setTime(0, 0, 0);
            $dateFin->modify('+6 months');
            $dateFin->modify('-1 days');

            $Mois = array(
                1 => '01',
                2 => '02',
                3 => '03',
                4 => '04',
                5 => '05',
                6 => '06',
                7 => '07',
                8 => '08',
                9 => '09',
                10 => '10',
                11 => '11',
                12 => '12',
            );

            $Annee = array();
            for ($ans = 2016; $ans < 2050; $ans++) {
                $Annee[$ans] = "$ans";
            }

            //On alimente la liste des periodicite pour les filtres
            $sousProjet = $this->getDoctrine()->getRepository(SousProjet::class);
            $listePeriodicite = $sousProjet->listePeriodicite();
            $listePeriodiciteTampon = [];

            foreach ($listePeriodicite as $item) {
                array_push($listePeriodiciteTampon, $item['periodicite']);
            }

            //si on applique un filtre
            if ($_POST) {


                $dateDebutC = $_POST['duannee'] . '-' . $_POST['dumois'] . '-' . "01";
                $dateFinC = $_POST['auannee'] . '-' . $_POST['aumois'] . '-' . "01";


                $g = $projetRepo->findProjetfilterP($dateDebut->format('Y-m-d'), $dateFin->format('Y-m-d'), 0);

                $projetRepo = $this->getDoctrine()->getRepository(SousProjet::class);
                $total = $projetRepo->findTotalmoisP($dateDebut, $dateFin, 0);
                $n = $this->lireetcreetableau($g);
                //permet d'avoir les 6 mois actuel
                $lesMois = $this->les6Mois1ereentrée($dateDebut->format('m'), $dateFin->format('m'));
                $projetRepo = $this->getDoctrine()->getRepository(Categorie::class);
                $categorie = $projetRepo->findAll();


                if ($_POST['categorie'] != 'all' and $_POST['periodicite'] != "all" and $_POST['previsionnel'] != "all") {

                    [$nameCategorie, $pvm, $pm, $pvo, $po] = $this->donneesGraphiqueSelonCategorie($dateDebutC, $dateFinC);
                    [$previsionnel, $SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers] = $this->donneesGraphiqueToutesLesCategoriesVendusOuPrevi($dateDebutC, $dateFinC);

                    if ($_POST['previsionnel']==0) {
                        [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotauxPV($dateDebutC, $dateFinC);
                    }else{
                        [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotauxPrevisionnel($dateDebutC, $dateFinC);
                    }
                    return $this->render('statistique/stat.html.twig', [
                        'tabProjets' => $n,
                        'total' => $total,
                        'categorie' => $categorie,
                        'Mois' => $Mois,
                        'Ans' => $Annee,
                        'mobile' => ($detect->isMobile() || $detect->isTablet()),
                        'lesMois' => $lesMois,
                        'POST' => $_POST,
                        'categoriePie' => $pieChart->CreerCategoriePieChart($nameCategorie, $pvm, $pm, $pvo, $po),
                        'toutesCategoriesPie' => $pieChart->CreerToutesLesCategoriesVenduSeulOuPrevisionnelSeulPieChart($previsionnel, $SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers),
                        'listePeriodicite' => $listePeriodiciteTampon,
                        'totauxColonnePV' => $pieChart->CreerVenteCoutColumnChartPV($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi, $_POST['previsionnel']),

                    ]);


                } elseif ($_POST['categorie'] != 'all' and $_POST['periodicite'] != "all" and $_POST['previsionnel'] == "all") {

                    [$nameCategorie, $pvm, $pm, $pvo, $po] = $this->donneesGraphiqueSelonCategorie($dateDebutC, $dateFinC);
                    [$SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers] = $this->donneesGraphiqueToutesLesCategories($dateDebutC, $dateFinC);
                    [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotaux($dateDebutC, $dateFinC);


                    return $this->render('statistique/stat.html.twig', [
                        'tabProjets' => $n,
                        'total' => $total,
                        'categorie' => $categorie,
                        'Mois' => $Mois,
                        'Ans' => $Annee,
                        'mobile' => ($detect->isMobile() || $detect->isTablet()),
                        'lesMois' => $lesMois,
                        'POST' => $_POST,
                        'categoriePie' => $pieChart->CreerCategoriePieChart($nameCategorie, $pvm, $pm, $pvo, $po),
                        'toutesCategoriesPie' => $pieChart->CreerToutesLesCategoriesPVEtPrevisionnelPieChart($SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers),
                        'listePeriodicite' => $listePeriodiciteTampon,
                        'totauxColonnePV' => $pieChart->CreerVenteCoutColumnChartPV($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi, $_POST['previsionnel']),


                    ]);

                } elseif ($_POST['categorie'] != 'all' and $_POST['periodicite'] == "all" and $_POST['previsionnel'] != "all") {

                    [$nameCategorie, $pvm, $pm, $pvo, $po] = $this->donneesGraphiqueSelonCategorie($dateDebutC, $dateFinC);
                    [$previsionnel, $SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers] = $this->donneesGraphiqueToutesLesCategoriesVendusOuPrevi($dateDebutC, $dateFinC);
                    if ($_POST['previsionnel']==0) {
                        [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotauxPV($dateDebutC, $dateFinC);
                    }else{
                        [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotauxPrevisionnel($dateDebutC, $dateFinC);
                    }

                    return $this->render('statistique/stat.html.twig', [
                        'tabProjets' => $n,
                        'total' => $total,
                        'categorie' => $categorie,
                        'Mois' => $Mois,
                        'Ans' => $Annee,
                        'mobile' => ($detect->isMobile() || $detect->isTablet()),
                        'lesMois' => $lesMois,
                        'POST' => $_POST,
                        'categoriePie' => $pieChart->CreerCategoriePieChart($nameCategorie, $pvm, $pm, $pvo, $po),
                        'toutesCategoriesPie' => $pieChart->CreerToutesLesCategoriesVenduSeulOuPrevisionnelSeulPieChart($previsionnel, $SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers),
                        'listePeriodicite' => $listePeriodiciteTampon,
                        'totauxColonnePV' => $pieChart->CreerVenteCoutColumnChartPV($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi, $_POST['previsionnel']),


                    ]);


                } elseif ($_POST['categorie'] != 'all' and $_POST['periodicite'] == "all" and $_POST['previsionnel'] == "all") {

                    [$nameCategorie, $pvm, $pm, $pvo, $po] = $this->donneesGraphiqueSelonCategorie($dateDebutC, $dateFinC);
                    [$SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers] = $this->donneesGraphiqueToutesLesCategories($dateDebutC, $dateFinC);
                    [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotaux($dateDebutC, $dateFinC);


                    return $this->render('statistique/stat.html.twig', [
                        'tabProjets' => $n,
                        'total' => $total,
                        'categorie' => $categorie,
                        'Mois' => $Mois,
                        'Ans' => $Annee,
                        'mobile' => ($detect->isMobile() || $detect->isTablet()),
                        'lesMois' => $lesMois,
                        'POST' => $_POST,
                        'categoriePie' => $pieChart->CreerCategoriePieChart($nameCategorie, $pvm, $pm, $pvo, $po),
                        'toutesCategoriesPie' => $pieChart->CreerToutesLesCategoriesPVEtPrevisionnelPieChart($SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers),
                        'listePeriodicite' => $listePeriodiciteTampon,
                        'totauxColonnePV' => $pieChart->CreerVenteCoutColumnChartPV($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi, $_POST['previsionnel']),


                    ]);


                } elseif ($_POST['categorie'] == "all" and $_POST['periodicite'] != "all" and $_POST['previsionnel'] != "all") {
                    [$previsionnel, $SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers] = $this->donneesGraphiqueToutesLesCategoriesVendusOuPrevi($dateDebutC, $dateFinC);
                    if ($_POST['previsionnel']==0) {
                        [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotauxPV($dateDebutC, $dateFinC);
                    }else{
                        [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotauxPrevisionnel($dateDebutC, $dateFinC);
                    }
                    return $this->render('statistique/stat.html.twig', [
                        'tabProjets' => $n,
                        'total' => $total,
                        'categorie' => $categorie,
                        'Mois' => $Mois,
                        'Ans' => $Annee,
                        'mobile' => ($detect->isMobile() || $detect->isTablet()),
                        'lesMois' => $lesMois,
                        'POST' => $_POST,
                        'categoriePie' => NULL,
                        'toutesCategoriesPie' => $pieChart->CreerToutesLesCategoriesVenduSeulOuPrevisionnelSeulPieChart($previsionnel, $SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers),
                        'listePeriodicite' => $listePeriodiciteTampon,
                        'totauxColonnePV' => $pieChart->CreerVenteCoutColumnChartPV($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi, $_POST['previsionnel']),

                    ]);

                } elseif ($_POST['categorie'] == 'all' and $_POST['periodicite'] != "all" and $_POST['previsionnel'] == "all") {

                    [$SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers] = $this->donneesGraphiqueToutesLesCategories($dateDebutC, $dateFinC);
                    [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotaux($dateDebutC, $dateFinC);

                    return $this->render('statistique/stat.html.twig', [
                        'tabProjets' => $n,
                        'total' => $total,
                        'categorie' => $categorie,
                        'Mois' => $Mois,
                        'Ans' => $Annee,
                        'mobile' => ($detect->isMobile() || $detect->isTablet()),
                        'lesMois' => $lesMois,
                        'POST' => $_POST,
                        'categoriePie' => NULL,
                        'toutesCategoriesPie' => $pieChart->CreerToutesLesCategoriesPVEtPrevisionnelPieChart($SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers),
                        'listePeriodicite' => $listePeriodiciteTampon,
                        'totauxColonnePV' => $pieChart->CreerVenteCoutColumnChartPV($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi, $_POST['previsionnel']),

                    ]);


                } elseif ($_POST['categorie'] == "all" and $_POST['periodicite'] == "all" and $_POST['previsionnel'] != "all") {

                    [$previsionnel, $SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers] = $this->donneesGraphiqueToutesLesCategoriesVendusOuPrevi($dateDebutC, $dateFinC);
                    if ($_POST['previsionnel']==0) {
                        [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotauxPV($dateDebutC, $dateFinC);
                    }else{
                        [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotauxPrevisionnel($dateDebutC, $dateFinC);
                    }

                    return $this->render('statistique/stat.html.twig', [
                        'tabProjets' => $n,
                        'total' => $total,
                        'categorie' => $categorie,
                        'Mois' => $Mois,
                        'Ans' => $Annee,
                        'mobile' => ($detect->isMobile() || $detect->isTablet()),
                        'lesMois' => $lesMois,
                        'POST' => $_POST,
                        'categoriePie' => NULL,
                        'toutesCategoriesPie' => $pieChart->CreerToutesLesCategoriesVenduSeulOuPrevisionnelSeulPieChart($previsionnel, $SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers),
                        'listePeriodicite' => $listePeriodiciteTampon,
                        'totauxColonnePV' => $pieChart->CreerVenteCoutColumnChartPV($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi, $_POST['previsionnel']),

                    ]);


                } else {
                    //($_POST['categorie'] == "all" and $_POST['periodicite'] == "all" and $_POST['previsionnel'] == "all")

                    [$SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers] = $this->donneesGraphiqueToutesLesCategories($dateDebutC, $dateFinC);
                    [$totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi] = $this->donneesColumnChartTotaux($dateDebutC, $dateFinC);

                    return $this->render('statistique/stat.html.twig', [
                        'tabProjets' => $n,
                        'total' => $total,
                        'categorie' => $categorie,
                        'Mois' => $Mois,
                        'Ans' => $Annee,
                        'mobile' => ($detect->isMobile() || $detect->isTablet()),
                        'lesMois' => $lesMois,
                        'POST' => $_POST,
                        'categoriePie' => NULL,
                        'toutesCategoriesPie' => $pieChart->CreerToutesLesCategoriesPVEtPrevisionnelPieChart($SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers),
                        'listePeriodicite' => $listePeriodiciteTampon,
                        'totauxColonnePV' => $pieChart->CreerVenteCoutColumnChartPV($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi, $_POST['previsionnel']),

                    ]);
                }


            } else {

                //lorsque l'on arrive sur la page et qu'il y a pas encore de filtre
                $g = $projetRepo->findProjetfilterP($dateDebut->format('Y-m-d'), $dateFin->format('Y-m-d'), 0);

                $projetRepo = $this->getDoctrine()->getRepository(SousProjet::class);
                $total = $projetRepo->findTotalmoisP($dateDebut, $dateFin, 0);
                $n = $this->lireetcreetableau($g);
                //permet d'avoir les 6 mois actuel
                $lesMois = $this->les6Mois1ereentrée($dateDebut->format('m'), $dateFin->format('m'));
// je mets tout dans le else
                // }
                $sousProjet = $this->getDoctrine()->getRepository(SousProjet::class);
                $listePeriodicite = $sousProjet->listePeriodicite();
                $listePeriodiciteTampon = [];

                foreach ($listePeriodicite as $item) {
                    array_push($listePeriodiciteTampon, $item['periodicite']);
                }

                $projetRepo = $this->getDoctrine()->getRepository(Categorie::class);
                $categorie = $projetRepo->findAll();


                return $this->render('statistique/stat.html.twig', [
                    'tabProjets' => $n,
                    'total' => $total,
                    'categorie' => $categorie,
                    'Mois' => $Mois,
                    'Ans' => $Annee,
                    'mobile' => ($detect->isMobile() || $detect->isTablet()),
                    'lesMois' => $lesMois,
                    'POST' => $_POST,
                    'categoriePie' => NULL,
                    'toutesCategoriesPie' => NULL,
                    'totauxColonnePV' => NULL,
                    'listePeriodicite' => $listePeriodiciteTampon,

                ]);
            }
        }
        return $this->redirectToRoute('app_login');
    }


    //permet d'avoir les 6 mois en fonction du mois actuel
    public function les6Mois1ereentrée($mdebut, $mfin)
    {

        $mois = $mdebut;
        $date = new \DateTime('now');
        $date = $date->format('Y');
        $month = array(
            1 => 'Janvier',
            2 => 'Février',
            3 => 'Mars',
            4 => 'Avril',
            5 => 'Mai',
            6 => 'Juin',
            7 => 'Juillet',
            8 => 'Août',
            9 => 'Septembre',
            10 => 'Octobre',
            11 => 'Novembre',
            12 => 'Décembre',
        );
        $listemois = array();


        for ($mdebut; $mdebut <= $mfin; $mdebut++) {

            if ($mois == 13) {
                $mois = 1;
                $date++;
            }


            foreach ($month as $key => $value) {
                if ($key == $mois) {
                    $listemois[$date . "-" . $mois] = $value;
                }
            }
            $mois++;
        }


        return $listemois;
    }

    //permet d'avoir les mois en fonction de la date de debut et de fin
    public function lesMoisFiltre($mdebut, $adebut, $nb)
    {

        $mois = $mdebut;
        $annee = $adebut;

        $month = array(
            1 => 'Janvier',
            2 => 'Février',
            3 => 'Mars',
            4 => 'Avril',
            5 => 'Mai',
            6 => 'Juin',
            7 => 'Juillet',
            8 => 'Août',
            9 => 'Septembre',
            10 => 'Octobre',
            11 => 'Novembre',
            12 => 'Décembre',
        );
        $listemois = array();


        while ($nb != -1) {
            $nb = $nb - 1;
            if ($mois == 13) {
                $mois = 1;
                $annee++;
            }


            foreach ($month as $key => $value) {
                if ($key == $mois) {
                    $listemois[strval($annee) . "-" . strval($mois)] = $value;
                }
            }
            $mois++;
        }
        return $listemois;
    }

    //permet de générer une liste générer pour être en accord avec la vue twig
    public function lireetcreetableau($leTableauD)
    {
        $projet = [];
        $i = 0;
        $y = 0;


        foreach ($leTableauD as $value) {
            if ($i == 0) {
                $projet[] = ['id' => $value['id'], 'nom' => $value['nom'], 'sousProjets' => [['prixVente' => $value['prixVente'], 'dateAction' => $value['date_action']]]];
                $i = 1;
            }
            if ($i != 0) {
                if (!in_array($value['id'], $projet[$i - 1])) {

                    $i++;
                    $projet[] = ['id' => $value['id'], 'nom' => $value['nom'], 'sousProjets' => [['prixVente' => $value['prixVente'], 'dateAction' => $value['date_action']]]];
                } else {
                    if ($y != 0) {
                        $projet[$i - 1]['sousProjets'][] = ['prixVente' => $value['prixVente'], 'dateAction' => $value['date_action']];
                    }
                }
                $y = 1;
            }

        }

        return $projet;
    }

    //Statistiques : Alimentation des données pour le 1er graphique : pour une categorie : les PV, Previ, Mensuel et oneshot
    public function donneesGraphiqueSelonCategorie($dateDebutC, $dateFinC)
    {
        $sousProjet = $this->getDoctrine()->getRepository(SousProjet::class);
        $pvm = $sousProjet->totalVenteCategorie($_POST['categorie'], $dateDebutC, $dateFinC, 0, 'MENSUEL');
        $pm = $sousProjet->totalVenteCategorie($_POST['categorie'], $dateDebutC, $dateFinC, 1, 'MENSUEL');
        $pvo = $sousProjet->totalVenteCategorie($_POST['categorie'], $dateDebutC, $dateFinC, 0, 'ONESHOT');
        $po = $sousProjet->totalVenteCategorie($_POST['categorie'], $dateDebutC, $dateFinC, 1, 'ONESHOT');
        $pvm = (int)$pvm[0]['prix_vente'];
        $pm = (int)$pm[0]['prix_vente'];
        $pvo = (int)$pvo[0]['prix_vente'];
        $po = (int)$po[0]['prix_vente'];

        $doctrineCategorie = $this->getDoctrine()->getRepository(Categorie::class);

        $nameCategorie = $doctrineCategorie->nameCategorie($_POST['categorie'])[0]['libelle'];

        return array($nameCategorie, $pvm, $pm, $pvo, $po);
    }

    //Statistiques : Alimentation des données pour le 2eme graphique : proportion de chaque categorie (somme des PV, Previ, Mensuel et oneshot)
    public function donneesGraphiqueToutesLesCategories($dateDebutC, $dateFinC)
    {
        $sousProjet = $this->getDoctrine()->getRepository(SousProjet::class);
        $SommeComm = $sousProjet->totalVenteCategorieVenduEtPrevi(1, $dateDebutC, $dateFinC);
        $SommeDev = $sousProjet->totalVenteCategorieVenduEtPrevi(2, $dateDebutC, $dateFinC);
        $SommeConseil = $sousProjet->totalVenteCategorieVenduEtPrevi(3, $dateDebutC, $dateFinC);
        $SommeFormation = $sousProjet->totalVenteCategorieVenduEtPrevi(4, $dateDebutC, $dateFinC);
        $SommePrint = $sousProjet->totalVenteCategorieVenduEtPrevi(5, $dateDebutC, $dateFinC);
        $SommeDivers = $sousProjet->totalVenteCategorieVenduEtPrevi(6, $dateDebutC, $dateFinC);

        $SommeComm = (int)$SommeComm[0]['prix_vente'];
        $SommeDev = (int)$SommeDev[0]['prix_vente'];
        $SommeConseil = (int)$SommeConseil[0]['prix_vente'];
        $SommeFormation = (int)$SommeFormation[0]['prix_vente'];
        $SommePrint = (int)$SommePrint[0]['prix_vente'];
        $SommeDivers = (int)$SommeDivers[0]['prix_vente'];

        return array($SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers);
    }

    //Statistiques : Alimentation des données pour le 3eme graphique : proportion de chaque categorie avec Filtre vendus ou previsionnel (somme des Prix_vente, Mensuel et oneshot)
    public function donneesGraphiqueToutesLesCategoriesVendusOuPrevi($dateDebutC, $dateFinC)
    {
        $sousProjet = $this->getDoctrine()->getRepository(SousProjet::class);
        $SommeComm = $sousProjet->totalVenteCategorieVendusSeulOuPreviSeul(1, $dateDebutC, $dateFinC, $_POST['previsionnel']);
        $SommeDev = $sousProjet->totalVenteCategorieVendusSeulOuPreviSeul(2, $dateDebutC, $dateFinC, $_POST['previsionnel']);
        $SommeConseil = $sousProjet->totalVenteCategorieVendusSeulOuPreviSeul(3, $dateDebutC, $dateFinC, $_POST['previsionnel']);
        $SommeFormation = $sousProjet->totalVenteCategorieVendusSeulOuPreviSeul(4, $dateDebutC, $dateFinC, $_POST['previsionnel']);
        $SommePrint = $sousProjet->totalVenteCategorieVendusSeulOuPreviSeul(5, $dateDebutC, $dateFinC, $_POST['previsionnel']);
        $SommeDivers = $sousProjet->totalVenteCategorieVendusSeulOuPreviSeul(6, $dateDebutC, $dateFinC, $_POST['previsionnel']);

        $SommeComm = (int)$SommeComm[0]['prix_vente'];
        $SommeDev = (int)$SommeDev[0]['prix_vente'];
        $SommeConseil = (int)$SommeConseil[0]['prix_vente'];
        $SommeFormation = (int)$SommeFormation[0]['prix_vente'];
        $SommePrint = (int)$SommePrint[0]['prix_vente'];
        $SommeDivers = (int)$SommeDivers[0]['prix_vente'];


        return array($_POST['previsionnel'], $SommeComm, $SommeDev, $SommeConseil, $SommeFormation, $SommePrint, $SommeDivers);
    }

    //Statistiques : Alimentation des données pour le 1er graphique en colonne: somme des ventes et somme des couts selon les dates seulement
    public function donneesColumnChartTotaux($dateDebutC, $dateFinC)
    {
        $sousProjet = $this->getDoctrine()->getRepository(SousProjet::class);
        $totVenteMensuel = $sousProjet->totalVenteEtCout($dateDebutC, $dateFinC, "MENSUEL");
        $totCoutMensuel = $sousProjet->totalVenteEtCout($dateDebutC, $dateFinC, "MENSUEL");

        $totVenteOneshot = $sousProjet->totalVenteEtCout($dateDebutC, $dateFinC, "ONESHOT");
        $totCoutOneshot = $sousProjet->totalVenteEtCout($dateDebutC, $dateFinC, "ONESHOT");


        $totVenteMensuel = (int)$totVenteMensuel[0]['prixVente'];
        $totCoutMensuel = (int)$totCoutMensuel[0]['cout'];
        $totVenteOneshot = (int)$totVenteOneshot[0]['prixVente'];
        $totCoutOneshot = (int)$totCoutOneshot[0]['cout'];


        $totVente = $totVenteMensuel + $totVenteOneshot;
        $totCout = $totCoutMensuel + $totCoutOneshot;

        $roiMensuel = (int)(($totCoutMensuel / $totVenteMensuel) * 100);
        $roiOneshot = (int)(($totCoutOneshot / $totVenteOneshot) * 100);
        $roi = (int)(($totCout / $totVente) * 100);


        return array($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi);
    }

    //Statistiques : Alimentation des données pour le 2eme graphique en colonne: somme des projets vendus seuls et somme des couts selon les dates
    public function donneesColumnChartTotauxPV($dateDebutC, $dateFinC)
    {
        $sousProjet = $this->getDoctrine()->getRepository(SousProjet::class);
        $totVenteMensuel = $sousProjet->totalVenteEtCoutPV($dateDebutC, $dateFinC, "MENSUEL");
        $totCoutMensuel = $sousProjet->totalVenteEtCoutPV($dateDebutC, $dateFinC, "MENSUEL");

        $totVenteOneshot = $sousProjet->totalVenteEtCoutPV($dateDebutC, $dateFinC, "ONESHOT");
        $totCoutOneshot = $sousProjet->totalVenteEtCoutPV($dateDebutC, $dateFinC, "ONESHOT");


        $totVenteMensuel = (int)$totVenteMensuel[0]['prixVente'];
        $totCoutMensuel = (int)$totCoutMensuel[0]['cout'];
        $totVenteOneshot = (int)$totVenteOneshot[0]['prixVente'];
        $totCoutOneshot = (int)$totCoutOneshot[0]['cout'];


        $totVente = $totVenteMensuel + $totVenteOneshot;
        $totCout = $totCoutMensuel + $totCoutOneshot;

        $roiMensuel = (int)(($totCoutMensuel / $totVenteMensuel) * 100);
        $roiOneshot = (int)(($totCoutOneshot / $totVenteOneshot) * 100);
        $roi = (int)(($totCout / $totVente) * 100);


        return array($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi);
    }

    //Statistiques : Alimentation des données pour le 2eme graphique en colonne: somme des projets previsionnels seuls et somme des couts selon les dates
    public function donneesColumnChartTotauxPrevisionnel($dateDebutC, $dateFinC)
    {
        $sousProjet = $this->getDoctrine()->getRepository(SousProjet::class);
        $totVenteMensuel = $sousProjet->totalVenteEtCoutPrevisionnel($dateDebutC, $dateFinC, "MENSUEL");
        $totCoutMensuel = $sousProjet->totalVenteEtCoutPrevisionnel($dateDebutC, $dateFinC, "MENSUEL");

        $totVenteOneshot = $sousProjet->totalVenteEtCoutPrevisionnel($dateDebutC, $dateFinC, "ONESHOT");
        $totCoutOneshot = $sousProjet->totalVenteEtCoutPrevisionnel($dateDebutC, $dateFinC, "ONESHOT");


        $totVenteMensuel = (int)$totVenteMensuel[0]['prixVente'];
        $totCoutMensuel = (int)$totCoutMensuel[0]['cout'];
        $totVenteOneshot = (int)$totVenteOneshot[0]['prixVente'];
        $totCoutOneshot = (int)$totCoutOneshot[0]['cout'];


        $totVente = $totVenteMensuel + $totVenteOneshot;
        $totCout = $totCoutMensuel + $totCoutOneshot;

        $roiMensuel = (int)(($totCoutMensuel / $totVenteMensuel) * 100);
        $roiOneshot = (int)(($totCoutOneshot / $totVenteOneshot) * 100);
        $roi = (int)(($totCout / $totVente) * 100);


        return array($totVenteMensuel, $totCoutMensuel, $roiMensuel, $totVenteOneshot, $totCoutOneshot, $roiOneshot, $totVente, $totCout, $roi);
    }
}
