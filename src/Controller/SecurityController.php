<?php

namespace App\Controller;

use App\Form\UserType;
use Detection\MobileDetect;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
         if ($this->getUser()) {
             return $this->redirectToRoute('projet_list');
         }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/update",name="user_update")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function update(Request $request,UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager)
    {
        $detect = new MobileDetect();
        $user = $this->getUser();
        $userForm = $this->createForm(UserType::class,$user);

        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid())
        {

            $password = $encoder->encodePassword($user,$user->getPassword());
            $user->setPassword($password);
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('projet_list');
        }
        return $this->render('security/updateUser.html.twig',[
            'form'=>$userForm->createView(),
            'mobile'=>($detect->isMobile()||$detect->isTablet())
        ]);
    }

}
