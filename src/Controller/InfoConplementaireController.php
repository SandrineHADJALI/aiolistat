<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Projet;
use App\Entity\SousProjet;
use App\Form\SousProjetFiltreType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;
use Detection\MobileDetect;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Expr\Cast\String_;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class InfoConplementaireController
 * @package App\Controller
 * @Route("/infoComplementaire",name="infoComplementaire_")
 */

//la page n'utilise pas le javascript tout est fait en requete sql et php
class InfoConplementaireController extends AbstractController
{
    /**
     * @Route("", name="tableau")
     */
    public function index()
    {
        $detect = new MobileDetect();
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            
            $projetRepo = $this->getDoctrine()->getRepository(Projet::class);
            //traitement date;
            $dateDebut = new DateTime();
            $dateDebut->setDate($dateDebut->format('Y'),$dateDebut->format('m'),1);
            $dateDebut->setTime(0,0,0);
            $dateFin = new DateTime();
            $dateFin->setDate($dateFin->format('Y'),$dateFin->format('m'),1);
            $dateFin->setTime(0,0,0);
            $dateFin->modify('+6 months');
            $dateFin->modify('-1 days');
            
            
           //si on applique un filtre 
            if( $_POST)
            {
            
           $dateDebut=$_POST['duannee'].'-'.$_POST['dumois'].'-'."01";
           $dateFin=$_POST['auannee'].'-'.$_POST['aumois'].'-'."01";
           
            //si on applique le filtre previsionnel
            if($_POST['categorie']=="all" && $_POST['previsionnel']!='all')
            {
                $b = $projetRepo->findProjetfilterP($dateDebut,$dateFin,intval($_POST['previsionnel']));
                $n=$this->lireetcreetableau($b);
                
                $projetRepo = $this->getDoctrine()->getRepository(SousProjet::class);
                $total=$projetRepo->findTotalmoisP($dateDebut,$dateFin,intval($_POST['previsionnel']));
            }
            else{
                //si on applique le filtre catégorie 
                if($_POST['categorie']!="all" && $_POST['previsionnel']=='all')
                {
                    $b = $projetRepo->findProjetfilterC($dateDebut,$dateFin,intval($_POST['categorie']));
                    $n=$this->lireetcreetableau($b);
                    
                    $projetRepo = $this->getDoctrine()->getRepository(SousProjet::class);
                    $total=$projetRepo->findTotalmoisC($dateDebut,$dateFin,intval($_POST['categorie']));
                }
                    //si on applique que le filtre de date 
                    else{
                    if($_POST['categorie']=="all" && $_POST['previsionnel']=='all')
                    {
                    $b = $projetRepo->findProjet6mois($dateDebut,$dateFin);
                    
                    $n=$this->lireetcreetableau($b);
                    $projetRepo = $this->getDoctrine()->getRepository(SousProjet::class);
                    $total=$projetRepo->findTotalmois($dateDebut,$dateFin);
                    }
                    //si on applique tous les filtres 
                    else
                    {
                    $b = $projetRepo->findProjetfilter($dateDebut,$dateFin,intval($_POST['categorie']),intval($_POST['previsionnel']));
                    
                    $n=$this->lireetcreetableau($b);
                    $projetRepo = $this->getDoctrine()->getRepository(SousProjet::class);
                    $total=$projetRepo->findTotalall($dateDebut,$dateFin,intval($_POST['categorie']),intval($_POST['previsionnel']));
                    }
                }
            
            }
           //permet d'avoir les mois en fonction du filtre date 
            $dateDebut=new DateTime($_POST['duannee'].'-'.$_POST['dumois'].'-'."01");
            $dateFin =new DateTime($_POST['auannee'].'-'.$_POST['aumois'].'-'."01");
            $nb= $dateDebut->diff($dateFin);
            
            $nb=($nb->format('%m')+($nb->format('%Y')*12));
            
            $lesMois=$this->lesMoisFiltre($_POST['dumois'],$_POST['duannee'],$nb);
            
            }
            //lorsque l'on arrive sur la page et qu'il y a pas encore de filtre 
            else{
                
            $g = $projetRepo->findProjetfilterP($dateDebut->format('Y-m-d'),$dateFin->format('Y-m-d'),0);
            
            $projetRepo = $this->getDoctrine()->getRepository(SousProjet::class);
            $total=$projetRepo->findTotalmoisP($dateDebut,$dateFin,0);
            $n=$this->lireetcreetableau($g);
            //permet d'avoir les 6 mois actuel 
            $lesMois=$this->les6Mois1ereentrée($dateDebut->format('m'),$dateFin->format('m'));
            
            }
            $projetRepo = $this->getDoctrine()->getRepository(Categorie::class);
            $categorie=$projetRepo->findAll();
            $Mois=array(
                1 => '01',
                2 => '02',
                3 => '03',
                4 => '04',
                5 => '05',
                6 => '06',
                7 => '07',
                8 => '08',
                9=> '09',
               10=> '10',
               11 => '11',
               12 => '12',
           );
           
           $Annee=array();
           for ($ans=2016; $ans<2050; $ans++) { 
               $Annee[$ans]="$ans";
           }
           
            return $this->render('info_conplementaire/index.html.twig', [
                'tabProjets' => $n,
                'total'=>$total,
                'categorie'=>$categorie,
                'Mois'=>$Mois,
                'Ans'=>$Annee,
                'mobile'=>($detect->isMobile() || $detect->isTablet()),
                'lesMois'=>$lesMois,
                'POST'=>$_POST,
                
            ]);
        }
        return $this->redirectToRoute('app_login');

       
    }
    //permet d'avoir les 6 mois en fonction du mois actuel 
    public function les6Mois1ereentrée($mdebut,$mfin)
    {
        
        $mois = $mdebut;
        $date = new \DateTime('now');
        $date=$date->format('Y');
        $month = array(
            1 => 'Janvier',
            2 => 'Février',
            3 => 'Mars',
            4 => 'Avril',
            5 => 'Mai',
            6 => 'Juin',
            7 => 'Juillet',
            8 => 'Août' ,
            9 => 'Septembre',
           10=> 'Octobre',
           11 => 'Novembre',
           12 => 'Décembre',
       );
       $listemois=array();
       
      
        for ($mdebut;$mdebut<=$mfin ;$mdebut++) { 
            
            if($mois==13){$mois=1;$date++;}
        
           
        
            foreach ($month as $key =>$value) {
                if ($key==$mois)
                {
                    $listemois[$date."-".$mois]=$value;
                }
            }
            $mois++;
        }
    
        
        

       return $listemois;
    }

    //permet d'avoir les mois en fonction de la date de debut et de fin 
    public function lesMoisFiltre($mdebut,$adebut,$nb)
    {
        
        $mois = $mdebut;
        $annee= $adebut;
        
        $month = array(
            1 => 'Janvier',
            2 => 'Février',
            3 => 'Mars',
            4 => 'Avril',
            5 => 'Mai',
            6 => 'Juin',
            7 => 'Juillet',
            8 => 'Août',
            9=> 'Septembre',
           10=> 'Octobre',
           11 => 'Novembre',
           12 => 'Décembre',
       );
       $listemois=array();
       
      
        while ($nb != -1)
         { 
            $nb=$nb-1;
            if($mois==13){$mois=1;$annee++;}
        
           
        
            foreach ($month as $key =>$value) {
                if ($key==$mois)
                {
                    $listemois[strval ($annee)."-".strval ($mois)]=$value;
                }
            }
            $mois++;
        }
       return $listemois;
    }
    //permet de générer une liste générer pour étre en accord avec la vue twig 
    public function lireetcreetableau($leTableauD)
    {
        $projet= [];
        $i=0;
        $y=0;
        
        
            foreach ($leTableauD  as  $value) {
                if($i==0)
                {
                    $projet[]= ['id'=> $value['id'] , 'nom'=> $value['nom'],'sousProjets'=> [[ 'prixVente' => $value['prixVente'],'dateAction' => $value['date_action']]]]; 
                    $i=1;
                }
                if($i!=0)
                {
                if (!in_array($value['id'], $projet[$i-1]))
                {
                    
                    $i++;
                    $projet[]= ['id'=> $value['id'] , 'nom'=> $value['nom'],'sousProjets'=> [[ 'prixVente' => $value['prixVente'],'dateAction' => $value['date_action']]]];
                }
                else
                {
                    if($y!=0)
                    {
                    $projet[$i-1]['sousProjets'][]=['prixVente' => $value['prixVente'],'dateAction' => $value['date_action']];
                    }
                }
                $y=1;
            }
                
            }
        
        return $projet;
    }
}
