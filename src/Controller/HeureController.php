<?php

namespace App\Controller;

use App\Entity\Heure;
use App\Entity\Independant;
use App\Entity\SousProjet;
use App\Form\CommentaireType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class HeureController
 * @package App\Controller
 * @Route("/heure",name="heure_")
 */
class HeureController extends AbstractController
{
    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/add",name="add")
     */
    public function insert(Request $request,EntityManagerInterface $manager)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                $data = json_decode($request->getContent(),true);
                if (isset($data))
                {
                    $heure = new Heure();
                    $heureRepo = $this->getDoctrine()->getRepository(Heure::class);
                    $indeRepo = $this->getDoctrine()->getRepository(Independant::class);
                    $spjRepo = $this->getDoctrine()->getRepository(SousProjet::class);
                    $inde = $indeRepo->find($data["independant"]);
                    $spj = $spjRepo->find($data["sousProjet"]);

                    if (isset($inde))
                    {
                        if (isset($spj))
                        {
                            $verif = $heureRepo->findBy(["sousProjet"=>$spj,"independant"=>$inde]);
                            //si independant a deja une heure en lien avec spj
                            //erreur
                            if ($verif==null)
                            {
                                $heure->setHeure($data["heure"]);
                                $heure->setIndependant($inde);
                                //ajout inde avant sous projet ar calcul cout
                                $heure->setSousProjet($spj);
                                $heure->updateCout();
                                $heure->setCommentaire($data["commentaire"]);
                                $manager->persist($heure);
                                $manager->persist($spj);
                                $manager->persist($inde);
                                $manager->flush();
                                return  new JsonResponse($heure->getId());
                            }else{
                                return new JsonResponse("Cet Indépendant effectue déjà des heures sur ce Sous-projet",Response::HTTP_FORBIDDEN);
                            }
                        }else{
                            return new JsonResponse("Sous-Projet inconnu",Response::HTTP_NOT_FOUND);
                        }
                    }else{
                        return  new JsonResponse("Indépendant inconnu",Response::HTTP_NOT_FOUND);
                    }
                }
            }
        }
        return$this->redirectToRoute('app_login');
    }

    /**
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/delete/{id}",name="delete",requirements={"id":"\d+"})
     */
    public function delete($id,Request $request,EntityManagerInterface $manager)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                $heureRepo = $this->getDoctrine()->getRepository(Heure::class);
                $heure=$heureRepo->find($id);
                if (isset($heure))
                {
                    $sp= $heure->getSousProjet();
                    $sp->removeHeure($heure);
                    $manager->remove($heure);
                    $manager->persist($sp);
                    $manager->flush();
                    return  new JsonResponse("Suppression effectuée avec succès");
                }else{
                    return new JsonResponse("Heure inconnue",Response::HTTP_NOT_FOUND);
                }
            }
        }
        return $this->redirectToRoute('app_login');
    }

    /**
     * @Route("/{id}",name="detail" ,requirements={"id":"\d+"})
     * @param $id
     * @param Request $request
     */
    public function detail($id,Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                $heureRepo = $this->getDoctrine()->getRepository(Heure::class);
                $heure = $heureRepo->findInArray($id);
                if (isset($heure))
                {
                    return new JsonResponse($heure);
                }else{
                    return new JsonResponse("Heure inconnue",Response::HTTP_NOT_FOUND);
                }
            }
        }
        return $this->redirectToRoute('app_login');
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/update",name="update")
     */
    public function update(Request $request, EntityManagerInterface $manager)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                //recuperation des informations contenues dans la requete AJAX
                $heureInArray = json_decode($request->getContent(),true);
                $heurerepo = $this->getDoctrine()->getRepository(Heure::class);
                //recherche de l'heure en fonction de l'id fournit par la requète
                $heure = $heurerepo->find($heureInArray["id"]);
                if (isset($heure))
                {
                    $heure->setHeure($heureInArray["heure"]);
                    $heure->setCommentaire($heureInArray["commentaire"]);
                    //recuperation de l'id de l'inde fournit par requete
                    $idIndeMdf = $heureInArray["idInde"];
                    $indeRepo = $this->getDoctrine()->getRepository(Independant::class);
                    $inde = $indeRepo->find($idIndeMdf);


                    if (isset($inde))
                    {
                        //requete pour savoir si independant deja lie au sous-projet
                        $verif = $heurerepo->findBy(["sousProjet"=>$heure->getSousProjet(),"independant"=>$inde]);
                        if ($verif===null)
                        {
                            $heure->setIndependant($inde);
                            $heure->updateCout();
                            $manager->persist($heure);
                            $manager->persist($inde);
                            $manager->flush();
                        }else{
                            //return new JsonResponse("Cet Indépendant effectue déjà des heures sur ce Sous-projet",Response::HTTP_FORBIDDEN);
                            $heure->setIndependant($inde);
                            $heure->updateCout();
                            $manager->persist($heure);
                            $manager->persist($inde);
                            $manager->flush();
                        }
                    }else{
                        return  new JsonResponse("Indépendant inconnu",Response::HTTP_NOT_FOUND);
                    }
                }else{
                    return new JsonResponse("Heure inconnue",Response::HTTP_NOT_FOUND);
                }
            }
        }
        return $this->redirectToRoute('app_login');
    }

}
