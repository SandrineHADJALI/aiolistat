<?php

namespace App\Controller;

use App\Entity\Independant;


use App\Form\IndependantType;
use DateTime;
use Detection\MobileDetect;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class IndependantController
 * @package App\Controller
 * @Route("/independant",name="independant_")
 */
class IndependantController extends AbstractController
{
    /**
     * @Route("", name="list")
     */
    public function list(Request $request)
    {
        $detect = new MobileDetect();
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $indeRepo = $this->getDoctrine()->getRepository(Independant::class);
            if ($request->isXmlHttpRequest())
            {
                $indeList = $indeRepo->findAllArray();
                return new JsonResponse($indeList,200);

            }else{

                $indes = $indeRepo->findBy([],['nom'=>'ASC']);
                return $this->render('independant/indeList.html.twig', [
                    'indes' => $indes,
                    'mobile'=>($detect->isTablet() || $detect->isMobile()),
                ]);
            }
        }else{
            return $this->redirectToRoute('/');
            }
    }


    /**
     * @Route("/delete/{id}",name="delete",requirements={"id":"\d+"})
     */
    public function delete($id,EntityManagerInterface $manager,Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                $indeRepo = $this->getDoctrine()->getRepository(Independant::class);
                $inde = $indeRepo->find($id);
                if (isset($inde))
                {
                    $manager->remove($inde);
                    $manager->flush();
                    return new Response('Suppression ok',200);
                }else{
                    return new Response('Independant inconnu',404);
                }
            }else{
                return $this->redirectToRoute('independant_list');
            }
        }else{
            return $this->redirectToRoute('app_login');
        }

    }


    /**
     * @Route("/update",name="update")
     */
    public function update(Request $request, EntityManagerInterface $manager, ValidatorInterface $validator)
    {
            if ($request->isXmlHttpRequest()) {
                $encoders = [new JsonEncoder()];
                $normalizers = [new ObjectNormalizer()];
                $serializer = new Serializer($normalizers, $encoders);
                $indeModif = $serializer->deserialize($request->getContent(), Independant::class, 'json');

                $inderepo = $this->getDoctrine()->getRepository(Independant::class);
                $inde = $inderepo->find($indeModif->getId());
                if (isset($inde))
                {
                    $inde->setNom($indeModif->getNom());
                    $inde->setSalaire($indeModif->getSalaire());
                    $inde->setPrenom($indeModif->getPrenom());
                    //on modifie le cout des heures ulterieur a la date du jour
                    $errors = $validator->validate($inde);
                    if ($errors->count()>0)
                    {
                        //si les datas ne sont pas valides, on retourne les erreurs
                        $erreurs = $this->getErreursList($errors);
                        return new JsonResponse($erreurs,Response::HTTP_BAD_REQUEST);
                    }else{
                        $date = new DateTime();
                        foreach ($inde->getHeures() as $heure)
                        {
                            $sousProjet = $heure->getSousProjet();
                            if ($sousProjet->getDateAction()>=$date)
                            {
                                $heure->updateCout();
                            }
                        }
                        $manager->persist($inde);
                        $manager->flush();

                        return new Response();
                    }
                }else{
                    return new Response('Independant inconnu',Response::HTTP_BAD_REQUEST);
                }
            }else{
                return $this->redirectToRoute('independant_list');
            }

    }


    /**
     * @Route("/insert",name="insert")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param ValidatorInterface $validator
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function insert(Request $request, EntityManagerInterface $manager,ValidatorInterface $validator)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                $encoders = [new JsonEncoder()];
                $normalizers = [new ObjectNormalizer()];
                $serializer = new Serializer($normalizers, $encoders);
                $indeAjout = $serializer->deserialize($request->getContent(),Independant::class,'json');
                if (isset($indeAjout))
                {
                    $errors = $validator->validate($indeAjout);
                    if ($errors->count()>0)
                    {
                        //si les datas ne sont pas valides, on retourne les erreurs
                        $erreurs = $this->getErreursList($errors);
                        return new JsonResponse($erreurs,Response::HTTP_BAD_REQUEST);
                    }
                    $manager->persist($indeAjout);
                    $manager->flush();
                    //retourner id de inde en reponse en reponse
                    return new JsonResponse($indeAjout->getId(),Response::HTTP_OK);
                }
            }
        }
        return $this->redirectToRoute('app_login');
    }


    /**
     * @Route("/{id}",name="detail",requirements={"id":"\d+"})
     */
    public function detail($id,Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
               $indeRepo = $this->getDoctrine()->getRepository(Independant::class);
               $inde = $indeRepo->findArray($id);
               if (isset($inde))
               {
                return new JsonResponse($inde,200);
               }
            }
        }else{
            return $this->redirectToRoute('app_login');
        }
    }


    /**
     * @Route("/filtres",name="detail_type",methods={"POST"})
     */
    public function detailByFiltre(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $filtresReq = json_decode($request->getContent(), true);

            $indeRepo = $this->getDoctrine()->getRepository(Independant::class);
            //traitement date;
            $dateDebut = new DateTime();
            $dateDebut->setDate($filtresReq["year1"], $filtresReq["month1"], 1);
            $dateDebut->setTime(0, 0, 0, 0);
            $dateFin = new DateTime();
            $dateFin->setDate($filtresReq["year2"], $filtresReq["month2"], 1);
            $dateFin->setTime(0, 0, 0, 0);

          $liste = $indeRepo->findByFiltres($filtresReq,$dateDebut,$dateFin);
          if (!empty($liste))
          {
              $inde = $liste[0];
              $nbHeures = 0;
              $totaleCost =0;
              $nbProjets =0;
              $prixVente=0;
              foreach ($inde->getHeures() as $heure)
              {
                  $nbProjets++;
                  $nbHeures = $nbHeures + $heure->getHeure();
                  $totaleCost = $totaleCost + $heure->getCout();
                  $sousProjet = $heure->getSousProjet();
                  //calcul du prix de vente special inde : pvSpj - cout Autre inde - autres presta
                  $pv = $sousProjet->getPrixVente();
                  foreach ($sousProjet->getHeures() as $heureSpj)
                  {
                      //on enleve le cout des autres independants
                        if ($heureSpj->getIndependant() != $inde)
                        {
                            $pv = $pv - $heureSpj->getCout();
                        }
                  }
                  //on enleve le cout des prestations
                  foreach ($sousProjet->getAutrePrestations() as $prestation)
                  {
                      $pv = $pv - $prestation->getMontant();
                  }

                 $prixVente=$prixVente+$pv;
              }
              //eviter erreurs
              if ($prixVente<0)
              {
                  $prixVente=0;
              }
              //creation des infos à afficher
              $ficheInde = [
                  "id"=>$inde->getId(),
                  "nom"=>$inde->getNom(),
                  "prenom"=>$inde->getPrenom(),
                  "salaire"=>$inde->getSalaire(),
                  "nbHeures"=>$nbHeures,
                  "nbProjets"=>$nbProjets,
                  "totalCost"=>$totaleCost,
                  "pv"=>$prixVente,
                  "ratio"=>$totaleCost/$prixVente,
              ];
              return new JsonResponse($ficheInde);
          }else{
              return new JsonResponse($liste);
          }
        }
    }


    /**Traite la ConstraintViolationList pour retourner un tableau associatif
     * en key la propriete et en valeur le dernier message d'erreur de la proprietee
     *
     * @param ConstraintViolationListInterface $violations
     * @return array
     */
    function getErreursList(ConstraintViolationListInterface $violations)
    {
        $erreurs = [];
        foreach ($violations as $violation)
        {
            $erreurs[$violation->getPropertyPath()]= $violation->getMessage();
        }
        return $erreurs;
    }

}
