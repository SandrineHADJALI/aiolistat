<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Projet;

use DateTime;
use Detection\MobileDetect;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProjetController
 * @package App\Controller
 * @Route("/projet",name="projet_")
 */
class ProjetController extends AbstractController
{
    /**
     * @Route("", name="list")
     */
    public function list()
    {
        $detect = new MobileDetect();

        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $projetRepo = $this->getDoctrine()->getRepository(Projet::class);
            //traitement date;
            $dateDebut = new DateTime();
            $dateDebut->setDate($dateDebut->format('Y'),$dateDebut->format('m'),1);
            $dateDebut->setTime(0,0,0);
            $dateFin = new DateTime();
            $dateFin->setDate($dateFin->format('Y'),$dateFin->format('m'),1);
            $dateFin->setTime(0,0,0);
            $dateFin->modify('+6 months');

            $filtres=[
                "categorie"=>null,
                "perio"=>null,
                "independant"=>null,
                "mot-cle"=>null,
                "previsionnel"=>"all"
            ];


            $pMensuel = $projetRepo->findByFiltres($filtres,$dateDebut,$dateFin,'MENSUEL');
            $projetRepo->clear();
            $pOneShot = $projetRepo->findByFiltres($filtres,$dateDebut,$dateFin,'ONESHOT');
            $projetRepo->clear();
            $listeVide =$this->getProjetsVide();
            krsort($pMensuel);
            krsort($pOneShot);
            $projets = array('VIDE' => $listeVide,
                'MENSUEL' => $pMensuel,
                'ONESHOT' => $pOneShot)
            ;
            //recuperation des categories
            $cateRepo = $this->getDoctrine()->getRepository(Categorie::class);
            $categories = $cateRepo->findAll();

            return $this->render('projet/projetList.html.twig', [
                'tabProjets' => $projets,
                'categories'=>$categories,
                'mobile'=>($detect->isMobile() || $detect->isTablet()),
                'page'=>'projet'
            ]);
        }
        return $this->redirectToRoute('app_login');

    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     * @Route("/{id}",name="detail",requirements={"id":"\d+"})
     */
    public function detail($id, Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($request->isXmlHttpRequest()) {
                $pjRepo = $this->getDoctrine()->getRepository(Projet::class);
                $pj = $pjRepo->findOneInArray($id);

                if (isset($pj)) {
                    return new JsonResponse($pj);
                } else {
                    return new JsonResponse("Projet inconnu", Response::HTTP_NOT_FOUND);
                }

            }
        }
        return $this->redirectToRoute('app_login');
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     * @Route("/update",name="update")
     */
    public function update(Request $request, EntityManagerInterface $manager)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($request->isXmlHttpRequest()) {
                $pjrepo = $this->getDoctrine()->getRepository(Projet::class);
                $pjInfo = json_decode($request->getContent(), true);
                dump($request->getContent());
                $pj = $pjrepo->find($pjInfo["id"]);
                if (isset($pj)) {
                    $pj->setNom($pjInfo["nom"]);
                    $manager->persist($pj);
                    $manager->flush();
                    return new JsonResponse("Modifications effectuées avec succès");
                } else {
                    return new JsonResponse("Projet inconnu", Response::HTTP_NOT_FOUND);
                }

            }
        }
        return $this->redirectToRoute('app_login');
    }

    /**
     * @param $id
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/delete/{id}",name="delete",requirements={"id":"\d+"})
     */
    public function remove($id, Request $request, EntityManagerInterface $manager)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($request->isXmlHttpRequest()) {
                $pjRepo = $this->getDoctrine()->getRepository(Projet::class);
                $pj = $pjRepo->find($id);
                if (isset($pj)) {
                    $manager->remove($pj);
                    $manager->flush();
                    return new JsonResponse("Suppression effectuée avec succès");
                } else {
                    return new JsonResponse("Projet inconnu", Response::HTTP_NOT_FOUND);
                }
            }
        }
        return $this->redirectToRoute('app_login');
    }


    /**
     * @param $id
     * @param Request $request
     * @Route("/prix/{type}/{id}",name="prix_type",requirements={"id":"\d+", "type":"(?:ONE SHOT|MENSUEL)"})
     */
    public function getOneByPeriodicite($type, $id, Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($request->isXmlHttpRequest()) {
                $pjRepo = $this->getDoctrine()->getRepository(Projet::class);
                $pj = $pjRepo->findOneByPeriodicite($id, $type);
                if (isset($pj)) {
                    return new JsonResponse($pj);
                } else {
                    return new JsonResponse('Projet inconnu', Response::HTTP_NOT_FOUND);
                }
            }
        }
        return $this->redirectToRoute('app_login');
    }


    /**
     * @Route("/insert",name="insert")
     */
    public function insert(Request $request, EntityManagerInterface $manager)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($request->isXmlHttpRequest()) {

                $nom = json_decode($request->getContent(), true);
                $projet = new Projet();
                $projet->setNom($nom["nom"]);
                $manager->persist($projet);
                $manager->flush();
                return new JsonResponse($projet->getId());
            }
        }
        return $this->redirectToRoute('app_login');
    }


    /**
     * @param Request $request
     * @Route("/filtre",name="filtre")
     */
    public function projetsByFiltre(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $filtresReq = json_decode($request->getContent(), true);
            $projetRepo = $this->getDoctrine()->getRepository(Projet::class);
            //traitement date;
            $dateDebut = new DateTime();
            $dateDebut->setDate($filtresReq["year1"], $filtresReq["month1"], 1);
            $dateDebut->setTime(0,0,0,0);
            $dateFin = new DateTime();
            $dateFin->setDate($filtresReq["year2"], $filtresReq["month2"], 1);
            $dateFin->setTime(0,0,0,0);
            //chargement des listes selon periodicite
            $perio = $filtresReq["perio"];
            $listeOS=[];
            $listeM=[];
            //si filtre independant -> recuperation total heure de l'inde par projet
            $idInde = $filtresReq["independant"];
            if ($perio =="ONESHOT" || $perio==null)
            {
                $listeOS = $projetRepo->findByFiltres($filtresReq,$dateDebut,$dateFin,'ONESHOT');
                $listeOS =$this->getInfosBase($listeOS,$idInde);
            }
            if ($perio =="MENSUEL" || $perio==null)
            {
                $projetRepo->clear();
                $listeM = $projetRepo->findByFiltres($filtresReq,$dateDebut,$dateFin,'MENSUEL');
                $listeM=$this->getInfosBase($listeM,$idInde);
            }

            $projetList= [
                "MENSUEL"=>$listeM,
                "ONESHOT"=>$listeOS
            ];

            return new JsonResponse($projetList);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     * @Route("/filtre/{id}",name="filtre_one",requirements={"id":"\d+"})
     */
    public function oneByFiltre($id,Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $filtresReq = json_decode($request->getContent(), true);
            $projetRepo = $this->getDoctrine()->getRepository(Projet::class);
            //traitement date;
            $dateDebut = new DateTime();
            $dateDebut->setDate($filtresReq["year1"], $filtresReq["month1"], 1);
            $dateDebut->setTime(0,0,0,0);
            $dateFin = new DateTime();
            $dateFin->setDate($filtresReq["year2"], $filtresReq["month2"], 1);
            $dateFin->setTime(0,0,0,0);
            //chargement des listes selon periodicite
            $perio = $filtresReq["perio"];
            //si filtre independant -> recuperation total heure de l'inde par projet
            $idInde = $filtresReq["independant"];

            if ($perio =="ONESHOT" || $perio==null)
            {
                $projetOs = $projetRepo->findOneByFiltres($filtresReq,$dateDebut,$dateFin,'ONESHOT',$id);
                if (!empty($projetOs))
                {
                    $projetOs= $this->getInfosBaseOne($projetOs[0],$idInde);
                }

            }
            if ($perio =="MENSUEL" || $perio==null)
            {
                $projetRepo->clear();
                $projetM = $projetRepo->findOneByFiltres($filtresReq,$dateDebut,$dateFin,'MENSUEL',$id);
                if (!empty($projetM)) {
                    $projetM = $this->getInfosBaseOne($projetM[0],$idInde);
                }
            }


            $projetList= [
                "MENSUEL"=>$projetM,
                "ONESHOT"=>$projetOs
            ];

            return new JsonResponse($projetList);
        }
    }


    /**
     * @param Request $request
     * @Route("/vide",name="vide")
     */
    public function listeVide(Request $request)
    {
        if ($request->isXmlHttpRequest())
        {
            $listeVide = $this->getProjetsVide();
            $listeVide = $this->getInfosBase($listeVide);

            return new JsonResponse(array("VIDE"=>$listeVide));
        }
    }



    public function getInfosBase($liste,$idInde=null)
    {
        $projetList =[];
        foreach ($liste as $projet)
        {
            $projetList[] =$this->getInfosBaseOne($projet,$idInde);
        }
        return $projetList;
    }



    public function getInfosBaseOne($projet,$idInde=null)
    {
        if (isset($idInde))
        {
            //si filtre independant
            //calcul du nombre d'heures de l'independant sur le projet
            $heures =0;
            $cost =0;
            $costAutre=0;
            foreach ($projet->getSousProjets() as $sousProjet)
            {
                foreach ($sousProjet->getHeures() as $heure)
                {
                    if ($heure->getIndependant()->getId()==$idInde)
                    {
                        $heures+=$heure->getHeure();
                        $cost+=$heure->getCout();
                    }else{
                        $costAutre+=$heure->getCout();
                    }
                }
            }
        }else{
            //si pas de filtre independant
            //heure==null
            $heures=null;
            $cost=null;
            $costAutre=null;
        }


        return array(
            "id" => $projet->getId(),
            "nom" => $projet->getNom(),
            "roi" => $projet->getRoi(),
            "pv" => $projet->getPrixVente(),
            "cout"=>$projet->getCoutTotal(),
            "nbSousProjets"=>count($projet->getSousProjets()),
            "heuresInde"=>$heures,
            "idInde"=>$idInde,
            "coutInde"=>$cost,
            "coutAutre"=>$costAutre,
            "pvPrevi"=> $projet->getPrixVentePrevi(),
            "coutPrevi"=> $projet->getCoutTotalPrevi(),
            "roiPrevi"=>$projet->getRoiPrevi(),
            "pvNonPrevi"=> $projet->getPrixVenteNonPrevi(),
            "coutNonPrevi"=> $projet->getCoutTotalNonPrevi(),
            "roiNonPrevi"=>$projet->getRoiNonPrevi(),
        );
    }



    public function getProjetsVide()
    {
        $projetRepo=$this->getDoctrine()->getRepository(Projet::class);
        $listeAll = $projetRepo->findAll();
        $listeVide = [];
        foreach ($listeAll as $projet)
        {
            if (count($projet->getSousProjets())===0)
            {
                $listeVide[]=$projet;
            }
        }
        return $listeVide;
    }
}
