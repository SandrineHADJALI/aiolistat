<?php

namespace App\Controller;

use App\Entity\AutrePrestation;
use App\Entity\Categorie;
use App\Entity\Heure;
use App\Entity\Independant;
use App\Entity\Projet;
use App\Entity\SousProjet;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @Route("/sousProjet", name="sousProjet_")
 */
class SousProjetController extends AbstractController
{

    /**
     * @Route("/{type}/{idProjet}",name="detail_type",methods={"GET"}, requirements={"idProjet":"\d+", "type":"(?:ONESHOT|MENSUEL)"})
     * @param $type
     * @param $idProjet
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
     public function detailByType($type,$idProjet,Request $request)
     {
         if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
         {
                if ($request->isXmlHttpRequest()) {
                    $spjRepo = $this->getDoctrine()->getRepository(SousProjet::class);
                    $sp = $spjRepo->findInArray($idProjet,$type);
                    if (isset($sp))
                    {
                        return new JsonResponse($sp);
                    }else{
                        return  new JsonResponse("Sous-projet inconnu",Response::HTTP_NOT_FOUND);
                    }

                }
         }
             return $this->redirectToRoute('app_login');
     }



    /**
     * @Route("/{type}/{idProjet}",name="detail_type",methods={"POST"}, requirements={"idProjet":"\d+", "type":"(?:ONESHOT|MENSUEL)"})
     * @param $type
     * @param $idProjet
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function detailByFiltre($type,$idProjet,Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $filtresReq = json_decode($request->getContent(), true);

            $spjRepo = $this->getDoctrine()->getRepository(SousProjet::class);
            //traitement date;
            $dateDebut = new DateTime();
            $dateDebut->setDate($filtresReq["year1"], $filtresReq["month1"], 1);
            $dateDebut->setTime(0, 0, 0, 0);
            $dateFin = new DateTime();
            $dateFin->setDate($filtresReq["year2"], $filtresReq["month2"], 1);
            $dateFin->setTime(0, 0, 0, 0);
            $liste = $spjRepo->findByFiltre($idProjet, $type, $filtresReq, $dateDebut, $dateFin);

            /*Si filtres independant, on supprime les sous-projets oùu l'independant n'est pas present
             * tout en gardant les autres independants dans les autres sous-projets
             */
            if (isset($filtresReq["independant"])) {
                //liste d'elements a supprimer
                $aEffacer = [];
                foreach ($liste as $sousProjet) {
                    //parcours les sous-projets
                    $heures = $sousProjet["heures"];
                    $valid = false;
                    foreach ($heures as $heure)
                    {
                        if ($heure["independant"]["id"]==$filtresReq["independant"])
                        {
                            $valid=true;
                        }
                    }
                    if (!$valid)
                    {
                        $aEffacer[]=$sousProjet;
                    }
                }
                //parcours de la liste des elements à supprimer
                foreach ($aEffacer as $efface)
                {
                    //suppression d'un elemnt du tableau
                    unset($liste[array_search($efface,$liste)]);
                }
                //replace les index, permet de renvoyer un tableau et pas un objet pour interpretation JSON
                $liste=array_values($liste);
            }

            if (isset($liste))
            {
                return new JsonResponse($liste);
            }else{
                return  new JsonResponse("Sous-projet inconnu",Response::HTTP_NOT_FOUND);
            }
        }
    }




    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     * @Route("/{id}",name="detail",requirements={"id":"\d+"})
     */
    public function detail($id,Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            if ($request->isXmlHttpRequest())
            {
                $spjRepo = $this->getDoctrine()->getRepository(SousProjet::class);
                $spj = $spjRepo->findOneInArray($id);
                if (isset($spj))
                {
                    return new JsonResponse($spj);
                }else{
                    return  new JsonResponse("Sous-projet inconnu",Response::HTTP_NOT_FOUND);
                }
            }
        }else{
            return $this->redirectToRoute('app_login');
        }
    }


    /**
     * @param $id
     * @param Request $request
     * @Route("/delete/{id}",name="delete",requirements={"id":"\d+"})
     */
    public function delete($id,Request $request,EntityManagerInterface $manager)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $spjRepo = $this->getDoctrine()->getRepository(SousProjet::class);
            if ($request->isXmlHttpRequest())
            {
                $spj = $spjRepo->find($id);
                if (isset($spj))
                {
                    $manager->remove($spj);
                    $manager->flush();
                    return new JsonResponse("Suppression effectuée avec succès");
                }else{
                    return new Response("Sous-Projet inconnu",Response::HTTP_NOT_FOUND);
                }
            }
        }
        return $this->redirectToRoute('app_login');
    }


    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/insert",name="insert")
     */
    public function insert(Request $request,EntityManagerInterface $manager)
    {
            if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            {

                if ($request->isXmlHttpRequest())
                {
                    $projetRepo = $this->getDoctrine()->getRepository(Projet::class);
                    $caterepo = $this->getDoctrine()->getRepository(Categorie::class);
                    $indeRepo = $this->getDoctrine()->getRepository(Independant::class);
                    //recuperation et separation des data de la requete ajax
                    $infos = json_decode($request->getContent(),true);

                    $spjInfo =$infos["infos"];
                    $heures =$infos["autres"]["heures"];
                    $prestations =$infos["autres"]["prestations"];
                    //recuperation du projet
                    $projet = $projetRepo->find($spjInfo["projetId"]);
                    //recuperation categorie
                    $categorie = $caterepo->find($spjInfo["categorieId"]);
                    if (isset($categorie))
                    {
                        if (isset($projet))
                        {
                            $spj = new SousProjet();
                            //traitement date
                            $date = new DateTime();
                            $date->setDate($spjInfo["year"],$spjInfo["month"],1);
                            $spj->setDateAction($date);
                            $spj->setIsPrevisionnel($spjInfo["isPrevisionnel"]);
                            $spj->setCategorie($categorie);
                            $spj->setProjet($projet);
                            $spj->setPrixVente($spjInfo["pv"]);
                            $spj->setDesignation($spjInfo["designation"]);
                            $spj->setPeriodicite($spjInfo["periodicite"]);
                            $spj->setCout(0);
                            //gestion des heures
                            if (isset($heures))
                            {
                                foreach ($heures as $heureInfo)
                                {
                                    $heure = new Heure();
                                    $inde = $indeRepo->find($heureInfo["idInde"]);
                                    if (isset($inde))
                                    {
                                        $heure->setIndependant($inde);
                                        $heure->setHeure($heureInfo["heure"]);
                                        $heure->setCommentaire($heureInfo["commentaire"]);
                                        $heure->updateCout();
                                        $heure->setSousProjet($spj);
                                        $spj->addHeure($heure);

                                    }
                                }
                            }

                            //gestion des prestations
                            if (isset($prestations))
                            {
                                foreach ($prestations as $pInfo)
                                {
                                    $presta = new  AutrePrestation();
                                    $presta->setDetail($pInfo["detail"]);
                                    $presta->setMontant($pInfo["montant"]);
                                    $presta->setSousProjet($spj);
                                    $spj->addAutrePrestation($presta);

                                }
                            }
                            $manager->persist($spj);
                            $manager->flush();

                            return new JsonResponse($spj->getId());
                        }else{
                            return  new JsonResponse("Projet inconnu",Response::HTTP_NOT_FOUND);
                        }
                    }else{
                        return new JsonResponse("Catégorie inconnue",Response::HTTP_NOT_FOUND);
                    }
                }
            }
            return $this->redirectToRoute('app_login');
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @Route("/update/{id}",name="update",requirements={"id":"\d+"} )
     */
    public function update($id,Request $request,EntityManagerInterface $manager)
    {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            if ($request->isXmlHttpRequest()) {
                $caterepo = $this->getDoctrine()->getRepository(Categorie::class);
                $spjRepo = $this->getDoctrine()->getRepository(SousProjet::class);
                $spjInfo = json_decode($request->getContent(), true);

                //recuperation categorie
                $categorie = $caterepo->find($spjInfo["categorieId"]);
                if (isset($categorie)) {

                    $spj = $spjRepo->find($id);
                    if (isset($spj))
                    {
                        //traitement date
                        $date = new DateTime();
                        $date->setDate($spjInfo["year"], $spjInfo["month"], 1);
                        $spj->setCategorie($categorie);
                        $spj->setPrixVente($spjInfo["pv"]);
                        $spj->setIsPrevisionnel($spjInfo["isPrevisionnel"]);
                        $spj->setPeriodicite($spjInfo["periodicite"]);
                        $spj->setDesignation($spjInfo["designation"]);
                        $spj->setDateAction($date);
                        $manager->persist($spj);
                        $manager->flush();

                        return new JsonResponse();
                    }else{
                        return new JsonResponse("Sous-projet inconnu",Response::HTTP_NOT_FOUND);
                    }
                } else {
                    return new JsonResponse("Catégorie inconnu", Response::HTTP_NOT_FOUND);
                }
            }
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/list",name="list")
     */
    public function listByFiltre(Request $request)
    {
        $filtresReq = json_decode($request->getContent(), true);

        $spjRepo = $this->getDoctrine()->getRepository(SousProjet::class);
        //traitement date;
        $dateDebut = new DateTime();
        $dateDebut->setDate($filtresReq["year1"], $filtresReq["month1"], 1);
        $dateDebut->setTime(0, 0, 0, 0);
        $dateFin = new DateTime();
        $dateFin->setDate($filtresReq["year2"], $filtresReq["month2"], 1);
        $dateFin->setTime(0, 0, 0, 0);
        $perio = $filtresReq["perio"];
        if ($perio =="ONESHOT" || $perio==null)
        {
            //on recuperes la liste des soous-projet par periodicite
            $listeOs = $spjRepo->findAllByFiltre('ONESHOT',$filtresReq, $dateDebut, $dateFin);
            if (!empty($listeOs))
            {
                // on trie la liste par id de projet
                $listeOs=$this->repartirParProjet($listeOs);
                foreach ($listeOs as $projetId => $listeSpjOS )
                {
                    //on recupere les prix et cout par categorie
                    $listeOs[$projetId]= $this->traitementCategories($listeSpjOS);
                }
                //en sortie on a une liste par periodicite par id du projet avec les prix et couts par categories
            }
        }else{
            $listeOs=[];
        }
        if ($perio =="MENSUEL" || $perio==null)
        {
            $spjRepo->clear();
            $listeM = $spjRepo->findAllByFiltre('MENSUEL',$filtresReq, $dateDebut, $dateFin);
            if (!empty($listeM)) {
                // on trie la liste par id de projet
                $listeM=$this->repartirParProjet($listeM);
                foreach ($listeM as $projetId => $listeSpjM )
                {
                    //on recupere les prix et cout par categorie
                    $listeM[$projetId]= $this->traitementCategories($listeSpjM);

                }
                //en sortie on a une liste par periodicite par id du projet avec les prix et couts par categories
            }
        }else{
            $listeM=[];
        }
        $projetList= [
            "MENSUEL"=>$listeM,
            "ONESHOT"=>$listeOs,
        ];

        return new JsonResponse($projetList);
    }

    /**calcule le sprix de vente et les couts par categorie
     * retourne la liste de chaques pv et cout par categorie
     * @param $liste
     * @return array
     */
    public function traitementCategories($liste)
    {
        $pv1=0;
        $cost1=0;
        $pv2=0;
        $cost2=0;
        $pv3=0;
        $cost3=0;
        $pv4=0;
        $cost4=0;
        $pv5=0;
        $cost5=0;
        $pv6=0;
        $cost6=0;
        foreach ($liste as $spj)
        {
            $categorie = $spj["categorie"];
            switch ($categorie["id"])
            {
                case 1:
                    $pv1 += $spj["prixVente"];
                    $cost1 += $spj["cout"];
                    break;
                case 2:
                    $pv2 += $spj["prixVente"];
                    $cost2 += $spj["cout"];
                    break;
                case 3:
                    $pv3 += $spj["prixVente"];
                    $cost3 += $spj["cout"];
                    break;
                case 4:
                    $pv4 += $spj["prixVente"];
                    $cost4 += $spj["cout"];
                    break;
                case 5:
                    $pv5 += $spj["prixVente"];
                    $cost5 += $spj["cout"];
                    break;
                case 6:
                    $pv6 += $spj["prixVente"];
                    $cost6 += $spj["cout"];
                    break;
            }
        }

        return [
            "pv1"=>$pv1,
            "cost1"=>$cost1,
            "pv2"=>$pv2,
            "cost2"=>$cost2,
            "pv3"=>$pv3,
            "cost3"=>$cost3,
            "pv4"=>$pv4,
            "cost4"=>$cost4,
            "pv5"=>$pv5,
            "cost5"=>$cost5,
            "pv6"=>$pv6,
            "cost6"=>$cost6,
        ];
    }


    /**Trier la liste par projet
     * @param $liste
     */
    public function repartirParProjet($liste)
    {
        $newListe=[];
        foreach ($liste as $spj)
        {
            $projet = $spj["projet"];
            if (array_key_exists($projet["id"],$newListe))
            {
                $newListe[$projet["id"]][] =$spj;
            }else{
                $newListe[$projet["id"]]=[];
                $newListe[$projet["id"]][] =$spj;
            }
        }
        return $newListe;
    }
}

