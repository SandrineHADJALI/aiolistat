<?php

namespace App\Form;

use App\Entity\Heure;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('heure')
//            ->add('cout')
            ->add('commentaire',TextareaType::class,[
                'label'=>'commentaire',
                'attr'=>[
                    'required'=>false
                ],
            ])
//            ->add('sousProjet')
//            ->add('independant')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Heure::class,
        ]);
    }
}
