<?php

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\SousProjet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Validator\Constraints\DateTime;

class SousProjetFiltreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateAction')
            ->add('isPrevisionnel')
            ->add('categorie',EntityType::class,array('class' => Categorie::class,'choice_label'=>'libelle'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SousProjet::class,
        ]);
    }
}
