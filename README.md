# Aïoli Statistiques

### Table des matières 
1. [Clonage du projet](#Clonage-du-projet)
2. [Initialisation du projet](#Initialisation-du-projet)
3. [Xampp et base de données](#Xampp-et-base-de-données)
4. [Virtual host](#Virtual-host)

***
Développement de la page "Statistiques" pour l'agence Aïoli. 
Début du projet : 13/04/2021.

Les technologies utilisées sont : HTML, CSS, PHP

Avec les frameworks : Symfony et Bootstrap.
***
# Clonage du projet

Vous pouvez utiliser gitlab pour obtenir le lien du clone ou bien le lien ci-dessous :

https://gitlab.com/SandrineHADJALI/aiolistat.git

Attention, vous devez extraire les éléments du zip (build.zip) dans le répertoire "public". 

Vous aurez normalement le chemin suivant : ```/public/build/```

# Initialisation du projet
### Installation Node Module

Mettez vous à la racine du projet.
Vous allez devoir installer les modules et dépendances en exécutant les commandes suivantes.


```npm install```

Désormais il faut installer composer dans le répertoire.

``` 
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```
### Installation du repertoire Vendor

Nous allons installer le répertoire Vendor avec ces deux commande :

```composer update```

```php bin/console doctrine:schema:update```

# Xampp et base de données 

Lancer Apache et Mysql sur le tableau de commande Xampp.

Aller sur : http://localhost/phpmyadmin/

Créer une nouvelle base de données et y mettre les données du fichier .sql (SQL/jcaezalaioli.sql)

Changer le fichier .env (.env) en fonction de base de données créées.

# Virtual host 
Se reférer à ce lien : https://fallinov.medium.com/cr%C3%A9er-un-virtual-host-7ccd8a51cb6d
### Ajout d'un domaine 

Penser à modifier le fichier hosts en tant qu'administrateur, en ouvrant par exemple le bloc-notes en administrateur.

Ce fichier se situe sur windows au chemin : ```C:\windows\system32\drivers\etc\hosts```

Ajouter la ligne suivante à la fin du fichier host : ```127.0.0.1 app.aioli-digital.local```

### Ajout d'un virtual host sur Xampp

Désormais nous allons nous occupé de la configuration de Xampp.

Modifier le fichier : ```C:\xampp\apache\conf\extra\httpd-vhosts.conf```

En ajoutant les lignes suivantes à la fin du fichier :
```
NameVirtualHost *
<VirtualHost *>
    DocumentRoot "C:\xampp\htdocs"
    ServerName localhost
</VirtualHost>

<VirtualHost *>
    DocumentRoot "C:\xampp\htdocs\php\aioli-management\public"
    ServerName app.aioli-digital.local
    
    <Directory "C:\xampp\htdocs\php\aioli-management\public">
        Order allow,deny
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>
```
Attention à bien prendre le même nom de domaine ainsi que le bon chemin pour le répertoire public (Ligne "DocumentRoot" et "Directory").

Pensez à relancer le server Apache sur Xampp.
