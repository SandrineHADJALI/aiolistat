import * as Utils from './utils/utils';
import * as Projet from './projet';


export function trier(event)
{
    event.stopPropagation();
    event.preventDefault();
    //recuperation des informations pour le tris
    let target=event.currentTarget;
    let name = target.dataset.name;
    let columnNum = parseInt(target.dataset.column);
    let type=target.dataset.type;
    let periodicite = target.dataset.periodicite;
    let sort = parseInt(target.dataset.sort);
    //les differents noms possibles
    let names = ['nom','prix-vente','roi'];
    //on recupere les lignes des projets
    let lignes = document.querySelectorAll('.ligne_'+periodicite);
    //on recupere le body
    let tBody = document.getElementById('tBody_'+periodicite);
    let tab=[];
    //on ajoute la valeur de la cellule dans le tableau, pour trie ultérieur
    lignes.forEach(function (ligne) {
        tab.push(ligne.children[columnNum].getAttribute('data-'+name).toLowerCase());
    })
    //on trie le tableau des elements
    trieTabByType(type,tab)
    // action a faire selon ordre de tris
    if (sort === 1)
    {
        //tris decroissant
        target.dataset.sort =0;
        //changement d'icon
        document.getElementById(name+'_'+periodicite+'_down').style.display='none';
        document.getElementById(name+'_'+periodicite+'_up').style.display='';
    }else{
        tab.reverse();
        target.dataset.sort=1;
        //changement d'icon
        document.getElementById(name+'_'+periodicite+'_up').style.display='none';
        document.getElementById(name+'_'+periodicite+'_down').style.display='';
    }
    for (let nameElement of names) {
        if (nameElement !== name)
        {
            //on masque les autres icons
            document.getElementById(nameElement+'_'+periodicite+'_up').style.display='none';
            document.getElementById(nameElement+'_'+periodicite+'_down').style.display='none';
        }
    }
    //on remplace les elements du tableau par les lignes correspondantes
    lignes.forEach(function (ligne) {

        let data = ligne.children[columnNum].getAttribute('data-'+name).toLowerCase();
        let index = tab.indexOf(data);
        tab[index]=ligne;
        //gestion des sous-projets
        let idProjet = Utils.getId(ligne.id);
        //recuperation des lignes des sous-projets
        let spjLignes = document.querySelectorAll('.spj_'+periodicite+'_'+idProjet);
        for (let i =0;i<spjLignes.length;i++)
        {
            //insertions des lignes de sous-projets dans le tableau sans suppression de projets
            tab.splice(index+1+i,0,spjLignes[i]);
        }
    })
    //parcours le tableau
    for (let projet of tab) {
        //laliste des lignes dans le body sont modifier
        tBody.appendChild(projet);
    }
}



export function trierSousProjets(event)
{
    event.preventDefault();
    event.stopPropagation();
    let btnTrie = event.currentTarget;
    let btnAutre;
    let idProjet = Utils.getId(btnTrie.id);
    let type = btnTrie.dataset.type;
    let periodicite = Projet.getPeriodicite(btnTrie.id);
    let sort = parseInt(btnTrie.dataset.sort);
    //on recupere l'autre bouton filtre
    if (btnTrie.id ==='trie_alpha_spj_'+periodicite+'_'+idProjet)
    {
        btnAutre = document.getElementById('trie_numeric_spj_'+periodicite+'_'+idProjet);
    }else{
        btnAutre = document.getElementById('trie_alpha_spj_'+periodicite+'_'+idProjet);
    }
    //on change son data-sort
    btnAutre.dataset.sort =0;
    //on modifie l'icon du bouton
    if (type==='text')
    {
        btnAutre.innerText='';
        btnAutre.appendChild(Utils.getIconNumeric('up'));
        let type = 'up'
        if (sort===0)
        {
           type = 'down';
        }
        btnTrie.innerText='';
        btnTrie.appendChild(Utils.getIconAlpha(type));
    }else {
        btnAutre.innerText='';
        btnAutre.appendChild(Utils.getIconAlpha('up'));
        let type = 'up'
        if (sort===0)
        {
            type = 'down';
        }
        btnTrie.innerText='';
        btnTrie.appendChild(Utils.getIconNumeric(type));
    }
    //recuperation des lignes sous-projet hors lignes des formulaires
    let sousProjets = document.querySelectorAll('.spj_'+periodicite+'_'+idProjet+':not(form_spj_'+periodicite+'_'+idProjet+')');
    //On creer une instance de tableau via la nodeList
    let tab = Array.from(sousProjets);
    //trie du tableau
    tab.sort(function (a,b) {
      if (type==='text')
      {
          let valueA = document.querySelector('#design_spj_'+Utils.getId(a.id)).innerText;
          let valueB = document.querySelector('#design_spj_'+Utils.getId(b.id)).innerText;
          return valueA.localeCompare(valueB);
      }else{
          let tabA = document.querySelector('#date_spj_'+Utils.getId(a.id)).innerText.split('/');
          let tabB = document.querySelector('#date_spj_'+Utils.getId(b.id)).innerText.split('/');
          let monthA= parseInt(tabA[0]);
          let yearA= parseInt(tabA[1]);
          let monthB= parseInt(tabB[0]);
          let yearB= parseInt(tabB[1]);
          if (yearA<yearB)
          {
              return -1;
          }else if (yearA>yearB)
          {
              return 1;
          }else{
              //meme annee
              if (monthA<monthB)
              {
                  return -1;
              }else if (monthA>monthB)
              {
                  return 1;
              }else{
                  let valueA = document.querySelector('#design_spj_'+Utils.getId(a.id)).innerText;
                  let valueB = document.querySelector('#design_spj_'+Utils.getId(b.id)).innerText;
                  return valueA.localeCompare(valueB);
              }
          }
      }
    })


    //si ordre inverse
    if (sort===0)
    {
        sort=1;
        tab.reverse();
    }else{
        sort =0;
    }
    // insertion des lignes apres dernier formulaire ou apres ligne projet
    let forms = document.querySelectorAll('form_spj_'+periodicite+'_'+idProjet);
    let ligneOuInsert = document.getElementById('projet_'+periodicite+'_'+idProjet);
    if (forms.length>0)
    {
        ligneOuInsert =forms.lastItem;
    }
    //suppression des elements anciens
    sousProjets.forEach(function (element) {
        element.remove();
    })
    // ajout des elemnts trier
    tab.forEach(function (tr) {
        ligneOuInsert.insertAdjacentElement('afterend',tr);
    })
    btnTrie.dataset.sort = sort;
}




function trieTabByType(type,tab)
{
    if (type==='number')
    {
        tab.sort(function (a,b) {
            let numA =parseFloat(a);
            let numB = parseFloat(b);
            return numA-numB;
        });
    }else if (type==='date')
    {
        tab.sort(function (a,b) {
            let tabA = a.split('/');
            let tabB = b.split('/');
            let monthA= parseInt(tabA[0]);
            let yearA= parseInt(tabA[1]);
            let monthB= parseInt(tabB[0]);
            let yearB= parseInt(tabB[1]);
            if (yearA<yearB)
            {
                return -1;
            }else if (yearA>yearB)
            {
                return 1;
            }else{
                //meme annee
                if (monthA<monthB)
                {
                    return -1;
                }else if (monthA>monthB)
                {
                    return 1;
                }else{
                    return 0;
                }
            }
        });
    }else {
        tab.sort();
    }
}

