import * as Utils from './utils/utils';
import * as Categorie from './ajax/categorie_ajax'
import * as Independant from './ajax/independant_ajax';
import * as ProjetA from './ajax/projet_ajax';
import {isEmptyString} from "./utils/utils";
import * as Projet from './projet';

window.addEventListener("load",chargerFiltre);

function chargerFiltre() {
    chargerCategories();
    chargerPeriodicite();
    chargerIndependant();
    chargerMonthAndYear();
    //remettre selected de base lors du rafraichissement de la page
    document.getElementById('previsionnel').value='all';
    document.getElementById('form-filtre').addEventListener("submit",filtre);
    document.getElementById('3months').addEventListener("click", afficherTroisMois);
    document.getElementById('visiPrix').addEventListener("change", prixDisplayNone);

    let filtres = getFiltreValues();
    enregistrerFiltre(filtres);
}



function chargerCategories()
{
    Categorie.loadCategories().then((categories)=>{
        let selectCate = Utils.getSelectCategorie(categories,'categorie',1);
        selectCate.classList.add('input-filtres');
        let option = document.createElement("option");
        option.value = 0;
        option.innerText='Toutes les catégories';
        selectCate.add(option,0);
        selectCate.value=0;
        let selectCategorie = document.getElementById('categorie');
        selectCategorie.replaceWith(selectCate);
    })
}

function chargerPeriodicite()
{
        let selectPerio = Utils.getSelectPeriodicite(null,'MENSUEL');
        selectPerio.classList.add('input-filtres');
        selectPerio.id='perio';
        let option = document.createElement("option");
        option.value = 0;
        option.innerText='Toutes les périodicitées';
        selectPerio.add(option,0);
        selectPerio.value=0;
        let select = document.getElementById('perio');
        select.replaceWith(selectPerio);
}

function chargerIndependant()
{
    Independant.loadListInde().then((indeList)=>{
        let selectInde = Utils.getSelectInde(indeList);
        selectInde.className='input-filtres custom-select form-control w-auto';
        selectInde.id='inde';
        let option = document.createElement("option");
        option.value = 0;
        option.innerText='Tous les indépendants';
        selectInde.add(option,0);
        selectInde.value=0;
        let select = document.getElementById('inde');
        select.replaceWith(selectInde);
    })
}

function chargerMonthAndYear()
{
    let date = new Date();
    let month1 = Utils.getSelectNumber(date.getMonth()+1,1,13,'month-begin');
    let month2Value =getMonth2Value();
    let month2 = Utils.getSelectNumber(month2Value,1,13,'month-end');
    document.getElementById('month-begin').replaceWith(month1);
    document.getElementById('month-end').replaceWith(month2);
    chargerYear(date.getMonth()+1,month2Value);
}

function getMonth2Value(month1=null,increment=null)
{
    if (month1 == null)
    {
        month1 = new Date();
    }
    if (increment==null)
    {
        increment=4;
    }
    let month2 = month1.getMonth()+increment;
    if (month2>12)
    {
        month2 = month2-12;
    }
    return month2;
}

function chargerYear(month1Value,month2Value)
{
    let date = new Date();
    let year1 = Utils.getSelectNumber(date.getFullYear(),2015,2100,'year-begin');
    let year2 = Utils.getSelectNumber(date.getFullYear(),2015,2100,'year-end');
    year2 = Utils.getSelectNumber(getYear2Value(month1Value,month2Value),2015,2100,'year-end');
    year2.classList.add('input-filtres');
    document.getElementById('year-begin').replaceWith(year1);
    document.getElementById('year-end').replaceWith(year2);
}

function getYear2Value(month1Value,month2Value)
{
    let date = new Date();
    let year2Value
    if (month1Value>month2Value)
    {
        return date.getFullYear()+1
    }else{
        return date.getFullYear();
    }
}

function filtre(event)
{
    event.preventDefault();
    let filtres = getFiltreValues();
    if (filtres!= null)
    {
        ProjetA.getProjetByFiltres(filtres).then((projetList)=>{
                enregistrerFiltre(filtres);
                Projet.refreshProjetLis(projetList);

                if (Utils.isMobile())
                {
                    document.getElementById('les-filtres').style.display='none';
                    document.getElementById('div-general').style.display='';
                }
        })
        prixDisplayNone();
    }
}

function enregistrerFiltre(filtres) {
    let divFiltre = document.getElementById('filtres');
    for (const [key, value] of Object.entries(filtres)) {
        divFiltre.setAttribute('data-'+key,value);
    }
}

export function recupererFiltresEnCours()
{
    let divFiltre = document.getElementById('filtres');
    return {
        "categorie":setNull(divFiltre.dataset.categorie),
        "perio":setNull(divFiltre.dataset.perio),
        "independant":setNull(divFiltre.dataset.independant),
        "mot-cle":setNull(divFiltre.dataset.motCle),
        "month1":parseInt(divFiltre.dataset.month1),
        "year1":parseInt(divFiltre.dataset.year1),
        "month2":parseInt(divFiltre.dataset.month2),
        "year2":parseInt(divFiltre.dataset.year2),
        "previsionnel": (divFiltre.dataset.previsionnel),
    };
}

function afficherTroisMois(event)
{
    event.preventDefault();
    let date = new Date();
    let month2 = getMonth2Value();
    let filtres = {
        "categorie":null,
        "perio":null,
        "independant":null,
        "mot-cle":null,
        "month1":date.getMonth()+1,
        "year1":date.getFullYear(),
        "month2":month2,
        "year2":getYear2Value(date.getMonth()+1,month2),
        "previsionnel":"all",
    };
    enregistrerFiltre(filtres);
    ProjetA.getProjetByFiltres(filtres).then((projetList)=>{
        modifierFiltres(filtres);
        Projet.refreshProjetLis(projetList);
    })
    prixDisplayNone()
}

function modifierFiltres(filtres)
{
    let selectInde = document.getElementById('inde');
    selectInde.value=0;
    let selectPerio = document.getElementById('perio');
    selectPerio.value=0;
    let selectCate=document.getElementById('categorie');
    selectCate.value=0;
    let selectM1 =document.getElementById('month-begin');
    selectM1.value=filtres.month1;
    let selectM2 = document.getElementById('month-end');
    selectM2.value=filtres.month2;
    let selectY1 = document.getElementById('year-begin');
    selectY1.value=filtres.year1;
    let selectY2 =document.getElementById('year-end');
    selectY2.value=filtres.year2;
    let motCle = document.getElementById('mot-cle');
    motCle.value="";
    motCle.setAttribute('placeholde','Recherche par mos-clés');
    let selectPrevi = document.getElementById('previsionnel');
    selectPrevi.value=filtres.previsionnel;
}



export function getFiltreValues()
{
    if (valideDate())
    {

        return {
            "categorie":setNull(document.getElementById('categorie').value),
            "perio":setNull(document.getElementById('perio').value),
            "independant":setNull(document.getElementById('inde').value),
            "mot-cle":setNull(document.getElementById('mot-cle').value.trim()),
            "month1":parseInt(document.getElementById('month-begin').value),
            "year1":parseInt(document.getElementById('year-begin').value),
            "month2":parseInt(document.getElementById('month-end').value),
            "year2":parseInt(document.getElementById('year-end').value),
            "previsionnel":document.getElementById('previsionnel').value,
        };
    }else{
        return null
    }

}

function valideDate()
{
    let month1 = parseInt(document.getElementById('month-begin').value);
    let month2= parseInt(document.getElementById('month-end').value);
    let year1 = parseInt(document.getElementById('year-begin').value);
    let year2 = parseInt(document.getElementById('year-end').value);
    let valid = true;
   if (month2<month1 && year2===year1)
   {
       valid=false;
   }
   if (year1>year2)
   {
       valid=false;
   }
    return valid;
}

function setNull(value)
{
    if ( value == 0 ||  isEmptyString(value) || value==='null')
    {
        value=null;
    }
    return value;
}


export function needDisplayNone()
{
    let filtres = document.getElementById('filtres');
    let independant = setNull(filtres.dataset.independant);

    if (independant !== null)
    {

        afficherFicheInde();
    }else{
        //on masque la fiche Inde
        document.getElementById('fiche-inde').style.display="none";
    }

}


function afficherFicheInde()
{
    let visiPrix = document.getElementById('visiPrix');
    let filtres = getFiltreValues();
    Independant.loadByFiltres(filtres).then((ficheInde)=>{
        if (ficheInde!==null && ficheInde.length!==0)
        {
            modifierFicheInde(ficheInde);
           // if(visiPrix.checked===false) {
                //affichage de la fiche
                document.getElementById('fiche-inde').style.display = "";
          //  }
        }else{
           // if(visiPrix.checked===false) {
                document.getElementById('fiche-inde').style.display = "none";
        //    }
        }

    })
}



export function modifierFicheInde(ficheInde)
{
    if (ficheInde.nom!==undefined && ficheInde.nom !== null )
    {
        let spanNomInde = document.getElementById("nom-inde-periode");
        spanNomInde.innerText= Utils.capitalize(ficheInde.prenom)+' '+Utils.capitalize(ficheInde.nom);
    }
    let spanNbProjets = document.getElementById('nb-projets-periode');
    spanNbProjets.innerText = ficheInde.nbProjets;
    let spanNbHeure = document.getElementById('heure-periode');
    spanNbHeure.innerText=ficheInde.nbHeures+' h';
    let spanCout = document.getElementById('cout-periode');
    spanCout.innerText=Utils.formatEuro(ficheInde.totalCost);
    let pv = document.getElementById('pv-periode');
    pv.innerText=Utils.formatEuro(ficheInde.pv);
    let ratio = document.getElementById('ratio-periode');
    ratio.classList.add('percent-text')
    Utils.setClassRoi(ficheInde.ratio,ratio)
}

function prixDisplayNone(event)
{
    if (event != null) event.preventDefault();
    let visiPrix = document.getElementById('visiPrix');
    console.log('ré affiche le statut');
    if(visiPrix.checked===true)
    {

        document.getElementById('global-perio-non-previ').style.display="none";
        document.getElementById('global-cate').style.display="none";
        document.getElementById('global-perio').style.display="none";
        document.getElementById('ratio-periode').style.display="none";
        document.getElementById('pv-periode').style.display="none";
        document.getElementById('cout-periode').style.display="none";
        //document.getElementById('fiche-inde').style.visibility ="hidden";
        document.getElementsByTagName('button').forEach(button => {
           if(button.id !== "filtrer" & button.id !== "3months") button.style.display = "none";
        });


        var percentList = document.getElementsByClassName('percent-text')
        var i;
        for (i = 0; i < percentList.length; i++) {
            percentList[i].style.display = 'none';
        }
        var moneyList = document.getElementsByClassName('money-text');
        var i;
        for (i = 0; i < moneyList.length; i++) {
            moneyList[i].style.display = 'none';
        }
    }
    else
    {

        document.getElementById('global-perio-non-previ').style.display="";
        document.getElementById('global-cate').style.display="";
        document.getElementById('global-perio').style.display="";
        document.getElementById('ratio-periode').style.display="";
        document.getElementById('pv-periode').style.display="";
        document.getElementById('cout-periode').style.display="";


        var percentList = document.getElementsByClassName('percent-text')
        var i;
        for (i = 0; i < percentList.length; i++) {
            percentList[i].style.display = '';
        }
        var moneyList = document.getElementsByClassName('money-text');
        var i;
        for (i = 0; i < moneyList.length; i++) {
            moneyList[i].style.display = '';
        }
    }
}