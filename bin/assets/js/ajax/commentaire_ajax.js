 import * as Ajax from "./rest_ajax";


export function loadCommentaire(idCommentaire)
{
    return new Promise((resolve, reject) => {
        let method= 'POST';
        let url = '/heure/'+idCommentaire;
        Ajax.loadOne(url,method).then(
            (commentaire)=>{
                resolve(commentaire);
            }),
            (erreurs)=>{
                reject(erreurs);
            }

    });
}