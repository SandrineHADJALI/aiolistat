import * as Ajax from './rest_ajax';
/**Retourne la liste des independant en JSON
 *
 * @returns {Promise<unknown>}
 */
export function loadListInde()
{
    return new Promise((resolve, reject) => {
        let method ='GET';
        let url = '/independant';
        Ajax.loadList(url,method).then(
            (independants)=>{
                resolve(independants);
            },
            (erreurs)=>{
                reject(erreurs);
            }
        )
    });
}

/**Retourne un independant selon id passer en parametre
 *
 * @param id
 * @returns {Promise<unknown>} json
 */
export function loadInde(id)
{
    return new Promise(function (resolve, reject) {
        let method = 'GET';
        let url = '/independant/'+id;
        Ajax.loadOne(url,method).then(
            (independant)=>{
                resolve(independant)
            },
            (erreurs)=>{
                reject(erreurs);
            }
        )
    });
}

/**
 *
 * @param event
 */
export function update(inde)
{
    return new Promise((resolve, reject) => {
        let method = 'POST';
        let url = '/independant/update';
        Ajax.update(url,method,inde).then(
            ()=>{
                resolve();
        },
        (erreurs)=>{
                reject(erreurs);
        }
        )

    })
}

/**Supprime un independant via son id
 *
 * @param idInde
 * @returns {Promise<>}
 */
    export  function remove(idInde)
    {
        return new Promise((resolve, reject) => {
            let methode = 'GET';
            let url = '/independant/delete/'+idInde;
            Ajax.remove(url,methode).then(
                ()=>{
                    reject
                },
                (erreurs)=>{
                    reject(erreurs);
                }
            )
        })
    }

export function insert(inde)
{
    return new Promise((resolve, reject) => {
        let method = 'POST';
        let url = '/independant/insert';

        Ajax.insert(url,method,inde).then(
            (idInde)=>{
                resolve(idInde);
            },
            (erreurs)=>{
                reject(erreurs);
            }
        )
    });
}

export function loadByFiltres(filtres)
{
    return new Promise((resolve, reject) => {
        let url = '/independant/filtres';
        let method='POST';
        Ajax.loadList(url,method,filtres).then((indeList)=>{
            resolve(indeList);
        })
    })
}