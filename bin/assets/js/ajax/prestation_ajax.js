import * as Ajax from './rest_ajax';

/**Modifie la prestation passer en parametre
 *
 * @param presta JSON
 * @returns {Promise<unknown>}
 */
export function update(presta)
{
    return new Promise((resolve, reject) => {
        let method = 'POST';
        let url = '/prestation/update';
        Ajax.update(url,method,presta).then(
            ()=>{
                resolve();
            },
            (erreurs)=>{
                reject(erreurs);
            });
    });
}


/**Ajoute la prestation passer en parametre
 * return id de la presta
 * @param idSpj
 * @param presta
 * @returns {Promise<JSON>}
 */
export function addPresta(idSpj,presta)
{
    return new Promise((resolve, reject) => {
        let url = '/prestation/add/'+idSpj;
        let method ='POST';
        Ajax.insert(url,method,presta).then(
            (idPresta)=>{
                resolve(idPresta);
            },
            (erreurs)=>{
                reject(idPresta);
            }
        )
    })
}

/**retourne la prestation via son id en format JSON
 *
 * @param idPresta
 * @returns {Promise<JSON>}
 */
export function loadPresta(idPresta)
{
    return new Promise((resolve, reject) => {
        let url = '/prestation/'+idPresta;
        let method ='GET';
        Ajax.loadOne(url,method).then(
            (prestation)=>{
                resolve(prestation);
            },
            (erreurs)=>{
                reject(erreurs);
            }
        )

    })
}

export function supprimer(idPresta)
{
    return new Promise((resolve, reject) => {
        let method = 'GET';
        let url = "/prestation/delete/"+idPresta;
        Ajax.remove(url,method).then(
            ()=>{
                resolve();
            },
            (erreurs)=>{
                reject(erreurs);
            }
        )
    })
}