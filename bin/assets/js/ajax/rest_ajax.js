/**Requete AJAX pour inserer un element enb base
 *
 * @param url
 * @param method
 * @param objet
 * @returns {Promise<unknown>}
 */
export function  insert(url,method,objet) {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method,url);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.responseType='json';
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState===4) {
                if (xhr.status === 200) {
                    //retour id de l'element creer
                    resolve(xhr.response);

                    console.log("200");
                }
                if (xhr.status === 400 || xhr.status === 404 || xhr.status === 403) {
                    reject(xhr.response);
                    console.log("400");

                }
                if (xhr.status === 500) {
                    window.location.href = "http://www.app.aioli-digital.com";
                    console.log("500");

                }
            }
        }
        xhr.send(JSON.stringify(objet));
    })
}


/**Requete AJAX pour recuperer un element en base
 *
 * @param url
 * @param method
 * @param filtres
 * @returns {Promise<unknown>}
 */
export function loadOne(url,method,filtres=null)
{
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method,url);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.responseType='json';
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState===4) {
                if (xhr.status === 200) {
                    //retourne l'element
                    resolve(xhr.response[0]);
                }
                if ( xhr.status === 400 || xhr.status === 404 || xhr.status === 403) {
                    reject(xhr.response);
                }
                if (xhr.status === 500) {
                    // window.location.href="http://www.app.aioli-digital.com";
                }
            }
        }
        xhr.send(JSON.stringify(filtres));
    })
}

/**Methode AJAX permettant de supprimer un element en base
 *
 * @param url
 * @param method
 * @returns {Promise<unknown>}
 */
export function remove(url,method)
{
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method,url);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.onreadystatechange=function()
        {
            if (xhr.readyState===4) {
                if (xhr.status === 200) {
                    resolve();
                }
                if (xhr.status === 400 || xhr.status === 404 || xhr.status === 403) {
                    reject(xhr.response);
                }
                if (xhr.status === 500) {
                    // window.location.href="http://www.app.aioli-digital.com";
                }
            }
        }
        xhr.send();
    })
}



export function update(url,method,objet)
{
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.responseType='json';
        xhr.onreadystatechange = function () {
            if (xhr.readyState===4)
            {
                if ( xhr.status===200)
                {
                    resolve();
                }
                if (xhr.status ===400 || xhr.status===404 || xhr.status===403)
                {
                    reject(xhr.response);
                }
                if (xhr.status ===500)
                {
                    // window.location.href="http://www.app.aioli-digital.com";
                }
            }

        }
        xhr.send(JSON.stringify(objet));
    })
}

export function loadList(url,method,filtres=null)
{
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open(method,url);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.responseType='json';
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState===4) {
                if (xhr.status === 200) {
                    //retourne la liste
                    resolve(xhr.response);
                }
                if (xhr.status === 400 || xhr.status === 404 || xhr.status === 403) {

                    reject(xhr.response);
                }
                if (xhr.status === 500) {
                    // window.location.href="http://www.app.aioli-digital.com";
                }
            }
        }
        xhr.send(JSON.stringify(filtres));
    })
}