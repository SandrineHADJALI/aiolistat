import * as Ajax from './rest_ajax';

export function loadSousProjetByFiltre(idPj, periodicite, filtres) {
    return new Promise((resolve, reject) => {
        let method = 'POST';
        let url ='/sousProjet/'+periodicite+'/'+idPj;
        Ajax.loadList(url,method,filtres).then(
            (sousProjets)=>{
                resolve(sousProjets);
            },
            (erreurs)=>{
                reject(erreurs);
            }
        )
    });
}


/**Retourne la liste des sous-projets
 * en fonctionde l'id du projet et de la periodicite (MENSUEL ou ONE SHOT)
 *
 * @param idProjet
 * @param type
 */

export function loadSousProjetByProjetAndType(idProjet,type)
{
    return new Promise((resolve, reject) => {
        let method = 'GET';
        let url ='/sousProjet/'+type+'/'+idProjet;
        Ajax.loadList(url,method).then(
            (sousProjets)=>{
                resolve(sousProjets);
            },
            (erreurs)=>{
                reject(erreurs);
            }
        )
    })

}
export function loadSousProjet(idSpj)
{
    return new Promise((resolve, reject) => {
        let url = '/sousProjet/'+idSpj;
        let method='GET';
        Ajax.loadOne(url,method).then((sousProjet)=>{
            resolve(sousProjet);
        })
    })

}

export function deleteSpj(idSpj)
{
    return new Promise((resolve, reject) => {
        let url = '/sousProjet/delete/'+idSpj;
        let method ='POST';
        Ajax.remove(url,method).then(()=>{
            resolve();
        })
    })
}
export function insert(spjInfo)
{
    return new Promise((resolve, reject) =>
    {
        let url ='/sousProjet/insert';
        let method = 'POST';
        console.log("insert = ",spjInfo);
        Ajax.insert(url,method,spjInfo).then((idSpj)=>{
            resolve(idSpj);
        })
    })

}
export function update(spjInfo,idSpj)
{
    return new Promise((resolve, reject) => {
        let url = '/sousProjet/update/'+idSpj;
        let method='POST';
        Ajax.update(url,method,spjInfo).then((diffCout)=>{
            resolve(diffCout);
        })
    })
}


export function loadAllByFiltre(filtres)
{
    return new Promise((resolve, reject) => {
        let url = '/sousProjet/list';
        let method = 'POST';
        Ajax.loadList(url,method,filtres).then((liste)=>{
            resolve(liste);
        });
    })

}