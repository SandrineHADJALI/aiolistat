import * as Utils from "./utils/utils";
import * as Inde from './ajax/independant_ajax';
import * as Heure from './ajax/heure_ajax';
import * as Projet from './projet';
import * as SousProjet from './sousProjet';
import * as Refresh from './refresh';
import * as Valid from './validation';
import * as Filtre from './filtres';
import {capitalize} from "./utils/utils";
import {loadheure} from "./ajax/heure_ajax";
import {updateCommentaire} from "./ajax/heure_ajax";

let isAdmin = true;
/////////////////////////////////////////// AJOUT HEURE ////////////////////////////////

/**Creer et affiche le formulaire pour ajouter des heures sur le projet
 *
 * @param event
 */
export function formAddHeure(event)
{

    event.preventDefault();
    //recuperation du bouton
    let btnAdd = event.currentTarget;
    let nbForm = btnAdd.dataset.nbForm;
    nbForm++;
    btnAdd.dataset.nbForm = nbForm;
    // recuperation de l'id BDD du sous projet
    let idSpj = Utils.getId(btnAdd.id);
    let tdsSpj = SousProjet.getTdsAndInput(idSpj);
    let idHeure = idSpj+'form'+nbForm;
    //recuperation de la ligne des en tete
    let trBase = btnAdd.parentElement.parentElement;

    //recuperation de la liste des independnats
    Inde.loadListInde(idSpj).then((indeList)=>
    {
        // creation de la ligne qui va contenir le formulaire
        let trInput = document.createElement('tr');
        trInput.id='heure_'+idHeure;
        trInput.className='compl_spj_'+idSpj+' form_compl_spj_'+idSpj+' heure';
        trInput.setAttribute('data-id-sous-projet',idSpj);
        //trInput.id='commentaire'+idHeure;
        //trInput.className='commentaire'+idSpj+ 'form_compl_spj_';
        //trInput.setAttribute('data_commentaire',idSpj);

        //cellule du select
        let tdSelect = document.createElement('td');
        tdSelect.id='full_name_heure_'+idHeure;
        //creation d'un select avec la liste des independants
        let selectInde = Utils.getSelectInde(indeList);
        selectInde.id='select_inde_heure_'+idHeure;
        selectInde.addEventListener('change',refreshSalaire);
        let option = document.createElement("option");
        option.value='add';
        option.text="Ajouter un Indépendant";
        selectInde.add(option,0);
        tdSelect.appendChild(selectInde);
        tdSelect.setAttribute('colspan',2);
        trInput.setAttribute('data-id-inde',selectInde.value);
        tdSelect.setAttribute('data-id-inde',selectInde.value);

        //cellule du choix nb heure
        let tdHeure = document.createElement('td');
        tdHeure.id='heure_heure_'+idHeure;
        tdHeure.className='heures_inde_'+tdsSpj.projetPerio+'_'+tdsSpj.projetId+'_'+selectInde.value;
        //creation champs input heure
        let attrHeure ={
            "id":"input_heure_heure_"+idHeure,
            'type':'number',
            'value':1,
            'step':0.25,
            'min':0.25
        }
        tdHeure.setAttribute("data-nb-heures",attrHeure.value);
        let inputHeure = Utils.getInput(attrHeure);
        //event listener pour valoriser le cout en fonction du nombre d'heures
        inputHeure.addEventListener("input",refreshCout);
        tdHeure.appendChild(inputHeure);

        /*Commentaire*/
        //cellule champs input Commentaire
        let tdCommentaire = document.createElement('td');
        tdCommentaire.id='commentaire_heure_'+idHeure;
        tdCommentaire.className='commentaire'+idSpj+ 'form_compl_spj_';
        //tdCommentaire.className='commentaire_heure_';

        //creation champs input commentaire
        let attrCommentaire ={
            "id":"input_commentaire_heure_"+idHeure,
            'type':'text',
            'value':'',
        }
        let inputCommentaire = Utils.getInput(attrCommentaire);
        tdCommentaire.appendChild(inputCommentaire);
        /*******/


        // cellules des boutons
        let tdBtns = document.createElement('td');
        tdBtns.colSpan=2;

        //span conteneur des boutons
        let span = document.createElement('span');
        span.id='btns_heure_spj_'+idSpj;
        //bouton validation formulaire
        let btnCheck = Utils.getBtnCheck(addHeure);
        btnCheck.id='valid_heure_'+idHeure;

        // bouton annulation ajout heure
        let btnCancel = Utils.getBtnCancel(cancelAdd);
        btnCancel.id = 'cancel_heure_'+idHeure
        span.appendChild(btnCheck);
        span.appendChild(btnCancel);
        tdBtns.appendChild(span);
        // cellule du cout
        let tdCout = document.createElement('td');
        tdCout.id = 'cout_heure_'+idHeure;

        //On masque les boutons lors de la creation d'un sous projet
        if (isNaN(idSpj))
        {
            btnCheck.style.display="none";
        }

        let trMessage = document.createElement('tr');
        let tdMessage = trMessage.insertCell();
        tdMessage.colSpan=4;
        trMessage.id='message_for_heure_'+idHeure;
        trMessage.style.display='none';
        //assemblage des elements
        trInput.appendChild(tdSelect);
        trInput.appendChild(tdHeure);
        trInput.appendChild(tdCout);
        trInput.appendChild(tdCommentaire);
        trInput.appendChild(tdBtns);
        trBase.insertAdjacentElement('afterend',trInput);
        trBase.insertAdjacentElement("afterend", trMessage);
        Refresh.refreshSalaire(idHeure,selectInde.value).then(()=>{
            Refresh.refreshCostHeure(idHeure);
            Refresh.refreshRoiPjAndSpj(idSpj);
        });
    });

}

/** Ajoute l'heure ou renvoie les erreurs dans le formulaire
 * TODO:gestion des erreurs
 * @param event
 */
function addHeure(event)
{

    event.preventDefault();
    //recuperation des inofs de base
    let idAddHeure = Utils.getId(event.currentTarget.id);
    let base = Utils.getinfosBase(event);
    let span = base.span;
    //recuperation des cellules
    let tds = getTdAndInputs(idAddHeure);
    let tr = tds.trHeure;
    let idSpj = tds.idSpj;
    let tdCout = tds.tdCout;
    let tdCommentaire = tds.tdCommentaire;
    let inputCommentaire = tds.inputCommentaire;
    let tdInde = tds.tdInde;
    let select = tds.selectInde;
    let inputHeure = tds.inputHeure;
    base.tr.classList.remove('form_compl_spj_'+base.tr.dataset.idSousProjet);
    if (Valid.heureForm(idAddHeure))
    {
        // recuperation des saisies
        let idInde = select.value;
        let fullName = select.options[select.selectedIndex].innerText;
        let heure = ({
            "heure": inputHeure.value,
            "sousProjet": idSpj,
            "independant": idInde,
            "commentaire": inputCommentaire.value,
        });

        Heure.addHeure(heure).then(
            (idHeure)=>{
                Utils.inverseBtns(span,1,'heure',formUpdateHeure,validSup);
                //modification des id
                tr.id='heure_'+idHeure;
                tdInde.setAttribute('colspan',2);
                tdInde.innerText=fullName;
                tdInde.id='full_name_heure_'+idHeure;
                tds.tdHeure.id='heure_heure_'+idHeure;
                tds.tdHeure.innerText=tds.tdHeure.dataset.nbHeures+' H';
                span.id='btns_inde_spj_'+idHeure;
                tdCout.id = 'cout_heure_'+idHeure;
                tdCommentaire.id = 'commentaire_heure_'+idHeure;
                tds.tdCommentaire.innerText=inputCommentaire.value;
                Refresh.refreshRoiPjAndSpj(idSpj);
                let btnAdd = document.getElementById('add_heure_'+idSpj);
                btnAdd.dataset.nbForm = parseInt(btnAdd.dataset.nbForm)-1;
                Utils.afficherMessage('Ajout effectué avec succès','heure_'+idAddHeure,'success');
            },
            (message)=>{
                Utils.afficherMessage(message,'heure_'+idAddHeure,'error');
            }
        );


    }
}


function cancelAdd(event)
{
    event.preventDefault();

    let idHeure = Utils.getId(event.currentTarget.id);

    let tds = getTdAndInputs(idHeure);
    let trAddInde = document.getElementById('create_inde_'+idHeure);
    if (trAddInde!==null)
    {
        trAddInde.remove();
    }
    let trMessage = document.getElementById("message_for_heure_"+idHeure);
    trMessage.remove();
    let idSpj = tds.idSpj;
    let btnAdd = document.getElementById('add_heure_'+idSpj);
    btnAdd.dataset.nbForm = parseInt(btnAdd.dataset.nbForm)-1;
    Utils.cancel(event);
    Refresh.refreshRoiPjAndSpj(idSpj);
    Refresh.getFicheIndeValuesOnDom();
}
////////////////////////////////// MODIFICATION HEURE ///////////////////////////////////////////////
/**Creer et affiche le formulaire mon modifier un heure
 *
 * @param event
 */
export function formUpdateHeure(event) {
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let idHeure = base.id;
    let idCommentaire = base.id;
    let spanBtns = base.span;
    let tds = getTdAndInputs(idHeure);
    //recuperation de l'heure en BDD

    Heure.loadheure(idHeure).then((heure)=>{

        //recuperations des cellules et des champs de saisies
        base.tr.classList.add('form_compl_spj_'+base.tr.dataset.idSousProjet);

        //recuperation du td fullname
        //premier element du tr
        let tdFullName = tds.tdInde;
        //recuperation id independant
        let inde = heure.independant;
        let commentaire = heure.commentaire;

        //creation des champs input
        let tdHeure = document.getElementById('heure_heure_'+idHeure);

        let attrHeure ={
            "id":"input_heure_heure_"+idHeure,
            'type':'number',
            'value':heure.heure,
            'step':0.25,
            'min':0.25
        }
        let inputHeure = Utils.getInput(attrHeure);

        //event pour modifier cout
        inputHeure.addEventListener("input",refreshCout);
        //ajout du champ heure dans la ligne apres champ fullname
        tdHeure.innerText='';
        tdHeure.appendChild(inputHeure);

        //recuperation commentaire
        let tdCommentaire = document.getElementById('commentaire_heure_'+idHeure);
        let attrCommentaire ={
            "id":"input_commentaire_heure_"+idHeure,
            'type':'text',
            'value':heure.commentaire,
        }
        let inputCommentaire = Utils.getInput(attrCommentaire);
        //ajout du champ commentaire dans la ligne

        tdCommentaire.innerText='';
        tdCommentaire.appendChild(inputCommentaire);
        /*******/



        //select independant
        Inde.loadListInde().then((listInde)=>{
            //si chargement de la liste bien effectue => creation du select
            //select doit afficher l'independant concerne en premier
            //sachant que value des option == id de independant
            let select = Utils.getSelectInde(listInde);
            select.id='select_inde_heure_'+idHeure;
            select.value = inde.id;
            select.addEventListener("change",refreshSalaire);
            let option = document.createElement("option");
            option.value='add';
            option.text="Ajouter un Indépendant";
            select.add(option,0);
            //select va remplacer text du tdFullname
            tdFullName.innerText="";
            tdFullName.appendChild(select);

            // changement des boutons
            Utils.inverseBtns(spanBtns,idHeure,'heure',updtHeure,cancelUpdtheure);
            let trMessage = document.createElement('tr');
            let tdMessage = trMessage.insertCell();
            tdMessage.colSpan=4;
            trMessage.id='message_for_heure_'+idHeure;
            trMessage.style.display='none';
            base.tr.insertAdjacentElement("beforebegin", trMessage);
        })
    })
}
/**
 *
 * @param event
 */
function cancelUpdtheure(event) {
    event.preventDefault();
    //recuperation des elements
    //span conteneur des boutons
    let base = Utils.getinfosBase(event);
    event.preventDefault();
    let span = base.span;
    //id heure BDD
    let idHeure = base.id;
    let tds = getTdAndInputs(idHeure)
    let tdFullName = tds.tdInde;
    let select = tds.selectInde;
    let tdH = tds.tdHeure;
    let tdCout = tds.tdCout;
    let tdCommentaire = tds.tdCommentaire;
    base.tr.classList.remove('form_compl_spj_'+base.tr.dataset.idSousProjet);
    //recuperation des infos sur heure en BDD
    Heure.loadheure(idHeure).then((heure)=>{
        tds.trHeure.dataset.idInde=heure.independant.id;
        //suppression du select
        tdFullName.removeChild(select);
        //mise a jour du nom
        tdFullName.innerText=Utils.capitalize(heure.independant.prenom) +" "+Utils.capitalize(heure.independant.nom);
        tdFullName.dataset.idInde = heure.independant.id;
        //suppression cellule heure
        tdH.innerText=heure.heure+' H';
        tdH.dataset.nbHeures = heure.heure;
        updateClassTdHeure(tds,heure.independant.id);
        //fusion des cellules
        tdFullName.setAttribute('colspan',2);
        //mise a jour du cout
        tdCout.innerText = Utils.formatEuro(heure.cout);
        tdCout.dataset.salaire =  heure.independant.salaire;
        tdCout.dataset.cost = heure.cout;
        updateClassTdHeure(tds,heure.commentaire.id);
        //mise a jour du commentaire
        tdCommentaire.innerText=heure.commentaire;
        tdCommentaire.dataset.commentaire = heure.commentaire;
        //changement des boutons
        Utils.inverseBtns(span,idHeure,'heure',formUpdateHeure,validSup);
        //suppression de la div message
        document.getElementById('message_for_heure_'+idHeure).remove();
        Refresh.refreshRoiPjAndSpj(tds.idSpj);
    })
}

/**Modifier l'heure renvoie les erreurs si erreurs dans les formulaires
 * TODO:gestion des erreurs
 * @param event
 */
function updtHeure(event) {
    //recuperation des infos de base
    event.preventDefault();
    let base= Utils.getinfosBase(event);
    let spanBtns = base.span;
    let idHeure = base.id;
    let tr = base.tr;
    let idCommentaire = base.id;


    tr.classList.remove('form_compl_spj_'+tr.dataset.idSousProjet);
    if (Valid.heureForm(idHeure))
    {
        //recuperation des saisies
        let tds = getTdAndInputs(idHeure);
        let idInde = tds.selectInde.value;
        let heureSaisie= tds.inputHeure.value;
        let commentaireSaisie = tds.inputCommentaire.value;

        if (isNaN(idInde))
        {
            //il y a une creation d'independant
            //il faut l'ajouter en base avant
            let inde = getIndeFormValues(idHeure);
            if (inde!== null)
            {
                Inde.insert(inde).then(
                    (idI)=>{
                        let heure =({
                            "id":idHeure,
                            "heure":heureSaisie,
                            "idInde":idI,
                            "commentaire":commentaireSaisie,

                        });

                        Heure.updateHeure(heure).then(()=>{
                            tds.tdInde.innerText=Utils.capitalize(inde.prenom)+' '+Utils.capitalize(inde.nom);
                            tds.tdHeure.innerText=tds.tdHeure.dataset.nbHeures+' H';
                            tds.tdCommentaire.innerText=tds.tdCommentaire.dataset.commentaire;
                            tds.tdCommentaire.innerText=tds.inputCommentaire.value;
                            let trAdd = document.getElementById('create_inde_'+idHeure);
                            trAdd.remove();
                            Utils.afficherMessage('Modifications effectuées avec succès','heure_'+idHeure,'success');
                        });
                    },
                    (message)=>{
                        Utils.afficherMessage(message,'heure_'+idHeure,'error');
                    }
                )};
        }else{
            let heure =({
                "id":idHeure,
                "heure":heureSaisie,
                "idInde":idInde,
                "commentaire":commentaireSaisie,
            });
            Heure.updateHeure(heure).then(
                ()=>{
                    trInputToText(tr,spanBtns,idHeure,tds);
                    Utils.getId(tr.className);
                    Utils.afficherMessage('Modifications effectuées avec succes','heure_'+idHeure,'success');
                },

                (message)=>{
                    Utils.afficherMessage(message,'heure_'+idHeure,'error');
                }
            );
        }
    }
}


///////////////////////////// SUPPRIMER HEURE ////////////////////////////////////////////
/**Demande validation suppression
 * changement des boutons modifier/supprimer par valider/annuler
 * passage de id de la presta dans id des elements
 * @param event
 */
export function validSup(event)
{
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let span =base.span;
    let id = base.id;
    //modification des boutons pour valider suppression
    Utils.inverseBtns(span,id,'sup',supprimerHeure,cancelSup);

}

/**Suppression de l'heure
 *
 * @param event
 */
function supprimerHeure(event) {
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let idHeure = base.id;
    let tds = getTdAndInputs(idHeure);
    let tr = base.tr;
    Heure.removeHeure(idHeure).then(()=>{
        tr.remove();
        Refresh.refreshRoiPjAndSpj(tds.idSpj);
    })

}


/**Appeler lors de l'annulation de la suppression
 *
 * @param event
 */
function cancelSup(event)
{
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let span =base.span;
    let id = base.id;
    Utils.inverseBtns(span,id,'presta',formUpdateHeure,validSup);
}

//////////////////////////////// OUTILS ////////////////////////////////////////////////

/**Modifie la cellule cout en fonction des heures saisie
 *
 * @param event
 */
function refreshCout(event)
{

    let inputheure = event.currentTarget;
    let idheure = Utils.getId(inputheure.id);


    //l'id de l'heure nous permet de recuperer les elements de l'heure sur le DOM
    let tds = getTdAndInputs(idheure);
    tds.tdHeure.dataset.nbHeures = (isNaN(inputheure.value)?0:inputheure.value);
    //A VOIR SI CODE ICI
    //et le tBody du sous-projet
    //l'id du tBody nous permet de recuperer l'id du sous-projet
    let idSpj = Utils.getId(tds.tBody.id);

    // on modifie les ROI
    Refresh.refreshCostHeure(idheure);
    Refresh.refreshRoiPjAndSpj(idSpj);

}


/**Fonction active sur event onchange du selectInde
 * MAj dataset salaire et cout et les roi
 * @param event
 */
function refreshSalaire(event)
{
    //recupere le select d'ou vient l'event
    let selectInde = event.currentTarget;
    //sa valeur correspond a l'id de l'independant enb BDD
    let idInde = selectInde.value;
    //l'id de l'heure nous permet de recuperer les elements de l'heure sur le DOM
    let idheure = Utils.getId(selectInde.id);
    let tds = getTdAndInputs(idheure);
    let trH =tds.trHeure;
    // ou a la demande ajouter un independant
    if (isNaN(idInde))
    {
        //ajout d'un independant
        trH.insertAdjacentElement("afterend",trCreateInde(idheure));
        // on modifie les dataset
        tds.tdCout.dataset.salaire=1;
        tds.tdCout.dataset.cost = tds.tdHeure.dataset.nbHeures;
        tds.tdCout.innerText= Utils.formatEuro(tds.tdHeure.dataset.nbHeures)
    }else{
        //recherche ligne creation independant
        let trAddInde = document.getElementById('create_inde_'+idheure);
        //si encore presente -> suppression
        if (trAddInde!=null)
        {
            trAddInde.remove();
        }
        //ajout ou modification du dataset id inde sur le td
        tds.tdInde.dataset.idInde=idInde;
        tds.trHeure.dataset.idInde=idInde;
        //modification de la classe du tdHeure
        updateClassTdHeure(tds,idInde);

        //et le tBody du sous-projet
        //l'id du tBody nous permet de recuperer l'id du sous-projet
        let idSpj = Utils.getId(tds.tBody.id);
        //On modifie le dataset salaire sur la cellule cout
        // en cherchant en base le salaire de l'independant
        Refresh.refreshSalaire(idheure,idInde).then(()=>{
            //au retour de la fonction async
            //on modifie le cout
            Refresh.refreshCostHeure(idheure);
            // puis les ROI
            Refresh.refreshRoiPjAndSpj(idSpj);
        });
    }
}



/**Passe d'une ligne en ode formulaire a une ligne affichage classique
 *
 * @param tr
 * @param span
 * @param idHeure
 * @param tds
 */
function trInputToText(tr,span,idHeure,tds=null)
{
    if (tds==null)
    {

        //si les td ne sont pas fournis, on les recuperes avec idHeure
        tds = getTdAndInputs(idHeure);
   }

    //affichage du nom complet de l'independant
    tds.tdInde.innerText=tds.selectInde.options[tds.selectInde.selectedIndex].innerText;
    //affichage nombre d'heures
    tds.tdHeure.innerText=tds.tdHeure.dataset.nbHeures+' H';
    //affichage du commentaire
    tds.tdCommentaire.innerText=tds.inputCommentaire.value;
    //changement des boutons
    Utils.inverseBtns(span,idHeure,'heure',formUpdateHeure,validSup);
}


/**Recuperes les cellules et les champs de saisies (si il y a )
 *
 * @param idHeure
 * @param idSpj
 * @returns {{tdInde: HTMLElement, selectInde: HTMLElement, tdHeure: HTMLElement, tdCout: HTMLElement, inputHeure: HTMLElement,inputCommentaire: HTMLElement}}
 */
export function getTdAndInputs(idHeure)
{
    let tdInde = document.getElementById('full_name_heure_'+idHeure);
    let selectInde = document.getElementById('select_inde_heure_'+idHeure);
    let tdH = document.getElementById('heure_heure_'+idHeure);
    let inputHeure = document.getElementById('input_heure_heure_'+idHeure);
    let tdCout = document.getElementById('cout_heure_'+idHeure);
    let tdCommentaire = document.getElementById('commentaire_heure_'+idHeure);
    let inputCommentaire = document.getElementById('input_commentaire_heure_'+idHeure);
    let tBody= tdInde.parentElement.parentElement;
    let trHeure = tdInde.parentElement;
    let idSpj = trHeure.dataset.idSousProjet;
    let trHead = document.getElementById('list_heures_spj_'+idSpj);
    let headH = document.getElementById('head_heure_spj_'+idSpj);
    let headC=document.getElementById('head_cout_spj_'+idSpj);
    let headCom=document.getElementById('head_commentaire_spj_'+idSpj);
    let headF=document.getElementById('head_add_spj_'+idSpj);
    let tdsSpj = SousProjet.getTdsAndInput(idSpj);
    let tdsProjet = tdsSpj.tdsProjet;
    return ({
        "tdInde":tdInde,
        "selectInde" :selectInde,
        "tdHeure":tdH,
        "inputHeure":inputHeure,
        "tdCout":tdCout,
        "tdCommentaire":tdCommentaire,
        "inputCommentaire":inputCommentaire,
        "tBody":tBody,
        "trHead":trHead,
        "idSpj":idSpj,
        "headHeure":headH,
        "headCout":headC,
        "headCom":headCom,
        "headAdd":headF,
        "trHeure":trHeure,
        "tdsSpj":tdsSpj,
        "tdsProjet":tdsProjet,
    });
}

export function getTrHeadHeure(id)
{
    // creation de  la ligne titre
    let trIndeHead = document.createElement('tr');
    trIndeHead.id = 'list_heures_spj_'+id;
    trIndeHead.setAttribute('class','align-middle');
    //cellule avec icon pour ajouter un independant
    let thIconAdd = document.createElement('th');
    thIconAdd.colSpan = 2;
    thIconAdd.id='head_add_spj_'+id;
    let btnAdd = Utils.getBtnAdd(formAddHeure);
    btnAdd.setAttribute('id','add_heure_'+id);
    btnAdd.setAttribute('class','bouton btn btn-add');
    btnAdd.setAttribute('title','Ajouter un indépendant');
    btnAdd.setAttribute('data-nb-form',0);
    //si il ya des heure l'en tete change
    thIconAdd.innerText = "Indépendants";
    thIconAdd.insertAdjacentElement('afterbegin',btnAdd);
    //cellule d'en tete heure a afficher lors des modif ou ajout
    let thHeure = document.createElement("th");
    thHeure.innerText="Heures";
    thHeure.id="head_heure_spj_"+id;
    //colonne pour les couts
    let thCout = document.createElement('th');
    thCout.innerText="Coût";
    thCout.id='head_cout_spj_'+id;
    //collonne commentaire
    let thCommentaire = document.createElement('th')
    thCommentaire.innerText="Commentaire";
    thCommentaire.id='head_commentaire_spj_'+id;

    //colonne vide si Role User
    let thVide = document.createElement('th');
    thVide.innerText="Indépendants";
    //colonne vide si Role User , de recalage colonne
    let thRecal = document.createElement('th');

    if(visiPrix.checked === true){
        trIndeHead.appendChild(thIconAdd).style.display="none";
        trIndeHead.appendChild(thVide);
        trIndeHead.appendChild(thCout);
        trIndeHead.appendChild(thHeure);
        trIndeHead.appendChild(thRecal);
        trIndeHead.appendChild(thCommentaire);
    }
    else{
        trIndeHead.appendChild(thIconAdd);
        trIndeHead.appendChild(thHeure);
        trIndeHead.appendChild(thCout);
        trIndeHead.appendChild(thCommentaire);
    }
    return trIndeHead;
}



export function trCreateInde(idHeure)
{
    let tr = document.createElement("tr");
    tr.id="create_inde_"+idHeure;
    let tdNom = tr.insertCell();
    tdNom.setAttribute("colspan",2);
    let tdPrenom = tr.insertCell();
    let tdSalaire = tr.insertCell();

    let attrPrenom = {
        "placeholder":"Prénom",
        "id":"prenom_create_inde_"+idHeure,
        "type":"text",
        "minlenght":3,
    }
    tdPrenom.appendChild(Utils.getInput(attrPrenom));
    let attrNom = {
        "placeholder":"Nom",
        "id":"nom_create_inde_"+idHeure,
        "type":"text",
        "minlenght":3,
    }
    tdNom.appendChild(Utils.getInput(attrNom));
    let attrSalaire = {
        "value":1,
        "id":"salaire_create_inde_"+idHeure,
        "type":"number",
        "min":1,
        "step":0.1,
    }
    let inputSaleire = Utils.getInput(attrSalaire);
    inputSaleire.addEventListener("input",updateSalaire);
    tdSalaire.appendChild(inputSaleire);


    let tdBtns = tr.insertCell();
    tdBtns.id='btns_create_inde_'+idHeure;
    let btnCheck = Utils.getBtnCheck(creationInde);
    btnCheck.id='valid_create_inde_'+idHeure;
    btnCheck.setAttribute('title','Créer l\'indépendant');
    tdBtns.appendChild(btnCheck);
    return tr;

}

function updateSalaire(event)
{
    let inputSalaire = event.currentTarget;
    let salaire = parseFloat(inputSalaire.value);
    let idHeure = Utils.getId(inputSalaire.id);
    let tds = getTdAndInputs(idHeure);
    tds.tdCout.dataset.salaire = salaire;
    Refresh.refreshCostHeure(idHeure);

}



export function getIndeFormValues(idHeure)
{

    let indeInputs =getIndeINputs(idHeure);
    if (Valid.indeForm('heure_'+idHeure,indeInputs))
    {
        return {
            "nom":indeInputs.nom.value,
            "prenom":indeInputs.prenom.value,
            "salaire":indeInputs.salaire.value
        };
    }else{
        return  null;
    }
}

export function getIndeINputs(idHeure)
{
    return{
        "nom":document.getElementById('nom_create_inde_'+idHeure),
        "prenom":document.getElementById('prenom_create_inde_'+idHeure),
        "salaire":document.getElementById('salaire_create_inde_'+idHeure),
    }
}


function creationInde(event)
{

    event.preventDefault();
    let idHeure = Utils.getId(event.currentTarget.id);
    //recuperation des infos sur l'inde
    let indeInputs = getIndeINputs(idHeure);
    let idMessage = 'message_for_heure_'+idHeure;
    if (Valid.indeForm(idMessage,indeInputs))
    {
        let inde = getIndeFormValues(idHeure);
        if (inde!== null) {
            Inde.insert(inde).then((idI) => {
                    inde.idInde=idI;
                    //on ajoute l'inde a tous les select Inde
                    //creation d'un optionElement
                    let optionInde= document.createElement("option");
                    optionInde.value= inde.idInde;
                    optionInde.text=Utils.capitalize(inde.prenom)+' '+Utils.capitalize(inde.nom)
                    let selectsInde = document.querySelectorAll('select[name=independant]');
                    //on recharge la liste des indépendnat pour chaque select
                    // bug sur select.add
                    // on recupere le premier option qui est differents si select du filtre ou select creation heure
                    Inde.loadListInde().then((indeList)=>{
                        selectsInde.forEach(function (sel) {
                            let optionBase = sel.options[0];
                            let selValue = sel.value;
                            sel.innerText='';
                            sel.add(optionBase);
                            //ajout de chaque independant au select
                            indeList.forEach(function (inde) {
                                let option = document.createElement('option');
                                option.innerText = capitalize(inde.prenom) +" "+capitalize(inde.nom);
                                option.value=inde.id;
                                sel.add(option);

                            });
                            sel.value=selValue;
                            //modification sur le select en cours
                            if (Utils.getId(sel.id)===idHeure)
                            {
                                document.getElementById('create_inde_'+idHeure).remove();
                                sel.value=inde.idInde;
                                Utils.setValid(sel.classList,true);
                            }
                        })
                        //modification des ids
                        let tds = getTdAndInputs(idHeure);
                        updateClassTdHeure(tds,inde.idInde);

                        tds.trHeure.dataset.idInde = inde.idInde;
                        tds.tdInde.dataset.idInde = inde.idInde;
                    })

                },
                (erreurs)=>{
                    let message = Utils.traitementErreursInde(erreurs,indeInputs);
                    Utils.afficherMessage(message,'heure_'+idHeure,'error');
                }
            );
        }
    }
}

function updateClassTdHeure(tds,idInde)
{
    let tdHeure = tds.tdHeure;
    //premiere class permet de recuperer les heure d'un inde par projet
    //premier mot est l'id de l'inde
    let classe = tdHeure.classList[0];
    let tabClass = classe.split('_');
    //recuepration de l'ancien id de l'inde
    let oldIdInde = tabClass[tabClass.length-1];
    //comparaison avec id filtre
    //si identique modification nombre de projets inde
    let idIndeFiltre = Filtre.getFiltreValues().independant;
    let tdHeureProjet = tds.tdsSpj.tdsProjet.tdHeure;
    if (oldIdInde===idIndeFiltre)
    {
        if (tdHeureProjet!== undefined && tdHeureProjet!== null)
        {
            let nbSousProjet = parseInt(tdHeureProjet.dataset.nbSousProjetsInde);
            nbSousProjet--;
            tdHeureProjet.dataset.nbSousProjetsInde=nbSousProjet;
        }
    }
    if (idInde==parseInt(idIndeFiltre))
    {
        if (tdHeureProjet!== undefined && tdHeureProjet!== null)
        {
            let nbSousProjet = parseInt(tdHeureProjet.dataset.nbSousProjetsInde);
            nbSousProjet++;
            tdHeureProjet.dataset.nbSousProjetsInde=nbSousProjet;
        }
    }
    tabClass.pop()
    tabClass.push(idInde);
    tdHeure.classList.replace(tdHeure.classList.item(0),tabClass.join('_'));
}