import * as Utils from "./utils/utils";
import * as Projet from "./ajax/projet_ajax";
import * as SousProjet from "./sousProjet";
import * as Valid from './validation';
import * as List from './projetList';
import * as Filtre from './filtres';
import * as Refresh from './refresh';
import  * as Tris from './tris';
import {userSup} from "./projetList";

let isAdmin = "false";
////////////////////////////// AJOUTER PROJET //////////////////////////////////

window.addEventListener('DOMContentLoaded', function() {

    let userAdmin = document.querySelector('.js-user-admin');
    isAdmin = userAdmin.dataset.isAdmin;
})
export function tableForCreatePj(event)
{
    event.stopPropagation();
    event.preventDefault();
    let divGeneral= document.getElementById('div-general');
    //masquer le bouton ajouter
    let btnAdd = event.currentTarget;
    btnAdd.style.display="none";
    let div = document.getElementById("create-projet");
    div.classList.add('create-projet');
    let form = document.createElement("form");
    form.setAttribute('class','w-100');
    form.id='form_create';
    div.appendChild(form);
    getTableCreateProjet().then((table)=>{
        form.appendChild(table);
        Refresh.refreshRoiPj('add','add');
    });
    if (Utils.isMobile()) {
        let filtres = document.getElementById('les-filtres');
        filtres.style.display='none';
        let infos = document.getElementById('les-infos');
        infos.style.display='none';
        divGeneral.style.display = 'none';
        document.getElementById('creation-projet').style.display = '';
    }

}

export function getTableCreateProjet()
{
    return new Promise(resolve => {
        let idPj ='add_add';
        //premier tableau infos de base du projet (nom,prix de vnete, roi)
        let table = document.createElement('table');
        table.setAttribute('class','table col');

        //en tete du tableau
        let tHeadPj = getTheadProjet();
        table.appendChild(tHeadPj);

        //tBody qui contiendra les infos du projet et des sous-Projets
        let tBody = document.createElement("tbody");
        table.appendChild(tBody);
        //premiere ligne = infos du projet
        let trPj = tBody.insertRow();
        trPj.id='projet_'+idPj;

        //Cellule Nom
        let tdNom = trPj.insertCell();
        tdNom.id='nom_projet_'+idPj;

        //Champ de saisie du nom
        let attrNom={
            "type":"text",
            "placeholder":"Nom du projet",
            "required":true,
            "id":"input_nom_projet_"+idPj,
        }
        let inputNom = Utils.getInput(attrNom);
        tdNom.appendChild(inputNom);

        //Cellule ROI
        let tdRoi = trPj.insertCell();
        tdRoi.id='roi_projet_'+idPj;
        tdRoi.innerText = Utils.formatPercent(0);
        tdRoi.className='percent-text projet-text';
        //Cellule prix de vente

        /*let visiPrix = document.getElementById('visiPrix');
        if(visiPrix.checked===false){*/

        let tdPv = trPj.insertCell();
        tdPv.id='pv_projet_'+idPj;
        tdPv.innerText= Utils.formatEuro(0);
        tdPv.className ='money-text projet-text';
        tdPv.setAttribute('data-cost',0);
        tdPv.setAttribute('data-prix-vente',0);
        //Cellules des boutons

        let tdBtns = trPj.insertCell();
        tdBtns.id='button_projet_'+idPj;
        let span = document.createElement('span');
        span.id='btns_projet_'+idPj;
        //TODO:ajouter les callbacks aux boutons
        let btnCheck = Utils.getBtnCheck(createProjet);
        btnCheck.id='valid_create_projet_add_add';
        let btnCancel = Utils.getBtnCancel(cancelCreatePj);
        btnCancel.id='cancel_create_projet_add_add';

        let btnAdd = Utils.getBtnAdd(SousProjet.formAddSpj);
        btnAdd.setAttribute('title','Ajouter un Sous-Projet');
        btnAdd.setAttribute('data-nb-form',0);
        btnAdd.setAttribute('data-type-form','create');
        btnAdd.id='add_spj_add_add';

        span.appendChild(btnCheck);
        span.appendChild(btnCancel);
        span.appendChild(btnAdd)
        tdBtns.appendChild(span);/*}*/

        ///////////Ajout des sous Projets

        //ajout d'un premier formulaire pour creer un sous-projet
        SousProjet.getFormAddSpj(btnAdd,true).then((trForm)=>{

            tBody.appendChild(trForm);
            resolve(table);
        })

    })

}

/**Retourne le tHead des en-tete pour le tableau des projets
 * (Nom,ROI,Prix de Vente)
 */
function getTheadProjet(periodicite=null)
{
    //creation du thead
    let tHead = document.createElement("thead");

    //creation de la ligne des en tete
    let tr = tHead.insertRow();
    //liste des en tete
    let enTetes = ["Nom","ROI","Prix de Vente"];
    //ajout d'un en tete pour heure independant si filtre par independant
    let filtres = Filtre.getFiltreValues();
    if (filtres.independant !== null)
    {
        enTetes.push('Heures de l\'indépendant');
    }
    enTetes.push('');
    enTetes.forEach(function (enTete,key) {
            let th = document.createElement("th");
            th.innerText = enTete;
            if (periodicite !== null) {
                th.setAttribute('data-periodicite', periodicite);
                th.setAttribute('data-column', key);
                th.className = 'trie';
                th.addEventListener("click", Tris.trier);
                if (enTete === 'Nom') {
                    th.setAttribute('data-name', 'nom');
                    th.setAttribute('data-sort', 0);
                    th.insertAdjacentElement("afterbegin", Utils.getSpanIconFiltreUp('nom_'+periodicite,''));
                    th.insertAdjacentElement("afterbegin", Utils.getSpanIconFiltreDown('nom_'+periodicite));
                } else {
                    th.setAttribute('data-type', 'number');
                    th.setAttribute('data-sort', 1);
                    if (enTete === 'ROI') {
                        th.setAttribute('data-name', 'roi');
                        th.insertAdjacentElement("afterbegin", Utils.getSpanIconFiltreUp('roi_'+periodicite));
                        th.insertAdjacentElement("afterbegin", Utils.getSpanIconFiltreDown('roi_'+periodicite));
                    } else {
                        th.setAttribute('data-name', 'prix-vente');
                        th.insertAdjacentElement("afterbegin", Utils.getSpanIconFiltreUp('prix-vente_'+periodicite));
                        th.insertAdjacentElement("afterbegin", Utils.getSpanIconFiltreDown('prix-vente_'+periodicite));
                    }
                }
            }
        tr.appendChild(th);
        }
    )
    return tHead;
}

function cancelCreatePj()
{

    let divCreate = document.getElementById(('create-projet'));
    divCreate.className="";
    divCreate.innerText="";
    let btnAdd = document.getElementById('add_projet');
    btnAdd.style.display="";
    if (Utils.isMobile())
    {
        document.getElementById('creation-projet').style.display='none';
        document.getElementById('div-general').style.display='';
    }
}

function createProjet(event)
{
    event.preventDefault();
    event.stopPropagation();
    let tds = getTdsAndInput('add','add');
    if (Valid.projetForm('add','add'))
    {
        let projet ={
            "nom":tds.inputNom.value.trim(),
        }
        let spjForm = document.querySelectorAll('.form_spj_add_add');
        let valid = true;
        spjForm.forEach(function (trForm) {
            let idSpj = Utils.getId(trForm.id);
            valid = Valid.sousProjetForm(idSpj)
        });
        if (valid)
        {
            //insertion du projet en bdd
            Projet.insertProjet(projet).then((idPj)=>{
                //modifications des ids des principaux elements pour insertion des sous-projets
                tds.tdNom.id='nom_projet_add_'+idPj;
                tds.inputNom.id='input_nom_projet_add_'+idPj;
                tds.tdRoi.id='roi_projet_add_'+idPj;
                tds.tdPv.id='pv_projet_add_'+idPj;
                tds.trProjet.id='projet_add_'+idPj;


                tds.tdNom.innerText= projet.nom;
                //si formulaire de sous-projet
                if (spjForm.length >0)
                {
                    let promiseTab =[];
                    spjForm.forEach(function (spj) {
                        spj.setAttribute('data-id-projet',idPj);
                        promiseTab.push(SousProjet.insertSpj(spj.id));
                    });
                    Promise.all(promiseTab).then(()=>{
                        addProjetToTable(idPj);
                        cancelCreatePj();
                    });
                }else{
                    //si creation projet vide
                    addProjetToTable(idPj,'add','VIDE');
                    cancelCreatePj();
                }
            })
        }

    }
}


///////////////////////////// MODIFIER PROJET ///////////////////////////////////////
export function formForUpdtPj(event)
{
    event.preventDefault();
    event.stopPropagation();
    let base = Utils.getinfosBase(event);
    let periodicite = getPeriodicite(base.span.id);
    let tds = getTdsAndInput(periodicite,base.id);
    Utils.inverseBtns(base.span,base.id,periodicite+'_projet',updatePj,cancelUpdatePj);

    let attrList={
        "nom":tds.tdNom.innerText,
    }
    getFormForPj(periodicite,base.id,attrList);
    document.querySelectorAll('.boutons_projet_'+periodicite+'_'+base.id).forEach(function (bouton) {
        bouton.style.visibility='hidden';
    });

}

function cancelUpdatePj(event) {
    event.preventDefault();
    event.stopPropagation();
    let base = Utils.getinfosBase(event);
    let periodicite = getPeriodicite(base.span.id);
    let tds = getTdsAndInput(periodicite,base.id);
    Projet.loadOne(base.id).then((projet)=>{
        Utils.inverseBtns(base.span,base.id,periodicite+'projet',formForUpdtPj,validerSupPj);
        tds.tdNom.innerText="";
        let h5 = document.createElement("h5");
        h5.innerText=projet.nom;
        tds.tdNom.appendChild(h5);
        //on affiche les boutons trie et ajout sous-projet
        if (tds.trProjet.dataset.visibility==='visible')
        {
            document.querySelectorAll('.boutons_projet_'+periodicite+'_'+base.id).forEach(function (bouton) {
                bouton.style.visibility='visible';
            });
        }

    })

}

function updatePj(event)
{
    event.preventDefault();
    event.stopPropagation();
    let base = Utils.getinfosBase(event);
    let periodicite = getPeriodicite(base.span.id);
    let tds = getTdsAndInput(periodicite,base.id);
    if (Valid.projetForm(periodicite,base.id))
    {
        let pj = ({
            "id":base.id,
            "nom":tds.inputNom.value,
        });
        Projet.updatePJ(pj).then(()=>{
            formToText(periodicite,pj.id);
            let autrePerio;
            //modification aussi de la ligne projet sur autre periodicite
            //si present
            if (periodicite === 'MENSUEL')
            {
                autrePerio='ONESHOT';
            }else{
                autrePerio='MENSUEL';
            }
            let tdNomAutrePerio = document.getElementById('nom_projet_'+autrePerio+'_'+pj.id);
            if (tdNomAutrePerio!==null)
            {
                //une ligne sur autre perdiodicite est presente
                //modification du nom
                tdNomAutrePerio.firstElementChild.innerText=pj.nom;
            }
            Utils.inverseBtns(base.span,base.id,periodicite+'_projet',formForUpdtPj,validerSupPj);

        });
    }

}


////////////////////////////SUPPRIMER PROJET ///////////////////////////////////////
export function validerSupPj(event)
{
    event.preventDefault();
    event.stopPropagation();
    let base = Utils.getinfosBase(event);
    let periodicite = getPeriodicite(base.span.id);
    Utils.inverseBtns(base.span,base.id,periodicite+'projet',removePj,cancelSupPj);
    document.querySelectorAll('.boutons_projet_'+periodicite+'_'+base.id).forEach(function (bouton) {
        bouton.style.visibility='hidden';
    });
}

function cancelSupPj(event)
{
    event.preventDefault();
    event.stopPropagation();
    let base = Utils.getinfosBase(event);
    let periodicite = getPeriodicite(base.span.id);
    let tds = getTdsAndInput(periodicite,base.id);
    Utils.inverseBtns(base.span,base.id,periodicite+'projet',formForUpdtPj,validerSupPj);
    //on affiche les boutons trie et ajout sous-projet
    if (tds.trProjet.dataset.visibility==='visible')
    {
        document.querySelectorAll('.boutons_projet_'+periodicite+'_'+base.id).forEach(function (bouton) {
            if (bouton.classList.contains('btn-trie-spj'))
            {
                if (parseInt(tds.trProjet.dataset.nbSousProjets)>1)
                {
                    bouton.style.visibility = 'visible';
                }
            }else {
                bouton.style.visibility = 'visible';
            }
        });
    }
}

function removePj(event)
{
    event.preventDefault();
    event.stopPropagation();
    let base = Utils.getinfosBase(event);
    let periodicite = getPeriodicite(base.span.id);
    let tds = getTdsAndInput(periodicite,base.id);
    Projet.removePj(base.id).then(()=>{
        removeTrProjet(base.id,periodicite);
        let autrePerio;
        //suppression aussi des lignes projet et sous-projet sur autre periodicite
        //si present
        if (periodicite === 'MENSUEL')
        {
            autrePerio='ONESHOT';
        }else{
            autrePerio='MENSUEL';
        }
        let trAutrePerio = document.getElementById('projet_'+autrePerio+'_'+base.id);

        if (trAutrePerio!==null)
        {
            trAutrePerio.remove();
        }
        let trSpjAutrePerio = document.querySelectorAll('.spj_'+autrePerio+'_'+base.id);
        if (trSpjAutrePerio!==null)
        {
            trSpjAutrePerio.forEach(function (trProjet) {
                trProjet.remove();
            });
        }
        Refresh.refreshGlobaux();
        Refresh.refreshCategorie();
        Refresh.getFicheIndeValuesOnDom();
    });
}


/////////////////////////////// OUTILS //////////////////////////////////////////////


/**Retourne les elements du tableau du projet
 * si idPj = null retourne les lignes de creation de projet
 *
 * @param periodicite
 * @param idPj
 * @returns {{tdPv: HTMLTableCellElement, trProjet: HTMLTableRowElement, inputNom: HTMLInputElement, tdNom: HTMLTableCellElement, tdRoi: HTMLTableCellElement, tdBtn: HTMLTableCellElement}}
 */

export function getTdsAndInput(periodicite,idPj) {
    let id = getIdSuffix(periodicite,idPj);
    let tdNom=document.getElementById('nom_'+id);
    let inputNom=document.getElementById('input_nom_'+id);
    let tdRoi=document.getElementById('roi_'+id);
    let tdPv= document.getElementById('pv_'+id);
    let tdHeure = document.getElementById('heures_inde_'+id);
    let trProjet=tdNom.parentElement;

    let tdBtn = document.getElementById('button_'+id);
    return ({
        "trProjet":trProjet,
        "tdNom":tdNom,
        "inputNom":inputNom,
        "tdRoi":tdRoi,
        "tdPv":tdPv,
        "tdBtn":tdBtn,
        "tdHeure":tdHeure
    })
}

/**Genere le formulaire pour le projet, recupere la ligne sur le DOM
 *
 * @param callbackCheck
 * @param callbackCancel
 * @param idPj
 * @param attrList
 */

function getFormForPj(periodicite,idPj=null,attrList=null)
{
    let id= getIdSuffix(periodicite,idPj);
    let tds = getTdsAndInput(periodicite,idPj);
    //definitiion value de base si pas fournis
    if (attrList==null)
    {
        attrList={
            'nom':nom,

        }
    }
    //creation input nom
    let attrNom = {
        "value":attrList.nom,
        "type":'text',
        "id":"input_nom_"+id,
        "minlength":4,
    }
    let inputNom = Utils.getInput(attrNom);
    //remplacement des element
    //Nom
    tds.tdNom.innerText="";
    tds.tdNom.appendChild(inputNom);

}

/**Retourne le suffixe pour les id des elements du projet
 *
 * @param idPj
 * @returns {string}
 */

function getIdSuffix(periodicite=null,idPj=null)
{
    let id;
    if (idPj!= null)
    {
        id = 'projet_'+periodicite+'_'+idPj;
    }else{
        id = 'projet';
    }
    return id;
}

/**Remplace les value des champs de saisie par du text pour la cellule nom
 * ajoute un h5
 *
 * @param idPj
 */

function formToText(periodicite,idPj)
{

    let tds = getTdsAndInput(periodicite,idPj);
    let h5 = document.createElement("h5");
    h5.innerText = tds.inputNom.value;
    tds.tdNom.innerText ="";
    tds.tdNom.appendChild(h5);


}

export function getPeriodicite(idElement)
{
    let ids = idElement.split('_');
    let periodicite = ids[ids.length-2];
    return periodicite;
}

//////////////////////// Construction Tableau projet ///////////////////////////////////
//projet = id,nom,roi,pv


/**Insere une ligne avec les informations du projet passer en parametre dans le tableau
 *
 * @param projet
 * @param periodicite
 * @param tbody
 */

function insertTrProjet(projet,periodicite,tbody)
{
    //definition de la base de l'id pour chaque elemnt
    let id='projet_'+periodicite+'_'+projet.id;
    //creation de la ligne
    let tr = tbody.insertRow(0);
    if (periodicite!=='VIDE')
    {
        tr.addEventListener("click", List.afficherMasquerSPJ);
        tr.style.cursor='pointer';
    }
    tr.id=id;
    tr.className='projet-ligne ligne_'+periodicite;
    //ajout des datas
    //visibility des sous-projets
    tr.setAttribute('data-visibility','hidden');
    //nombre sous-projets
    tr.setAttribute('data-nb-sous-projets',projet.nbSousProjets);
    //cellule Nom du projet
    let tdNom = tr.insertCell();
    tdNom.id='nom_'+id;
    tdNom.setAttribute('data-nom',projet.nom);
    let h5 = document.createElement('h5');
    h5.innerText=Utils.capitalize(projet.nom);
    tdNom.appendChild(h5);
    //cellule roi

    let visiPrix = document.getElementById('visiPrix');
    if(isAdmin === "true" && visiPrix.checked===false){

    let tdRoi = tr.insertCell();
    tdRoi.id='roi_'+id;
    let roi = parseFloat(projet.roi);
    tdRoi.setAttribute('data-roi',roi);
    tdRoi.className='percent-text projet-text roi';
    Utils.setClassRoi(roi,tdRoi);
    //cellule prix de vente
    let tdPv = tr.insertCell();
    tdPv.id = 'pv_'+id;
    tdPv.className='money-text projet-text  infos_'+periodicite;
    tdPv.innerText=Utils.formatEuro(projet.pv);
    //ajout des data sur le projet
    tdPv.setAttribute('data-cost',projet.cout);
    tdPv.setAttribute('data-prix-vente',projet.pv);
    tdPv.setAttribute('data-cost-previ',projet.coutPrevi);
    tdPv.setAttribute('data-pv-previ',projet.pvPrevi);
    tdPv.setAttribute('data-cost-non-previ',projet.coutNonPrevi);
    tdPv.setAttribute('data-pv-non-previ',projet.pvNonPrevi);}


    //cellule nombre heure de independant
    let idInde = projet.idInde;
    let heuresInde = projet.heuresInde;
    //ajout de la cellule si informations presente
    //c.a.d si filtre par independant
    if (idInde!==null && heuresInde!==null)
    {
        let tdHeures = tr.insertCell();
        tdHeures.id='heures_inde_projet_'+periodicite+'_'+projet.id;
        //class pour retrouver chaque heures de l'inde
        //pour refresh fiche
        tdHeures.className='heures_inde_projet_'+idInde;
        tdHeures.setAttribute('data-heures',heuresInde);
        tdHeures.setAttribute('data-id-inde',idInde);
        tdHeures.setAttribute('data-cost',projet.coutInde);
        tdHeures.setAttribute('data-cost-autre',projet.coutAutre);
        tdHeures.innerText=heuresInde+' H';
        tdHeures.setAttribute('data-nb-sous-projets-inde',projet.nbSousProjets);}



    if(isAdmin === "true" && visiPrix.checked===false){
    //cellules des boutons
    let tdBtn = tr.insertCell();
    tdBtn.id='button_'+id;
    // fonction qui nous genere le span avec les boutons
    let span = getSpanBtnsProjet(projet.id,periodicite);
    tdBtn.appendChild(span);
    let divPerio = document.getElementById('liste_'+periodicite);
    divPerio.dataset.nbProjets = parseInt(divPerio.dataset.nbProjets)+1;
}}

/**Genere le span conteneur des boutons d'une ligne d'un projet
 *
 * @param idProjet
 * @param periodicite
 * @param type
 * @returns {HTMLSpanElement}
 */

function getSpanBtnsProjet(idProjet,periodicite)
{
    let id = 'projet_'+periodicite+'_'+idProjet;
    let span = document.createElement("span");
    span.id='btns_'+id;
    //bouton modifier
    let btnMdf = Utils.getBtnMdf(formForUpdtPj);
    btnMdf.id = 'mdf_'+id;
    span.appendChild(btnMdf);
    //bouton supprimer
    let btnSup = Utils.getBtnSup(validerSupPj);
    btnSup.id='sup_'+id;
    span.appendChild(btnSup);
    //bouton ajouter sous-projet
    let btnAdd = Utils.getBtnAdd(SousProjet.formAddSpj);
    btnAdd.id='add_spj_'+periodicite+'_'+idProjet;
    btnAdd.title='Ajouter un sous-projet';
    btnAdd.classList.add('boutons_projet_'+periodicite+'_'+idProjet);
    btnAdd.setAttribute('data-nb-form',0);
    btnAdd.setAttribute('data-type-form','add');
    //bouton trie par designation
    let btnSortAlpha = Utils.getBtnAlpha(Tris.trierSousProjets,'up');
    btnSortAlpha.id='trie_alpha_spj_'+periodicite+'_'+idProjet;
    btnSortAlpha.classList.add('boutons_projet_'+periodicite+'_'+idProjet);
    //bouton trie par date
    let btnSortNumeric= Utils.getBtnNumeric(Tris.trierSousProjets,'down');
    btnSortNumeric.id='trie_numeric_spj_'+periodicite+'_'+idProjet;
    btnSortNumeric.classList.add('boutons_projet_'+periodicite+'_'+idProjet);
    span.appendChild(btnAdd);
    span.appendChild(btnSortAlpha);
    span.appendChild(btnSortNumeric);
    if (periodicite==='VIDE')
    {
        btnSortAlpha.style.visibility="hidden";
        btnSortNumeric.style.visibility="hidden";
    }else{
        btnAdd.style.visibility="hidden";
        btnSortAlpha.style.visibility="hidden";
        btnSortNumeric.style.visibility="hidden";
    }

    return span;
}

function getTableauProjet(projets,periodicite)
{
    //creation du tableau
    let table = document.createElement('table');
    table.id='list_projet_'+periodicite;
    table.className='projet table col table-sm';
    //creation de l'en tete
    let tHead = getTheadProjet(periodicite);
    table.appendChild(tHead);
    //creation du tBody
    let tBody = document.createElement("tbody");
    tBody.setAttribute('id','tBody_'+periodicite);
     if (projets[0] === undefined)
     {
         insertTrProjet(projets,periodicite,tBody);
     }else{
         projets.forEach(function (projet) {
             insertTrProjet(projet,periodicite,tBody);
         })
     }

    table.appendChild(tBody);
    return table;
}

/**Retourne la div d'un tableau de projets
 *
 * @param projets
 * @param periodicite
 * @returns {HTMLDivElement}
 */

function getDivProjet(projets,periodicite)
{
    //div qui contiendra tous les elemnts
    let divBase = document.createElement("div");
    divBase.id='liste_'+periodicite;
    divBase.setAttribute('data-nb-projets',projets.length);
    divBase.className='container mb-3';
    //titre du tableau selon periodicite
    let titre = document.createElement("h1");
    titre.className='text-center mb-5 titre';
    titre.id='titre_'+periodicite;
    titre.innerText= 'Projets '+Utils.capitalize(periodicite);
    titre.addEventListener("click", List.afficherListe);
    divBase.appendChild(titre);
    //div row qui contiendra le formulaire general
    let divRow = document.createElement("div");
    divRow.className='row';
    divBase.appendChild(divRow);
    //formulaire qui contiendra le tableau
    let form = document.createElement("form");
    form.className='w-100';
    divRow.appendChild(form);
    if (projets.length!==0)
    {
        form.appendChild(getTableauProjet(projets,periodicite));
    }else{
        divBase.style.display="none";
    }


    return divBase;
}



export function refreshProjetLis(projetList)
{
    let mobile = document.getElementById('div-general').dataset.mobile;
    //div des projets mensuel
    let divMensuel = document.getElementById('liste_MENSUEL');
    let newDiv = getDivProjet(projetList.MENSUEL,'MENSUEL');
    divMensuel.replaceWith(newDiv);
    //div des projets oS
    let divOneShot = document.getElementById('liste_ONESHOT');
     newDiv = getDivProjet(projetList.ONESHOT,'ONESHOT');
     divOneShot.replaceWith(newDiv);
     if (mobile==='false')
     {
    let titre = document.getElementById('titre-page')
    let span =titre.children[0];
    if (projetList.MENSUEL.length===0 && projetList.ONESHOT.length===0)
    {
        titre.innerText="Aucun projets trouvé";
    }else{
        titre.innerText="Ajouter des projets";

    }
    titre.insertAdjacentElement("afterbegin", span);
     }
    Filtre.needDisplayNone();
    Refresh.refreshGlobaux();
    Refresh.refreshCategorie();



}
export function addProjetToTable(idProjet,oldPeriodicite=null,newPeriodicite=null) {


    if (newPeriodicite==='VIDE')
    {
        let tds = getTdsAndInput(oldPeriodicite,idProjet);
        let projet = {
            "id":idProjet,
            "nom":tds.tdNom.innerText,
            "roi":Utils.calculRoi(parseFloat(tds.tdPv.dataset.cost),parseFloat(tds.tdPv.dataset.prixVente)),
            "pv":tds.tdPv.dataset.prixVente
        };
        let divVide = document.getElementById('liste_VIDE');
        if (divVide.style.display === "none")
        {
            //div masquer car pas de projets
            let newDiv = getDivProjet(projet,'VIDE');
            divVide.replaceWith(newDiv);
        }else{
            let tableau = document.getElementById('list_projet_'+newPeriodicite);
            let tBody = tableau.children[1];
            insertTrProjet(projet,newPeriodicite,tBody);
        }

    }else{
        let filtres = Filtre.getFiltreValues();
        Projet.loadOneByFiltres(filtres,idProjet).then((projetList)=>{

            if (projetList.MENSUEL.length !== 0 && oldPeriodicite !== 'MENSUEL')
            {
                //div des projets mensuel
                let divMensuel = document.getElementById('liste_MENSUEL');
                if (divMensuel.style.display==='none')
                {
                    //div masquer car pas de projets
                    let newDiv = getDivProjet(projetList.MENSUEL,'MENSUEL');
                    divMensuel.replaceWith(newDiv);
                }else{
                    let tableau = document.getElementById('list_projet_'+'MENSUEL');
                    let tBody = tableau.children[1];
                    insertTrProjet(projetList.MENSUEL,'MENSUEL',tBody);
                }

            }
            if (projetList.ONESHOT.length !== 0 && oldPeriodicite !== 'ONESHOT')
            {
                let divOS = document.getElementById('liste_ONESHOT');
                if (divOS.style.display === 'none')
                {
                    let newDiv = getDivProjet(projetList.ONESHOT,'ONESHOT');
                    divOS.replaceWith(newDiv);
                }else{
                    let tableau = document.getElementById('list_projet_'+'ONESHOT');
                    let tBody = tableau.children[1];
                    insertTrProjet(projetList.ONESHOT,'ONESHOT',tBody);
                }
            }

        });
    }
    Refresh.refreshGlobaux();
    Refresh.refreshCategorie();
    Refresh.getFicheIndeValuesOnDom();
}


/**Supprime la lige d'un projet et ses sous-projets si present dans un tableau
 * Si la taille du tableau est a 0, masque le tableau
 * @param idProjet
 * @param periodicite
 */

 export function removeTrProjet(idProjet,periodicite) {
    let trProjet = document.getElementById('projet_'+periodicite+'_'+idProjet);
    let divPerio = document.getElementById('liste_'+periodicite);
    let trsSpjs = document.querySelectorAll('.spj_'+periodicite+'_'+idProjet);
    trsSpjs.forEach(function (spj) {
        spj.remove();
    })
    trProjet.remove();
    divPerio.dataset.nbProjets = parseInt(divPerio.dataset.nbProjets)-1;
    if (parseInt(divPerio.dataset.nbProjets)===0)
    {
        divPerio.style.display="none";
    }
    refreshListeVide();
}

function refreshListeVide()
{
    Projet.getProjetsVide().then((projetsVide)=>{
        let divVide = document.getElementById('liste_VIDE');
        let tableVide = document.getElementById('list_projet_VIDE');
        let newDiv = getDivProjet(projetsVide.VIDE,'VIDE');
        if (tableVide!==null)
        {
            newDiv.querySelector('table').style.display= tableVide.style.display;
        }
        divVide.replaceWith(newDiv);
    });
}