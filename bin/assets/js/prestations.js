import * as Utils from './utils/utils';
import * as Presta from './ajax/prestation_ajax';
import * as SousProjet from './sousProjet';
import * as Refresh from './refresh';
import * as Valid from "./validation";
///////////////////////////////// AJOUTER PRESTATION ////////////////////////////////

/**Creation du formulaire pour ajouter une prestation
 * ligne ajouter en dessous de la ligne titre "Autre Prestation"
 * @param event
 */
let isAdmin = true;


export function formAddPresta(event) {
    event.preventDefault();
    //Recuperation des infos pour traitement
    // element bouton ajouter
    let btnAdd = event.currentTarget;
    let nbForm = parseInt(btnAdd.dataset.nbForm);
    nbForm++;
    btnAdd.dataset.nbForm = nbForm;
    //id du bouton poour recuperer id de la prestation (id BDD)
    let idAdd = btnAdd.id;
    let idSpj = Utils.getId(idAdd);
    //idpresta depend du nombre de formulaires
    //permet ajouts multiples
    let idPresta = idSpj+'form'+nbForm;

    //element tr lige de "Ajout prestations"
    let trBase = btnAdd.parentElement.parentElement;
    let trInput = document.getElementById('add_presta_spj_'+idSpj);

    if (trInput==null || isNaN(idSpj))
    {
        //creation des element pour formulaire
        //ligne pour afficher form
        trInput = document.createElement('tr');
        trInput.id='add_presta_'+idPresta;
        trInput.className='compl_spj_'+idSpj+' form_compl_spj_'+idSpj;
        trInput.setAttribute('data-id-sous-projet',idSpj);
        //cellule "Detail"
        let tdDetail =trInput.insertCell();
        tdDetail.colSpan=2;
        tdDetail.id = 'detail_presta_'+idPresta;
        //input detail
        let attrDetail ={
            "id":'input_detail_presta_'+idPresta,
            "placeholder":'Detail',
            'type':'text',
            'minlenght':'4'

        }
        let inputDetail = Utils.getInput(attrDetail);
        tdDetail.appendChild(inputDetail);

        //cellule "Montant"
        let tdMnt = trInput.insertCell();
        tdMnt.setAttribute('data-cost',1);
        tdMnt.id='mnt_presta_'+idPresta;
        let attrMnt={
            "id":'input_mnt_presta_'+idPresta,
            "type":"number",
            'value':1,
            'min':1,
            'step':0.01,
        }
        let inputMnt = Utils.getInput(attrMnt);

        //TODO:avoir id dynamique si multiple ajout
        inputMnt.addEventListener("input",refreshOnInputMnt);
        tdMnt.appendChild(inputMnt);


        //cellule des boutons
        let tdBtns = trInput.insertCell();
        tdBtns.colSpan=2;

        //span regroupant les boutons
        let span = document.createElement('span');
        span.id="btns_presta_"+idSpj;

        //bouton valider formaulaire
        let btnCheck = Utils.getBtnCheck();
        //TODO:avoir id dynamique si multiple ajout
        btnCheck.id= 'valid_add_presta_'+idPresta;
        btnCheck.addEventListener("click",validerAjoutPrestation);

        //bouton annuler
        let btnCancel = Utils.getBtnCancel(cancelAdd);
        btnCancel.id='cancel_add_presta_'+idPresta;

        span.appendChild(btnCheck);
        span.appendChild(btnCancel);

        //On masque les boutons lors de la creation d'un sous projet
        if (isNaN(idSpj))
        {
            btnCheck.style.display="none";
        }
        //imbrication des elements
        tdBtns.appendChild(span);

        //insertion de la ligne form apres ligne ajout
        trBase.insertAdjacentElement('afterend',trInput);
        //ligne pour afficher les messages
        let trMessage = document.createElement('tr');
        let tdMessage = trMessage.insertCell();
        tdMessage.colSpan=4;
        trMessage.id='message_for_presta_'+idPresta;
        trMessage.style.display='none';
        trBase.insertAdjacentElement('afterend',trMessage);
        Refresh.refreshRoiPjAndSpj(idSpj);
    }

}

function cancelAdd(event)
{
    let id = Utils.getId(event.currentTarget.id);
    let tds = getTdAndInputs(id);
    let btnAdd = document.getElementById('add_presta_'+tds.idSpj);
    btnAdd.dataset.nbForm = parseInt(btnAdd.dataset.nbForm)-1;
    let trMessage = document.getElementById("message_for_presta_"+id);
    trMessage.remove();
    Utils.cancel(event);
    Refresh.refreshRoiPjAndSpj(tds.idSpj);
}

/**function appeler lors validation formulaire ajout prestation
 *
 * @param event
 */
function validerAjoutPrestation(event)
{
    event.preventDefault();
    //recuperation des elements de base
    let base = Utils.getinfosBase(event);
    let span = base.span;

    let tr = base.tr;
    let idAddPresta = Utils.getId(tr.id);
    tr.classList.remove('form_compl_spj_'+tr.dataset.idSousProjet);
    //recuperation des cellules et champs
    let tds = getTdAndInputs(idAddPresta);
    let idSpj = tds.idSpj;
    let tdDetail = tds.tdDetail;
    let tdMnt = tds.tdMnt;
    let inputDetail = tds.inputDetail;
    let inputMnt = tds.inputMnt;
    //recuperation des saisies
    let detail = inputDetail.value;
    let mnt = inputMnt.value;

    if (Valid.prestaForm(idAddPresta))
    {

        let presta = ({
            "detail":detail,
            "montant":mnt,
        })
        Presta.addPresta(idSpj,presta).then((idPresta)=>{
            //recupere tr element conteneur des infos de la presta
            tr.id='presta_'+idPresta;

            //cellule detail
            tdDetail.id = 'detail_presta_'+idPresta;
            tdDetail.innerText=inputDetail.value;

            //cellules montant
            tdMnt.innerText=Utils.formatEuro(mnt);
            tdMnt.id='mnt_presta_'+idPresta;

            //changement des boutons
            Utils.inverseBtns(span,idPresta,'presta',modifierPresta,validSupPresta);
            span.id='btns_presta_'+idPresta;
            Refresh.refreshRoiPjAndSpj(idSpj);
            //changement des data sur btnAdd presta
            let btnAdd = document.getElementById('add_presta_'+tds.idSpj);
            btnAdd.dataset.nbForm = parseInt(btnAdd.dataset.nbForm)-1;
            Utils.afficherMessage('Ajout effectué avec succès','presta_'+idAddPresta,'success');
        },
            (message)=>{
                Utils.afficherMessage(message,'presta_'+idAddPresta,'error');
            }
        );
    }
}

///////////////////////////////// MODIFIER PRESTA ////////////////////////////////////////////
/**
 *
 * @param event
 */
export function modifierPresta(event)
{
    event.preventDefault();
    //recuperation des elements de base
    let base = Utils.getinfosBase(event);
    // span conteneur des boutons
    let span = base.span;
    //id BDD de la presta
    let idPresta = base.id;
    //tr element ligne
    let tr = base.tr;
    //cellules contenat les inputs
    let tds = getTdAndInputs(idPresta);
    base.tr.classList.add('form_compl_spj_'+base.tr.dataset.idSousProjet);
    let tdDetail = tds.tdDetail;
    let tdMnt = tds.tdMnt;
    let mnt = tdMnt.innerText.replace('€','').trim();
    //mise en place input dans les cellules
    //text/number ==> value des input
    let attrDetail ={
        "id":'input_detail_presta_'+idPresta,
        "value":tdDetail.innerText,
        'type':'text',
        'minlenght':'4'

    }
    let attrMnt={
        "id":'input_mnt_presta_'+idPresta,
        "type":"number",
        'value':parseFloat(mnt),
        'min':1,
        'step':0.01,
    }
    let inputDetail = Utils.getInput(attrDetail);
    let inputMnt = Utils.getInput(attrMnt);
    inputMnt.addEventListener("input",refreshOnInputMnt);
    inputMnt.value = parseFloat(mnt);
    tdMnt.innerText="";
    tdMnt.appendChild(inputMnt);
    tdDetail.innerText="";
    tdDetail.appendChild(inputDetail);
    //changement des boutons
    Utils.inverseBtns(span,idPresta,'resta',validUpdate,cancelMdf);
    //ligne pour les messages
    let trMessage = document.createElement('tr');
    let tdMessage = trMessage.insertCell();
    tdMessage.colSpan=4;
    trMessage.id='message_for_presta_'+idPresta;
    trMessage.style.display='none';
    base.tr.insertAdjacentElement("beforebegin", trMessage);
}

/**Annulation de la modification
 * recherche info de la presta en base pour revalorise les cellules
 * au cas ou modifications *
 * @param event
 */
function cancelMdf(event)
{
    event.preventDefault();
    //recuperation des elements
    //span conteneur des boutons
    let base = Utils.getinfosBase(event);
    let span = base.span;
    //id presta BDD
    let idPresta = base.id
    //cellules contenat les inputs
    let tds = getTdAndInputs(idPresta);
    let idSpj = Utils.getId(tds.tBody.id);
    let tdDetail = tds.tdDetail;
    let tdMnt = tds.tdMnt;
    base.tr.classList.remove('form_compl_spj_'+base.tr.dataset.idSousProjet);
    Presta.loadPresta(idPresta).then((presta)=>{
        tdDetail.innerText=presta.detail;
        tdMnt.innerText= Utils.formatEuro(presta.montant);
        tdMnt.dataset.cost = presta.montant;
        Utils.inverseBtns(span,presta.id,'presta',modifierPresta,validSupPresta);
        Refresh.refreshRoiPjAndSpj(idSpj);
        let trMessage = document.getElementById("message_for_presta_"+idPresta);
        trMessage.remove();
    })
}

export function getTrPrestaHead(id)
{
    let trPrestaHead = document.createElement('tr');
    trPrestaHead.id='list_presta_spj_'+id;
    let thIconAdd = document.createElement('th');
    thIconAdd.colSpan = 2;
    //colonne vide Role si User
    let thVide = document.createElement('th');
    thVide.innerText="Prestations";
    //colonne vide si Role User , de recalage colonne
    let thRecal = document.createElement('th');
    // bouton pour ajouter une prestation
    let btnAdd = Utils.getBtnAdd();
    btnAdd.setAttribute('id','add_presta_'+id);
    btnAdd.setAttribute('class','bouton btn btn-add');
    btnAdd.setAttribute('title','Ajouter une prestation');
    btnAdd.setAttribute('data-nb-form',0);
    btnAdd.addEventListener("click",formAddPresta);

    thIconAdd.innerText = "Prestations";
    thIconAdd.insertAdjacentElement('afterbegin',btnAdd);

    //colonne montant
    let thMnt = document.createElement('th');
    thMnt.innerText="Montant";
        if(visiPrix.checked === true){
        trPrestaHead.appendChild(thIconAdd).style.display="none";
        trPrestaHead.appendChild(thVide);
        trPrestaHead.appendChild(thRecal);
        trPrestaHead.appendChild(thMnt).style.display="none";
    }
    else{
        trPrestaHead.appendChild(thIconAdd);
        trPrestaHead.appendChild(thMnt);
    }
    return trPrestaHead;
}

/**Modifie la presta si champs sont valide
 * TODO:retour des erreurs
 *
 * @param event
 */
function validUpdate(event)
{
    event.preventDefault();
    //recuperation des infos de base pour construction/modification
    let base = Utils.getinfosBase(event);
    let span = base.span;
    let idPresta = base.id;
    base.tr.classList.remove('form_compl_spj_'+base.tr.dataset.idSousProjet);
    //cellules contenat les inputs
    let tds = getTdAndInputs(idPresta);
    let idSpj = Utils.getId(tds.tBody.id);
    let tdDetail = tds.tdDetail;
    let tdMnt = tds.tdMnt;
    //value des input
    let detail = tds.inputDetail.value;
    let mnt = tds.inputMnt.value;
    if (Valid.prestaForm(idPresta))
    {
        let presta = ({
            "id": idPresta,
            "detail": detail,
            "montant": mnt
        });
        Presta.update(presta).then(()=>{
            tdDetail.innerText=presta.detail;
            tdMnt.innerText= Utils.formatEuro(presta.montant);
            Utils.inverseBtns(span,presta.id,'presta',modifierPresta,validSupPresta);
            Refresh.refreshRoiPjAndSpj(idSpj);
            Utils.afficherMessage('Modifications effectuées avec succès','presta_'+idPresta,'success');
        },
            (message)=>{
                Utils.afficherMessage(message,'presta_'+idPresta,'error');
            })
    }
}
///////////////////////////////////////// SUPPRESSION ////////////////////////////////////////
/**Demande validation suppression
 * changement des boutons modifier/supprimer par valider/annuler
 * passage de id de la presta dans id des elements
 * @param event
 */
export function validSupPresta(event)
{
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let span =base.span;
    let id = base.id;
    Utils.inverseBtns(span,id,'presta',supprimer,cancelSup);

}

/**Appeler lors de l'annulation de la suppression
 *
 * @param event
 */
function cancelSup(event)
{
    event.preventDefault();
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let span =base.span;
    let id = base.id;
    Utils.inverseBtns(span,id,'presta',modifierPresta,validSupPresta);
}

/**Appeler lors de la validation de la suppression
 *
 * @param event
 */
function supprimer(event)
{
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let span =base.span;
    let idPresta = base.id;
    let tds = getTdAndInputs(idPresta);
    let idSpj = Utils.getId(tds.tBody.id);
    let tr = base.tr;
    Presta.supprimer(idPresta).then(()=>{
        tr.remove();
        Refresh.refreshRoiPjAndSpj(idSpj);
    });
}

////////////////////////// OUTILS ///////////////////////////////////



/**Retourne les cellules et les champs de saisies si ils sont presents
 *
 * @param idPresta
 * @param idSpj
 * @returns {{tdMnt: HTMLElement, tdDetail: HTMLElement, inputMnt: HTMLElement, inputDetail: HTMLElement}}
 */
export function getTdAndInputs(idPresta)
{

    let tdDetail= document.getElementById('detail_presta_'+idPresta);
    let inputDetail = document.getElementById('input_detail_presta_'+idPresta);
    let tdMnt=document.getElementById('mnt_presta_'+idPresta);
    let inputMnt=document.getElementById('input_mnt_presta_'+idPresta);
    let tBody =tdDetail.parentElement.parentElement;
    let trPresta = tdDetail.parentElement;

    return ({
        "tdDetail":tdDetail,
        "inputDetail": inputDetail,
        "tdMnt":tdMnt,
        "inputMnt":inputMnt,
        "tBody":tBody,
        "trPresta":trPresta,
        "idSpj":trPresta.dataset.idSousProjet,
    })

}


function refreshOnInputMnt(event)
{
    let input = event.currentTarget;
    let cost = input.value
    let tdMnt = input.parentElement
    tdMnt.setAttribute('data-cost',cost);
    let idPresta = Utils.getId(input.id);
    let tds = getTdAndInputs(idPresta);
    let idSpj = Utils.getId(tds.tBody.id);
    Refresh.refreshRoiPjAndSpj(idSpj);
}