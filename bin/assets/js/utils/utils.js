
export  function  getBtnSup(callback=null,width='1.5em',height='1.5em',color='00ff60') {
    let btnSup = document.createElement('button');
    btnSup.setAttribute('title','Supprimer');
    btnSup.setAttribute('class','bouton btn-valid-sup btn');
    btnSup.type="button";
    if (callback!=null)
    {
        btnSup.addEventListener("click",callback);
    }
    //console.log("creation bouton sup pour tout nouvel affichage")
    let iconSup = getIcon('bi bi-trash','currentColor','1.5em','1.5em',
    'M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z',
    'M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z');
    btnSup.appendChild(iconSup);
    return btnSup;
}

export  function getBtnCheck (callback=null,width='1.5em',height='1.5em',color='currentColor') {
    let btnCheck = document.createElement('button');
    btnCheck.setAttribute('title','Valider ');
    btnCheck.setAttribute('class','bouton btn btn-valid');
    btnCheck.type="submit";
    if (callback!=null)
    {
        btnCheck.addEventListener("click",callback);
    }
    let iconCheck = getIcon('bi bi-check','currentColor','2em','2em','M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z');
    btnCheck.appendChild(iconCheck);
    return btnCheck;
}
export function  getBtnCancel(callback=null,width='1.5em',height='1.5em',color='00ff60') {
    let btnCancel = document.createElement('button');
    btnCancel.setAttribute('title','Annuler');
    btnCancel.setAttribute('class','bouton btn-cancel btn');
    btnCancel.type="button";
    if (callback!=null)
    {
        btnCancel.addEventListener("click",callback);
    }
    let iconCancel = getIcon('bi bi-x','currentColor','2em','2em','M11.854 4.146a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708-.708l7-7a.5.5 0 0 1 .708 0z','M4.146 4.146a.5.5 0 0 0 0 .708l7 7a.5.5 0 0 0 .708-.708l-7-7a.5.5 0 0 0-.708 0z');
    btnCancel.appendChild(iconCancel);
    return btnCancel;

}
export function  getBtnMdf(callback=null,width='1.5em',height='1.5em',color='00ff60') {
    let btnMdf = document.createElement('button');
    btnMdf.setAttribute('title','Modifier');
    btnMdf.setAttribute('class','bouton btn btn-mdf');
    btnMdf.type="button";
    if (callback!=null)
    {
        btnMdf.addEventListener("click",callback);
    }
    let iconCheck = getIcon('bi bi-pencil','currentColor',
        '1.5em','1.5em','M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z',
        'M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z');
    btnMdf.appendChild(iconCheck);
    return btnMdf;
}

export function getBtnAdd(callback=null,width='2.2em',height='2.2em',color='00ff60')
{
    let btnAdd = document.createElement('button');
    btnAdd.setAttribute('title','Ajouter');
    btnAdd.setAttribute('class','bouton btn btn-add');
    btnAdd.type="button";
    if (callback!=null)
    {
        btnAdd.addEventListener("click",callback);
    }
    let iconCheck = getIcon('bi bi-plus','currentColor',
        '2.2em','2.2em','M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z',
        'M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z');
    btnAdd.appendChild(iconCheck);
    return btnAdd;
}



export function getBtnDash(callback=null,width='1.5em',height='1.5em',color='00ff60')
{
    let btnDash = document.createElement('button');
    btnDash.setAttribute('title','Ajouter');
    btnDash.type="button";
    if (callback!=null)
    {
        btnDash.addEventListener("click",callback);
    }
    let iconDash = getIcon('bi bi-dash','currentColor',
        '1.5em','1.5em','M3.5 8a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 0 1H4a.5.5 0 0 1-.5-.5z');
    btnDash.appendChild(iconDash);
    return btnDash;
}

// ///////////////insertion du bouton dupliquer ici////////////////
// export function getBtnDupliq(callback=null,width='1.5em', height='1.5em',color='00ff60')
 //{
 //    let btnDupliq =document.createElement('button');
  //   btnDupliq.setAttributes('title','dupliquer');
  //   btnDupliq.type="button";
  //   if (callback!=null)
  //   {
   //      btnDupliq.addEventListener("click",callback);
  //   }
  //   let iconDupliq = getIcon('bi bi-files','currentColor',
  //       '1.5em','1.5em','M4 2h7a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V4a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1H4z M6 0h7a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2v-1a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H6a1 1 0 0 0-1 1H4a2 2 0 0 1 2-2z');
  //   btnDupliq.appendChild(iconDupliq);
  //   return btnDupliq;
 //}


/**Recupere value du champ input pour la transformer en text dans element parent
 *
 * @param inputParent
 */
export function inputTextToText(inputParent)
{
    let value =  capitalize(inputParent.firstChild.value);

    inputParent.innerHTML = value;
}


/**Recupere value du champ input pour la transformer en text dans
 * element parent, si value est un nombre l'ecrit avec €
 *
 * @param inputParent
 */
export function inputNumberToTextMoney(inputParent)
{

    let value = inputParent.firstChild.value;
    if (isNaN(value))
    {
        inputParent.innerHTML = value;
    }else{
        inputParent.innerHTML = formatEuro(value);
    }

}
/**controle si null ou vide
 *
 * @param string
 * @returns {boolean}
 */
export function isEmptyString(text)
{
    if(text === '' || text === null)
    {
        return true;
    }else {
        return false;
    }
}


/**remplace le texte dans la cellule par un input
 *
 * @param ele
 */
export function replaceTextByInputText(td,id=null)
{
    //remplacer le text par input avec le text en value
    let input = document.createElement('input');
    let value = td.innerHTML;
    td.innerHTML="";
    input.setAttribute('type','text');
    input.setAttribute('value',value);
    input.id=id;

    td.appendChild(input);
    return input;
}


export function replaceTextByInputNb(ele,id=null)
{
    //remplacer le text par input avec le text en placeholder
    let input = document.createElement('input');
    input.setAttribute('type','number');
    input.setAttribute('min',1);
    input.id=id;
    let info = ele.innerHTML.split('&nbsp;')[0];

    ele.innerHTML="";
    input.setAttribute('value',info);
    ele.appendChild(input);
    return input;
}





export function getIcon(classe,color='currentColor',width='1.5em',height='1.5em',...value)
{


    let svg =document.createElementNS("http://www.w3.org/2000/svg","svg");
    svg.setAttribute('class',classe);
    svg.setAttribute('width',width);
    svg.setAttribute('height',height);
    svg.setAttribute('viewBox','0 0 16 16');
    svg.setAttribute('fill',color);
    for (let valueElement of value) {
        let path1 = document.createElementNS("http://www.w3.org/2000/svg","path");
        path1.setAttribute('fill-rule','evenodd');
        path1.setAttribute('d',valueElement);

        svg.appendChild(path1);
    }

    return svg;

}

export  function getBtnDetail(callback=null,width='1.5em',height='1.5em',color='currentColor')
{
    let btnAfficher = document.createElement('button');
    btnAfficher.setAttribute('title','Détail');
    btnAfficher.setAttribute('class','bouton btn btn-afficher');
    if (callback!=null)
    {
        btnAfficher.addEventListener("click",callback);
    }
    let iconCheck = getIcon('bi bi-file-arrow-down',color,
        width,height,'M4 1h8a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H4z',
        'M4.646 8.146a.5.5 0 0 1 .708 0L8 10.793l2.646-2.647a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 0 1 0-.708z',
        'M8 4a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0v-6A.5.5 0 0 1 8 4z');
    btnAfficher.appendChild(iconCheck);
    return btnAfficher;
}

export  function getBtnMasquer(callback=null,width='1.5em',height='1.5em',color='currentColor')
{
    let btnMasquer = document.createElement('button');
    btnMasquer.setAttribute('title','Masquer');
    btnMasquer.setAttribute('class','bouton btn btn-masquer');
    if (callback!=null)
    {
        btnMasquer.addEventListener("click",callback);
    }
    let iconCheck = getIcon('bi bi-file-arrow-up',color,
        width,height,'M4 1h8a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2zm0 1a1 1 0 0 0-1 1v10a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H4z',
        'M4.646 7.854a.5.5 0 0 0 .708 0L8 5.207l2.646 2.647a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 0 0 0 .708z',
        'M8 12a.5.5 0 0 0 .5-.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 0 .5.5z');
    btnMasquer.appendChild(iconCheck);
    return btnMasquer;
}

/**Supprimer la ligne lier au bouton
 *
 * @param event
 */
export function cancel(event)
{
    let tr = event.currentTarget.parentElement.parentElement.parentElement;
    tr.remove();
}

/**Retourne l'id en base d'un element
 * principe de nommage id des element bla_bla_id
 * @param idElem
 * @returns {*}
 */
export function getId(idElem)
{
    let tabId = idElem.split('_');
    return tabId[tabId.length-1];
}


/**Inverse les boutons valider/annuler par modifier/supprimer et inversement
 * avec les icons correspondant
 *possible passage d'id et de function callback sur onclick
 *btn1 => valider/modifier // btn2=>annuler/supprimer
 * @param span
 * @param id
 * @param complementId
 * @param callbackBtn1
 * @param callbackBtn2
 */
export function inverseBtns(span,id=null,complementId=null,callbackBtn1=null,callbackBtn2=null)
{
    let btn1 = span.children[0];
    let btn2 = span.children[1];
    let btn1Class = btn1.classList;
    if (btn1Class.contains('btn-mdf'))
    {
        //actuellement btn1=>modifier  btn2=>supprimer
        //ici changement des boutons
        // btn1=>valider et btn2=>annulation
        let btnCheck = getBtnCheck();

        let btnCancel = getBtnCancel();
        if (id!=null && complementId!= null)
        {
            btnCheck.id= "valid_"+complementId+"_"+id;
            btnCancel.id= "cancel_"+complementId+"_"+id;
        }
        if (callbackBtn1!=null)
        {
            btnCheck.addEventListener("click",callbackBtn1);
        }
        if (callbackBtn2!=null)
        {
            btnCancel.addEventListener("click",callbackBtn2);
        }
        span.replaceChild(btnCheck,btn1);
        span.replaceChild(btnCancel,btn2);
    }else{
        //actuellement btn1=>valider  btn2=>annulation
        //ici changement des boutons
        // btn1=>modifier et btn2=>supprimer
        let btnMdf = getBtnMdf();
        let btnSup = getBtnSup();
        
        if (id!=null && complementId!= null)
        {
            btnMdf.id= "mdf_"+complementId+"_"+id;
            btnSup.id= "sup_"+complementId+"_"+id;
            
        }
        if (callbackBtn1!=null)
        {
            btnMdf.addEventListener("click",callbackBtn1);
        }
        if (callbackBtn2!=null)
        {
            btnSup.addEventListener("click",callbackBtn2);
        }
        span.replaceChild(btnMdf,btn1);
        span.replaceChild(btnSup,btn2);
    }
}

/**Retourne le nombre formater avec 2 chiffres a la virgule max
 *
 * @returns {string}
 * @param x
 */
export function formatFloat(x)
{
    if (typeof x === "string")
    {
        x =parseFloat(x);
        if (isNaN(x) || x===undefined)
        {
            x=0;
        }
    }
    return x.toFixed(2).replace(/\.?0*$/,'');
}

/**Retourne le nombre en pourcentage
 *
 * @param nb
 * @returns {string}
 */
export function formatPercent(nb)
{
    if (isNaN(nb))
    {
        nb=0;
    }
    let formater=new Intl.NumberFormat("fr-FR", {style:"percent",maximumFractionDigits: 2,minimumFractionDigits:2});
    return formater.format(nb);
}

export function formatEuro(nb)
{
    let float = formatFloat(nb);
    return float+' €';
}


export function numberFromEuroText(string)
{
    return string.replace('€','').trim();
}
/**Retourne les infos de base pour traitement dans un objet
 * dans une construction <tr><td></td>...<td><span><button event></button></span></td> ...
 * {span,id,tr}
 * @param event
 * @returns {{id: *, tr: HTMLElement, span: HTMLElement}}
 */
export function getinfosBase(event)
{
    let span =event.currentTarget.parentElement;
    let id = getId(span.id);
    let tr = span.parentElement.parentElement;
    let infos = ({
        "span":span,
        "id":id,
        "tr":tr,

    });

    return infos;
}


/**Retourne un input construit avec les attributs passer en param sour forme de clef->valeur
 * ex: "type":"number"
 *
 * @param attributsList
 * @returns {HTMLInputElement}
 */
export function getInput(attributsList=null)
{
    let input = document.createElement('input');

    if (attributsList!== null)
    {
        for (let [key, value] of Object.entries(attributsList)) {

            input.setAttribute(key,value);
        }
    }
    input.classList.add('form-control');
    input.classList.add('form-control-sm');
    return input;
}

/**Retourne un select de nombre (pratique pour mois et annees)
 *
 * @param value
 * @param min
 * @param max
 * @returns {HTMLSelectElement}
 */
export function getSelectNumber(value = 0,min=1,max=13,id=null)
{
    let select = document.createElement('select');
    for (let i =min;i<max;i++)
    {
        let option = document.createElement('option');
        option.value=i;
        option.text= i.toString().padStart(2,'0');
        select.add(option);
    }
    select.className='form-control w-auto  custom-select';
    select.style.display='inline'
    select.value=value;
    select.id=id;
    return select;
}

/**Retourne la ligne du projet et le span conteneur des boutons
 *
 * @param idPj
 * @param type
 * @returns {{spanBtnPj: HTMLElement, trPj: HTMLElement}}
 */
export function getProjetElement(idPj,type)
{
    let trPj = document.getElementById('projet_'+type+'_'+idPj);
    let spanBtns = document.getElementById('btns_projet_'+type+'_'+idPj);
    return({
        "trPj":trPj,
        "spanBtnPj":spanBtns,
    });
}



export function calculRoi(cout,pv)
{
    if (cout>0 && pv>0)
    {
        return cout/pv;
    }else {
        return 0;
    }

}

export function setClassRoi(roi,tdRoi)
{
    if (parseFloat(roi)<=0.37)
    {
        tdRoi.classList.add('text-success');
        tdRoi.classList.remove('text-danger')
    }else
    {
        tdRoi.classList.remove('text-success');
        tdRoi.classList.add('text-danger');
    };
    tdRoi.innerText= formatPercent(parseFloat(roi));
}

/**ajout is-valid ou is-invalid selon le boolean
 * false => is-invalid true=> is-valid
 * @param classList
 * @param valid :boolean
 */
export function setValid(classList,valid)
{
    switch (valid) {
        case true:
            if (classList.contains('is-invalid'))
            {
                classList.remove('is-invalid');

            }
            classList.add('is-valid');
            break;
        case false:
            if (!classList.contains('is-invalid'))
            {
                classList.add('is-invalid');
            }
            break;
    }
}
/**Retourne un select avec les donnees passé en parametre
 *
 * @param dataList
 * @param id
 * @param value
 * @returns {HTMLSelectElement}
 */
export function getSelectCategorie(categories,id=null,value=1)
{
    let select = document.createElement('select');
    categories.forEach((categorie)=>{
        let option = document.createElement('option');
        option.value=categorie.id;

        option.text= capitalize(categorie.libelle);
        select.add(option);
    })
    select.className='form-control form-control-sm custom-select w-auto';
    select.selectedIndex=value-1;
    select.id=id;
    return select;
}


export function getSelectPeriodicite(id=null,value=null)
{
    let select = document.createElement('select');
    let perios = ['MENSUEL','ONESHOT'];
    for (let perio of perios)
    {
        let option = document.createElement('option');
        option.value=perio;
        option.innerText=capitalize(perio);
        select.add(option);
    }

    select.className='form-control form-control-sm w-auto custom-select';
    select.value=value;
    select.id='select_perio_'+id;
    return select;
}


export function capitalize(str1){
    str1=str1.toLowerCase();
    return str1.charAt(0).toUpperCase() + str1.slice(1);
}


/**Retourne un select avec la liste des independants
 *
 * @param indeList
 * @returns {HTMLSelectElement}
 */
export function getSelectInde(indeList)
{
    let select = document.createElement('select');
    select.name="independant";
    select.id="inde_select";
    select.setAttribute('class','form-control custom-select');
    //ajout de chaque independant au select
    indeList.forEach(function (inde) {
        let option = document.createElement('option');
        option.innerText = capitalize(inde.prenom) +" "+capitalize(inde.nom);
        option.value=inde.id;
        select.add(option);
    })

    return select;
}

export function getTotauxProjets()
{
    let tdPvM = document.querySelectorAll('.infos_MENSUEL');
    let mensuel;
    let oneShot;
    let tdPvOs = document.querySelectorAll('.infos_ONESHOT');
    if (tdPvM != null && tdPvM.length>0){
        mensuel = getDataPv(tdPvM);
    }
    if (tdPvOs != null && tdPvOs.length>0)
    {
        oneShot= getDataPv(tdPvOs);
    }

    return {
        "mensuel":mensuel,
        "oneshot":oneShot,
    };
}

function getDataPv(tdListe)
{
    let totalCost = 0;
    let totalPv = 0;
    let totalCostNonPrevi =0;
    let totalPvNonPrevi =0;
    let totalCostPrevi = 0;
    let totalPvPrevi =0;
    tdListe.forEach(function (td){
        totalCost = totalCost+parseFloat(td.dataset.cost);
        totalPv=totalPv+parseFloat(td.dataset.prixVente);
        totalCostNonPrevi += +parseFloat(td.dataset.costNonPrevi);
        totalPvNonPrevi+=parseFloat(td.dataset.pvNonPrevi);
        totalCostPrevi += +parseFloat(td.dataset.costPrevi);
        totalPvPrevi+=parseFloat(td.dataset.pvPrevi);
    });
    return ({
        "pv": totalPv,
        "cout":totalCost,
        "roi":calculRoi(totalCost,totalPv),
        "pvNonPrevi": totalPvNonPrevi,
        "coutNonPrevi":totalCostNonPrevi,
        "roiNonPrevi":calculRoi(totalCostNonPrevi,totalPvNonPrevi),
        "pvPrevi": totalPvPrevi,
        "coutPrevi":totalCostPrevi,
        "roiPrevi":calculRoi(totalCostPrevi,totalPvPrevi),
    });
}


export function getSpanIconFiltreUp(id,display='none')
{
    let icon =getIcon('bi bi-caret-up', 'currentColor',
        '1em', '1em', 'M3.204 11L8 5.519 12.796 11H3.204zm-.753-.659l4.796-5.48a1 1 0 0 1 1.506 0l4.796 5.48c.566.647.106 1.659-.753 1.659H3.204a1 1 0 0 1-.753-1.659z');
    let span = document.createElement("span");
    span.id=id+'_up';
    span.appendChild(icon);
    span.style.display=display;
    return span;

}

export function getSpanIconFiltreDown(id,display='none')
{
    let icon =getIcon('bi bi-caret-down', 'currentColor',
        '1em', '1em', 'M3.204 5L8 10.481 12.796 5H3.204zm-.753.659l4.796 5.48a1 1 0 0 0 1.506 0l4.796-5.48c.566-.647.106-1.659-.753-1.659H3.204a1 1 0 0 0-.753 1.659z');
    let span = document.createElement("span");
    span.id=id+'_down';
    span.appendChild(icon);
    span.style.display=display;
    return span;
}



export function afficherMessage(message,id,style)
{
    let trmessage = document.getElementById('message_for_'+id);
    trmessage.firstElementChild.innerText = message;
    trmessage.classList.add(style);

    trmessage.style.display='';
    setTimeout(()=>{
        trmessage.style.display='none';
    },3000);
}




export function getTrMessage(idMessage,colspan=4)
{
    let trMessage = document.createElement('tr');
    let tdMessage = trMessage.insertCell();
    tdMessage.colSpan=colspan;
    trMessage.id='message_for_'+idMessage;
    trMessage.style.display='none';
    return trMessage;
}



export function isMobile()
{
    let mobile = document.getElementById('div-general').dataset.mobile;
    if (mobile==='true')
    {
        return true;
    }else{
        return false;
    }
}



export function getIconNumeric(type)
{
    if (type==='up')
    {
        return getIcon('bi bi-sort','currentColor','1.5em','1.5em',
            'M4 14a.5.5 0 0 0 .5-.5v-11a.5.5 0 0 0-1 0v11a.5.5 0 0 0 .5.5z',
            'M6.354 4.854a.5.5 0 0 0 0-.708l-2-2a.5.5 0 0 0-.708 0l-2 2a.5.5 0 1 0 .708.708L4 3.207l1.646 1.647a.5.5 0 0 0 .708 0z',
            'M12.438 7V1.668H11.39l-1.262.906v.969l1.21-.86h.052V7h1.046zm-2.84 5.82c.054.621.625 1.278 1.761 1.278 1.422 0 2.145-.98 2.145-2.848 0-2.05-.973-2.688-2.063-2.688-1.125 0-1.972.688-1.972 1.836 0 1.145.808 1.758 1.719 1.758.69 0 1.113-.351 1.261-.742h.059c.031 1.027-.309 1.856-1.133 1.856-.43 0-.715-.227-.773-.45H9.598zm2.757-2.43c0 .637-.43.973-.933.973-.516 0-.934-.34-.934-.98 0-.625.407-1 .926-1 .543 0 .941.375.941 1.008z'
            );
    }else{
        return getIcon('bi bi-sort','currentColor','1.5em','1.5em',
            'M4 2a.5.5 0 0 1 .5.5v11a.5.5 0 0 1-1 0v-11A.5.5 0 0 1 4 2z',
            'M6.354 11.146a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L4 12.793l1.646-1.647a.5.5 0 0 1 .708 0z',
            'M9.598 5.82c.054.621.625 1.278 1.761 1.278 1.422 0 2.145-.98 2.145-2.848 0-2.05-.973-2.688-2.063-2.688-1.125 0-1.972.688-1.972 1.836 0 1.145.808 1.758 1.719 1.758.69 0 1.113-.351 1.261-.742h.059c.031 1.027-.309 1.856-1.133 1.856-.43 0-.715-.227-.773-.45H9.598zm2.757-2.43c0 .637-.43.973-.933.973-.516 0-.934-.34-.934-.98 0-.625.407-1 .926-1 .543 0 .941.375.941 1.008zM12.438 14V8.668H11.39l-1.262.906v.969l1.21-.86h.052V14h1.046z'
        );
    }
}

export function getIconAlpha(type)
{
    if (type==='up')
    {
        return getIcon('bi bi-sort','currentColor','1.5em','1.5em',
            'M4 14a.5.5 0 0 0 .5-.5v-11a.5.5 0 0 0-1 0v11a.5.5 0 0 0 .5.5z',
            'M6.354 4.854a.5.5 0 0 0 0-.708l-2-2a.5.5 0 0 0-.708 0l-2 2a.5.5 0 1 0 .708.708L4 3.207l1.646 1.647a.5.5 0 0 0 .708 0z',
            'M9.664 7l.418-1.371h1.781L12.281 7h1.121l-1.78-5.332h-1.235L8.597 7h1.067zM11 2.687l.652 2.157h-1.351l.652-2.157H11zM9.027 14h3.934v-.867h-2.645v-.055l2.567-3.719v-.691H9.098v.867h2.507v.055l-2.578 3.719V14z'
        );
    }else{
        return getIcon('bi bi-sort','currentColor','1.5em','1.5em',
            'M4 2a.5.5 0 0 1 .5.5v11a.5.5 0 0 1-1 0v-11A.5.5 0 0 1 4 2z',
            'M6.354 11.146a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L4 12.793l1.646-1.647a.5.5 0 0 1 .708 0z',
            'M9.027 7h3.934v-.867h-2.645v-.055l2.567-3.719v-.691H9.098v.867h2.507v.055L9.027 6.309V7zm.637 7l.418-1.371h1.781L12.281 14h1.121l-1.78-5.332h-1.235L8.597 14h1.067zM11 9.687l.652 2.157h-1.351l.652-2.156H11z'
       );
    }
}



export function getBtnAlpha(callback,type='up')
{
    let value = (type==='up')? 0 :1;
    let btn = document.createElement("button");
    btn.className='bouton btn btn-trie-spj';
    btn.title='Trier par désignation';
    btn.setAttribute('data-sort',value);
    btn.setAttribute('data-type','text');
    btn.addEventListener("click", callback);
    let iconAlpha = getIconAlpha(type);
    btn.appendChild(iconAlpha);
    return btn;

}
export function getBtnNumeric(callback,type='down')
{
    let value = (type==='up')? 0 :1;
    let btn = document.createElement("button");
    btn.className='bouton btn btn-trie-spj';
    btn.title='Trier par date';
    btn.setAttribute('data-sort',value);
    btn.setAttribute('data-type','date');
    btn.addEventListener("click", callback);
    let iconAlpha = getIconNumeric(type);
    btn.appendChild(iconAlpha);
    return btn;

}













/**Traite la liste d'errreurs envoyer par la serveur et construit le message à afficher à l'utilisateur
 *
 * @param erreurs
 * @param inputs
 * @returns {string}
 */
export function traitementErreursInde(erreurs,inputs)
{
    let message ='';
    if (erreurs.nom)
    {
        setValid(inputs.nom.classList,false);
        message += erreurs.nom;
    }
    if (erreurs.salaire)
    {
        setValid(inputs.salaire.classList,false);
        message += '\n'+ erreurs.salaire;
    }
    if (erreurs.prenom)
    {
        setValid(inputs.prenom.classList,false);
        message += '\n'+ erreurs.prenom;
    }
    return message;
}

export function setClassPrevisionnel(classList,isPrevisionnel)
{
    if (isPrevisionnel)
    {
        if (!classList.contains('previsionnel'))
        {
            classList.add('previsionnel');
        }
    }else{
        if (classList.contains('previsionnel'))
        {
            classList.remove('previsionnel');
        }
    }
}