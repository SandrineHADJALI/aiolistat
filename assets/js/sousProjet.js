import * as Utils from "./utils/utils";
import * as Categorie from "./ajax/categorie_ajax";
import * as Spj from "./ajax/sous_projet_ajax";
import * as Prestations from "./prestations";
import * as Heure from "./heure";
import * as Projet from './projet';
import * as Valid from './validation';
import * as List from './projetList';
import * as Refresh from './refresh';
import * as Inde from './ajax/independant_ajax';
////////////////////////////// AJOUTER SOUS PROJET ///////////////////////////////
console.log('ajout spj')

export function formAddSpj(event)
{
    event.preventDefault();
    event.stopPropagation();
    //recuperation de id du projet
    let btnAdd = event.currentTarget;

    let idProjet = Utils.getId(btnAdd.id);
    let isCreate = false;
    if (idProjet==='add')
    { isCreate=true;}
    let periodicite = Projet.getPeriodicite(btnAdd.id);
    //ligne du projet
    let tdsProjet = Projet.getTdsAndInput(periodicite,idProjet);
    let trProjet = tdsProjet.trProjet;
        //recuperation du formulaire de creation du sous-projet
    getFormAddSpj(btnAdd,isCreate).then((trForm)=>{
        trProjet.insertAdjacentElement("afterend",trForm);
        Refresh.refreshRoiPj(idProjet,periodicite);
    })

}
export function getFormAddSpj(btnAdd,isCreate=false)
{
    return new Promise((resolve, reject) => {
        //gestion du nombre de formulaires
        let nbForm = btnAdd.dataset.nbForm;
        let typeForm = btnAdd.dataset.typeForm;
        nbForm++;
        let idProjet = Utils.getId(btnAdd.id);
        let idSpj ='form'+nbForm;
        if (isCreate)
        {
            idSpj ='pjform'+nbForm;
        }

        let periodicite = Projet.getPeriodicite(btnAdd.id);
        getFormForSpj(typeForm,idSpj,create,cancelCreate,null,periodicite).then((trForms)=>
        {
            //creation des elements qui vont contenir le formulaire
            //ligne
            let tr = document.createElement("tr");
            tr.setAttribute('data-id-projet',idProjet);
            tr.setAttribute('class','form_spj_'+periodicite+'_'+idProjet+' spj_'+periodicite+'_'+idProjet);
            tr.setAttribute('data-previsionnel',false);
            tr.setAttribute('data-id-soous-projet',idSpj);
            tr.setAttribute('data-periodicite',periodicite);
            tr.id=idSpj;
            //cellule
            let td = document.createElement("td");
            td.colSpan=5;
            //tableau
            let table = document.createElement("table");
            table.setAttribute('class',"sousProjet table table-sm table-borderless");
            tr.appendChild(td);
            td.appendChild(table);
            //en tete sous-projet
            table.appendChild(List.getHeadSpj(idSpj));
            //creation du body
            let tBodySpj = document.createElement("tbody");
            tBodySpj.id = 'tbody_spjs_'+idSpj;
            //lignes des prestations
            let trPresta =Prestations.getTrPrestaHead(idSpj);
            //lignes heures
            let trHeure=Heure.getTrHeadHeure(idSpj);
            tBodySpj.insertAdjacentElement("afterbegin", trForms.trForm1);
            trForms.trForm1.insertAdjacentElement("afterend", trForms.trForm2);
            trForms.trForm1.insertAdjacentElement("beforebegin", trForms.trMessage);
            trForms.trForm2.insertAdjacentElement("afterend", trForms.trForm3);
            trForms.trForm3.insertAdjacentElement("afterend", trPresta);
            trPresta.insertAdjacentElement("afterend", trHeure);
            table.appendChild(tBodySpj);
            btnAdd.setAttribute('data-nb-form',nbForm);
            resolve(tr);
        })
    })
}
/**Supprime les lignes ajouter pour creation d'un projet
 * Rafraichis le roi et le prix de vente du projet si modifications
 *
 * @param event
 */
export function cancelCreate(event) {
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let tds = getTdsAndInput(base.id);
    //recuperer la ligne et la supprimer
    tds.trSpj.remove()
    //voir maj les autres formulaires si presents
    let trClass = tds.trSpj.classList[0];
    let idProjet = Utils.getId(trClass);
    let periodicite = Projet.getPeriodicite(trClass);
    let btnAdd = document.getElementById('add_spj_'+periodicite+'_'+idProjet);
    btnAdd.dataset.nbForm = parseInt(btnAdd.dataset.nbForm)-1;
    btnAdd.style.display="";
    let trMessage = document.getElementById("message_for_spj_"+base.id);
    Refresh.refreshRoiPj(idProjet,periodicite);
}

/**Fonction appellee lors du clic sur valider ajout d'un nouveau sous-projet
 * sur un projet deja enregistre en BDD
 * @param event
 */
export function create(event) {
    event.preventDefault();
   //event valide ajout
    console.log('valide ajout spj')
    let base = Utils.getinfosBase(event);
    let tds = getTdsAndInput(base.id);
    let trSpj = tds.trSpj;
    let trClass = tds.trSpj.classList[0];
    let idProjet = Utils.getId(trClass);
    let periodiciteProjet = Projet.getPeriodicite(trClass);
    let btnAdd = document.getElementById('add_spj_'+periodiciteProjet+'_'+idProjet);
    let nbForm = parseInt(btnAdd.dataset.nbForm);
    if (Valid.sousProjetForm(base.id)) {
        //base id = formX
        --nbForm;
        btnAdd.dataset.nbForm=nbForm;
        insertSpj(base.id).then((spj) => {
            let perioSpj = spj.spjInfo.infos.periodicite;
            Utils.afficherMessage('Ajout effectué avec succès','spj_'+base.id,'success');
                if (periodiciteProjet !== perioSpj) {
                    //les periodicitées sont différentes
                    let trPjAutre = document.getElementById('projet_' + perioSpj + '_' + idProjet);
                    if (trPjAutre !== null) {
                        //une ligne est deja presente pour le projet
                        // on reactualise les spj
                        List.afficherSpj(idProjet, perioSpj).then(()=>{
                            Refresh.refreshRoiPj(idProjet,perioSpj);
                            //on supprime le formulaire
                            trSpj.remove();
                            Refresh.refreshRoiPj(idProjet,periodiciteProjet);
                        });
                    } else {
                        //la ligne du projet n'est pas présente: on l'ajoute
                        Projet.addProjetToTable(idProjet, periodiciteProjet, perioSpj);
                        trSpj.remove();
                    }
                }else{
                    //periodicitees identiques
                    //reactualise uniquement la ligne du sous-projet
                    let trProjet = document.getElementById('projet_'+periodiciteProjet+'_'+idProjet);
                    trSpj.remove();
                    let tabPromise=[];
                    spj.idSpj.forEach(function (idSpj) {
                        tabPromise.push(Spj.loadSousProjet(idSpj));
                    });
                        Promise.all(tabPromise).then((sousProjets)=>{

                            sousProjets.forEach(function (sousProjet) {
                                let newTrSPj = List.getTrSousProjet(sousProjet,idProjet);
                                trProjet.insertAdjacentElement("afterend",newTrSPj);
                                Refresh.refreshRoiPj(idProjet,periodiciteProjet);
                                //modification du nombre de sous-projets
                                trProjet.dataset.nbSousProjets = parseInt(trProjet.dataset.nbSousProjets)+1;
                            });
                            //on affiche les boutons si le nombre de sous-projets est superieur à 1

                            if (parseInt(trProjet.dataset.nbSousProjets)>1)
                            {
                                let btnsAfficher= document.querySelectorAll('.boutons_projet_'+periodiciteProjet+'_'+idProjet);
                                btnsAfficher.forEach(function (btn) {
                                    btn.style.visibility='visible';
                                })
                            }
                        });
                }
                if (periodiciteProjet==='VIDE' && nbForm===0)
                {
                    Projet.removeTrProjet(idProjet,periodiciteProjet);
                }
        },
            (message)=>{
                Utils.afficherMessage(message,'spj_'+base.id,'error');
            });
    }
}






export function insertSpj(idForm,add=false)
{
    return new Promise((resolve, reject) => {
        let tds = getTdsAndInput(idForm);
        let infos = getFormValues(tds);
        let autresInfos = getPrestaAndHeuresOnCreate(idForm);
        let spjInfo = {
            "infos":infos,
            "autres":autresInfos,
        };
        console.log(spjInfo);
        Spj.insert(spjInfo).then((idSpj)=>{
            let tabPromise = [];
            for (let i=1;i<infos.recurrence;i++)
            {
                gestionRecurrence(infos);
                tabPromise.push(Spj.insert(spjInfo));
            }
            Promise.all(tabPromise).then((ids)=>{
                ids.unshift(idSpj);
                let spj={
                    "spjInfo":spjInfo,
                    "idSpj":ids,
                    "tds":tds,
                };

                resolve(spj);
            })

        });
    })
}

/**Incremente date de 1 mois a chaque appel
 *
 * @param infos
 */
function gestionRecurrence(infos)
{
    infos.month =  parseInt(infos.month)+1;
    if (parseInt(infos.month)>12)
    {
        infos.year = parseInt(infos.year)+1;
        infos.month= parseInt(infos.month)-12;
    }
}


/////////////////////MODIFIER SOUS PROJET /////////////////////////////////
export function formUpdtSpj(event)
{
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let tds = getTdsAndInput(base.id);
    Spj.loadSousProjet(base.id).then((sousProjet)=>
    {
        let date = new Date(sousProjet.dateAction.date);
        let attrList=({
            "designation":sousProjet.designation,
            "pv":sousProjet.prixVente,
            "categorie":sousProjet.categorie.id,
            "month": date.getMonth()+1,
            "year" : date.getFullYear(),
            "roi": sousProjet.cout/sousProjet.prixVente,
            "periodicite":sousProjet.periodicite,
            "cost":tds.tdPv.dataset.cost,
            "isPrevisionnel":sousProjet.isPrevisionnel,
        })

        getFormForSpj('update',base.id,updateSpj,cancelUpdateSpj,attrList).then((trForms)=>{
            tds.tBody.replaceChild(trForms.trForm1,base.tr);
            trForms.trForm1.insertAdjacentElement("afterend", trForms.trForm2);
            trForms.trForm1.insertAdjacentElement("beforebegin", trForms.trMessage);
            trForms.trForm2.insertAdjacentElement("afterend", trForms.trForm3);

        })
    })
}

function updateSpj(event) {
    event.preventDefault();
    //event bouton valider modif
    let base = Utils.getinfosBase(event);
    let tds = getTdsAndInput(base.id);
    let periodiciteProjet = tds.projetPerio;
    let idProjet = tds.projetId;
    let idSpj = base.id;
    if (Valid.sousProjetForm(idSpj))
    {
        let spjInfo = getFormValues(tds);
        let categorie = tds.selectCate.options[tds.selectCate.selectedIndex].innerText;
        Spj.update(spjInfo,idSpj).then((diffCout)=>{
                Utils.afficherMessage('Mofidications effectuées avec succès','spj_'+idSpj,'success');
            if (periodiciteProjet === spjInfo.periodicite)
            {
                //pas de changement de periodicitee
                //on affiche les nouvelles informations
                tds.tdDesign.innerText=spjInfo.designation;
                tds.categorie.innerText = categorie;
                tds.tdPv.innerText=Utils.formatEuro(spjInfo.pv);
                let month = tds.selectMonth.value;
                if (month<10)
                {
                    month = '0'+month;
                }
                tds.tdDate.innerText=month+'/'+tds.selectYear.value;
                //Suppression des lignes pour date
                tds.tBody.removeChild(tds.trHeadDate);
                tds.tBody.removeChild(tds.trBodyDate);
                Utils.inverseBtns(base.span,base.id,'spj',formUpdtSpj,validerSupSpj);
            }else{
                //on supprime le formulaire
                let perioSpj = spjInfo.periodicite;
                removeTrSousProjet(idSpj);
                //les periodicitées sont différentes
                let trPjAutre = document.getElementById('projet_' + perioSpj + '_' + idProjet);
                if (trPjAutre !== null) {
                    //une ligne est deja presente pour le projet
                    // on reactualise les spj
                    List.afficherSpj(idProjet, perioSpj).then(()=>{
                        Refresh.refreshRoiPj(idProjet,perioSpj);
                        Refresh.refreshRoiPj(idProjet,periodiciteProjet);
                    });
                } else {
                    //la ligne du projet n'est pas présente: on l'ajoute
                    Projet.addProjetToTable(idProjet, periodiciteProjet, perioSpj);
                    Refresh.refreshRoiPj(idProjet,perioSpj);
                }

            }
        },
            (message)=>{
                Utils.afficherMessage(message,'spj_'+idSpj,'error');
            });
    }
}

function cancelUpdateSpj(event) {
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let tds = getTdsAndInput(base.id);

    //remettres les infos du sousProjet dans les champs avant de passer en text
    Spj.loadSousProjet(base.id).then((sousProjet)=>{
        let date = new Date(sousProjet.dateAction.date);
        let attrList=({
            "designation":sousProjet.designation,
            "pv":sousProjet.prixVente,
            "categorie":sousProjet.categorie.id,
            "month": date.getMonth(),
            "year" : date.getFullYear(),
            "roi": Utils.formatPercent(sousProjet.cout/sousProjet.prixVente),
        })
        tds.tdDesign.innerText=sousProjet.designation;
        tds.categorie.innerText = sousProjet.categorie.libelle;
        tds.tdPv.innerText=Utils.formatEuro(sousProjet.prixVente);
        tds.tdPv.dataset.categorie = sousProjet.categorie.id;
        tds.tdPv.dataset.prixVente=sousProjet.prixVente;
        tds.tdPv.dataset.cost= sousProjet.cout;
        tds.tdPv.dataset.previsionnel = sousProjet.isPrevisionnel;
        //changement class Previsionnel
        Utils.setClassPrevisionnel(tds.tableSpj.classList,sousProjet.isPrevisionnel);
        tds.trSpj.dataset.previsionnel = sousProjet.isPrevisionnel;
        tds.tdDate.innerText=(attrList.month+1)+'/'+attrList.year;
        //     //Suppression des lignes pour date
        tds.tBody.removeChild(tds.trHeadDate);
        tds.tBody.removeChild(tds.trBodyDate);
        Utils.inverseBtns(base.span,base.id,'spj',formUpdtSpj,validerSupSpj);
        let trMessage = document.getElementById("message_for_spj_"+base.id);
        trMessage.remove();
        Refresh.refreshRoiPjAndSpj(base.id);
    })



}

///////////////////////////SUPPRESSION ////////////////////////////////////////


/**Redemande validation pour suppression du sous-projet
 *
 * @param event
 */
export function validerSupSpj(event) {
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    Utils.inverseBtns(base.span,base.id,'spj',supSpj,cancelSupSpj);
}

/**Annule la suppression du sous-projet
 *
 * @param event
 */
function cancelSupSpj(event) {
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    Utils.inverseBtns(base.span,base.id,'spj',formUpdtSpj,validerSupSpj);

}

/**Supprime le sous-projet et ses complements (prestations,heures)
 *
 * @param event
 */
function supSpj(event) {
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let tds = getTdsAndInput(base.id);
    let idProjet = Utils.getId(tds.tdSpjs.id);
    let periodicite = Projet.getPeriodicite(tds.tdSpjs.id);

    Spj.deleteSpj(base.id).then(()=>{
         removeTrSousProjet(base.id);
    }
    )

}
function removeTrSousProjet(idSpj) {
    //recuperation de sinfos de base
    let tds = getTdsAndInput(idSpj);
    let periodicite = tds.projetPerio;
    let idProjet = tds.projetId;
    //Modification du prix de vente du projet
    let tdPvProjet = document.getElementById('pv_projet_'+periodicite+'_'+idProjet);
    let pv = Utils.numberFromEuroText(tdPvProjet.innerText);
    let newPv = pv-Utils.numberFromEuroText(tds.tdPv.innerText);
    tdPvProjet.innerText=Utils.formatEuro(newPv);
    //modification du nombre de sous-projets
    let trProjet = document.getElementById('projet_'+periodicite+'_'+idProjet);

    let nbSpj = parseInt(trProjet.dataset.nbSousProjets)-1;
    trProjet.dataset.nbSousProjets = nbSpj;
    let tdH = tds.tdsProjet.tdHeure;
    if (tdH!==null)
    {
        let nbSousProjet = parseInt(tdH.dataset.nbSousProjetsInde);
        nbSousProjet--;
        tdH.dataset.nbSousProjetsInde=nbSousProjet;
    }
    //suppression des lignes
    tds.trSpj.remove();

    if (nbSpj===0)
    {
        //le projet n'a plus de sous-projet pour cette periodicite
        Projet.removeTrProjet(idProjet,periodicite);
    }else {
        // on masque les boutons de tris si il ne reste qu'un seul sous-projet
        if (nbSpj<=1)
        {
            let btnsAfficher= document.querySelectorAll('.boutons_projet_'+periodicite+'_'+idProjet);
            btnsAfficher.forEach(function (btn) {
                btn.style.visibility='hidden';
            })
        }
        Refresh.refreshRoiPj(idProjet,periodicite);
    }
    Refresh.refreshGlobaux();
    Refresh.refreshCategorie();
}


///////////////////////////////// OUTILS //////////////////////////////////////

/**Retournes les cellules et champs de saisies si ils sont présents
 *
 * @param idSpj
 * @param idPj
 * @param add
 * @returns {{tdPv: HTMLElement, inputPv: HTMLElement, tdDesign: HTMLElement, tdRoi: HTMLElement, inputDesign: HTMLElement}}
 */
export function getTdsAndInput(idSpj)
{
    console.log('récup tds et input')

    let tdDesign =document.getElementById('design_spj_'+idSpj);
    let inputDeisgn =document.getElementById('input_design_spj_'+idSpj);
    let tdCategorie =document.getElementById('categorie_spj_'+idSpj);
    let selectCategorie =document.getElementById('select_categorie_spj_'+idSpj);
    let tdRoi =document.getElementById('roi_spj_'+idSpj);
    let tdPv =document.getElementById('pv_spj_'+idSpj);
    let inputPv =document.getElementById('input_pv_spj_'+idSpj);
    let tdDate = document.getElementById('date_spj_'+idSpj);
    let selectMonth=document.getElementById('select_month_spj_'+idSpj);
    let selectYear=document.getElementById('select_year_spj_'+idSpj);
    let tdRecu = document.getElementById('recu_spj_'+idSpj);
    let inputRecu=document.getElementById('input_recu_spj_'+idSpj);
    let tdPerio = document.getElementById("perio_spj_"+idSpj);
    let selectPerio = document.getElementById("select_perio_spj_"+idSpj);
    let trHeadDate =document.getElementById('head_date_spj_'+idSpj);
    let trBodyDate =document.getElementById('body_date_spj_'+idSpj);
    let trListPrestaHead= document.getElementById('list_presta_spj_'+idSpj);
    let trListHeuresHead = document.getElementById('list_heures_spj_'+idSpj);
    let trComplements=document.querySelectorAll('.compl_spj_'+idSpj);
    let tBody = tdDesign.parentElement.parentElement;
    let tdSpjs =tBody.parentElement.parentElement;
    let trSpj =tdSpjs.parentElement;
    let idProjet = trSpj.dataset.idProjet;
    let periodicte = Projet.getPeriodicite(trSpj.classList[0]);
    let tableSpj=tBody.parentElement;
    let trProjet=document.getElementById('projet_'+periodicte+'_'+idProjet);
    let tdPrevi = document.getElementById('previ_spj_'+idSpj);
    let checkPrevi = document.getElementById('checkbox_previ_spj_'+idSpj);
    let tdsProjet = Projet.getTdsAndInput(periodicte,idProjet);
    return({
        "tBody":tBody,
        "tdDesign":tdDesign,
        "inputDesign":inputDeisgn,
        "categorie":tdCategorie,
        "selectCate":selectCategorie,
        "tdRoi":tdRoi,
        "tdPv":tdPv,
        "inputPv":inputPv,
        "selectMonth":selectMonth,
        "selectYear":selectYear,
        "tdRecu":tdRecu,
        "inputRecu":inputRecu,
        "tdPerio":tdPerio,
        "inputPerio":selectPerio,
        "trHeadDate":trHeadDate,
        "trBodyDate":trBodyDate,
        "tdSpjs":tdSpjs,
        "prestaSpj":trListPrestaHead,
        "heuresSpj":trListHeuresHead,
        "trSpj":trSpj,
        "tableSpj":tableSpj,
        "trProjet":trProjet,
        "complements":trComplements,
        "projetId":idProjet,
        "projetPerio":periodicte,
        "tdDate":tdDate,
        "tdsProjet":tdsProjet,
        "tdPrevi":tdPrevi,
        "inputPrevi":checkPrevi,

    });
}

/**Recuperes les infos du formulaire et periodicite du sous-projet
 *
 * @param tds
 * @returns {{recurrence: *, categorieId: *, month: *, periodicite: string, year: *, pv: *, designation: *, projetId: *}}
 */
export function getFormValues(tds)
{
    console.log('récup form et valeurs')

    return ({
        "designation": tds.inputDesign.value,
        "categorieId": tds.selectCate.value,
        "pv": tds.inputPv.value,
        "month": tds.selectMonth.value,
        "year":tds.selectYear.value,
        "recurrence": (tds.inputRecu==null?' ':tds.inputRecu.value),
        "projetId":tds.projetId,
        "periodicite":tds.inputPerio.value,
        "isPrevisionnel": tds.inputPrevi.checked,
    });

}


function getPrestaAndHeuresOnCreate(idForm) {
    //recuperation des lignes autres prestations et heures
    console.log('récup heure presta et cout')
    let trComplement = document.querySelectorAll('.compl_spj_'+idForm);
    let heures = [];
    let prestations = [];
    const hRegex = RegExp('heure');
    const prestRegex = RegExp('presta');
    let coutTotal = 0;
    //recuperation du cout global
    trComplement.forEach((ele)=>{
        if (hRegex.test(ele.id))
        {
            //ligne des heures
            let selectInde = ele.firstElementChild.firstElementChild;
            let idInde = selectInde.value;
            let inputHeure = ele.children[1].firstElementChild;
            let inputCommentaire = ele.children[3].firstElementChild;
            let heureSaisie = inputHeure.value;
            let commentaireSaisie = inputCommentaire.value;
            let heure = {
                "heure":heureSaisie,
                "commentaire":commentaireSaisie,
                "idInde":idInde,
                "idHeure":Utils.getId(selectInde.id),
            };

            heures.push(heure);

        }else if (prestRegex.test(ele.id))
        {
            // lignes des prestations
            let inputDetail = ele.firstElementChild.firstElementChild;
            let detail = inputDetail.value;
            let inputMnt =  ele.children[1].firstElementChild;
            let mnt = inputMnt.value;
            let presta = {
                "detail":detail,
                "montant":mnt
            };
            prestations.push(presta);
        }
    });

    return {
        "heures":heures,
        "prestations":prestations
    };
}



export function getFormForSpj(type,id,callbackCheck,callbackCancel,attrList=null,periodicite=null)
{
    return new Promise((resolve, reject) => {
        let inputRecu = null;

        if (type ==='create' || type ==='add'){
            //si creation ajout d'un champ pour recurrence
            let attrRecu ={
                'type':'number',
                'value':1,
                'id':'input_recu_spj_'+id,
                'min':1,
                'step':1,
                'max':24,
            }
            inputRecu =Utils.getInput(attrRecu);
            inputRecu.addEventListener('input',Refresh.refreshRecurrence);
        }
        //definition des attributs value par defaut
        if (periodicite !== 'ONESHOT' || periodicite!=='MENSUEL')
        {
            periodicite=null;
        }
        if (attrList==null)
        {
            let date = new Date();
            attrList=({
                "designation":"Désignation",
                "pv":0,
                "categorie":1,
                "month": date.getMonth()+1,
                "year" : date.getFullYear(),
                "roi": 0,
                "periodicite":(periodicite === null?'ONESHOT':periodicite),
                "cost":0,
                "isPrevisionnel":false,
            });
        }
        // creation de la ligne et des cellules
        let trForm1 = document.createElement("tr");
        trForm1.id='sous_projet_'+id;
        trForm1.setAttribute('class','top-tr');
        let tdDesign = trForm1.insertCell();
        tdDesign.colSpan=2;
        tdDesign.id='design_spj_'+id;
        //cellule date
        let tdDate = trForm1.insertCell();
        tdDate.id='date_spj_'+id;
        let tdCate = trForm1.insertCell();
        tdCate.id='categorie_spj_'+id;
        let tdPv = trForm1.insertCell();
        tdPv.id='pv_spj_'+id;
        tdPv.setAttribute('data-cost',attrList.cost);
        tdPv.setAttribute('data-prix-vente',attrList.pv);
        tdPv.setAttribute('data-categorie',attrList.categorie);
        tdPv.setAttribute('data-previsionnel',attrList.isPrevisionnel);
        let tdRoi = trForm1.insertCell();
        tdRoi.id="roi_spj_"+id;
        tdRoi.classList.add('roi');
        Utils.setClassRoi(attrList.roi,tdRoi);
        let tdBtns = trForm1.insertCell();

        //ajout des boutons valider//annuler
        let span = document.createElement("span");
        span.id='btns_spj_'+id;
        let btnCheck = Utils.getBtnCheck(callbackCheck);
        btnCheck.id='valid_'+type+'_'+id;
        if (type==='create')
        {
            btnCheck.style.display="none";
        }
        let btnCancel = Utils.getBtnCancel(callbackCancel);
        btnCancel.id='cancel_'+type+'_'+id;
        span.appendChild(btnCheck);
        span.appendChild(btnCancel);
        tdBtns.appendChild(span);


        let trForm2 = document.createElement("tr");
        trForm2.id='head_date_spj_'+id;

        let trForm3 =document.createElement('tr');
        trForm3.id='body_date_spj_'+id;

        if (inputRecu!== null)
        {
            //titre et cellule pour recurrence
            let thRecu = document.createElement("th");
            thRecu.setAttribute('colspan',2);
            thRecu.innerText='Récurrence';
            trForm2.appendChild(thRecu);
            let tdRecu = trForm3.insertCell();
            tdRecu.id='recu_spj_'+id;
            tdRecu.appendChild(inputRecu);
            tdRecu.setAttribute('colspan',2);

        }
        //titre et cellule pour periodicite
        let thPerio = document.createElement("th");
        thPerio.innerText="Périodicité";
        trForm2.appendChild(thPerio);
        let tdPerio = trForm3.insertCell();
        tdPerio.id='perio_spj_'+id;
        let selectPerio = Utils.getSelectPeriodicite('spj_'+id,attrList.periodicite);
        tdPerio.appendChild(selectPerio);

        //titre et cellule pour isPrevisionnel
        let thPrevi = document.createElement("th");
        thPrevi.innerText='Prévisionnel';
        trForm2.appendChild(thPrevi);
        let tdPrevi = trForm3.insertCell();
        tdPrevi.id='previ_spj_'+id;
        let attrPrevi =({
           'type': 'checkbox',
            'value': true,
            'id': 'checkbox_previ_spj_'+id,
        });
        //la div qui contiendra l'input et le label pour toggle switch
        let divToggle = document.createElement("div");
        divToggle.className='custom-control custom-switch';
        let inputPrevi = Utils.getInput(attrPrevi);
        inputPrevi.className='custom-control-input';
        inputPrevi.checked=attrList.isPrevisionnel;
        inputPrevi.setAttribute('data-id-sous-projet',id);
        inputPrevi.addEventListener("change",previsionnel);
        //label necessaire pour toggle
        let labelPrevi = document.createElement("label");
        labelPrevi.className='custom-control-label';
        labelPrevi.setAttribute('for','checkbox_previ_spj_'+id);
        //association des elemnts
        divToggle.append(inputPrevi,labelPrevi);
        tdPrevi.appendChild(divToggle);

        //creation des attributs des champs de saisie
        //champs Designation
        let attrDesign = {
            "id":'input_design_spj_'+id,
            "type" : "text",
            "minlength":4,

        };
        if (attrList.designation==='Désignation')
        {
            attrDesign["placeholder"]=attrList.designation;
        }else{
            attrDesign["value"]=attrList.designation;
        }

        //ajout et creation du champs de saisie
        let inputDesign = Utils.getInput(attrDesign);
        tdDesign.appendChild(inputDesign);

        //champs Prix de Vente
        let attrPv = {
            "id":'input_pv_spj_'+id,
            "type":'number',
            'min':0,
            'value':attrList.pv,
            'step':0.01,
            'max':999999999,
        };
        let inputPv =Utils.getInput(attrPv);
        //ajout event pour maj roi si pv modifier
        inputPv.addEventListener("input",refreshOnInputPv);
        tdPv.appendChild(inputPv);

        //champ DATE
        //creation et ajout des selects pour la date (dans meme cellule
        let selectMonth = Utils.getSelectNumber(attrList.month,1,13,'select_month_spj_'+id);
        let selectYear = Utils.getSelectNumber(attrList.year,2014,2100,'select_year_spj_'+id);
        let div = document.createElement("div");
        div.className='input-group input-group-sm';
        div.appendChild(selectMonth);
        div.appendChild(selectYear);
        tdDate.appendChild(div);
        //ligne pour afficher les messages
        let trMessage = document.createElement('tr');
        let tdMessage = trMessage.insertCell();
        tdMessage.colSpan=4;
        trMessage.id='message_for_spj_'+id;
        trMessage.style.display='none';
        // champs CATEGORIE
        Categorie.loadCategories().then((categories)=>{
            let select = Utils.getSelectCategorie(categories,'select_categorie_spj_'+id,attrList.categorie);
            select.addEventListener("change",Refresh.modifierCategorie);
            tdCate.appendChild(select);

            resolve({trForm1,trMessage,trForm2,trForm3});
        })
    })

}









/**Fonction appeller sur oninput de l'input prix de vente
 * modifie le dataset prixVente et les roi  et prix de vente
 * @param event
 */
function refreshOnInputPv(event)
{
    let inputPv = event.currentTarget;
    let idSpj = Utils.getId(inputPv.id);
    let tds = getTdsAndInput(idSpj);
    let pv = Utils.formatFloat(inputPv.value);
    tds.tdPv.dataset.prixVente = pv;
    Refresh.refreshRoiPjAndSpj(idSpj);

}

function previsionnel(event)
{
    let input = event.currentTarget;
    let idSpj = input.dataset.idSousProjet;
    let tds = getTdsAndInput(idSpj);
    let checked = input.checked;
    let classTable = tds.tableSpj.classList;
    Utils.setClassPrevisionnel(classTable,checked);
    tds.trSpj.dataset.previsionnel=checked;
    tds.tdPv.dataset.previsionnel = checked;
    Refresh.refreshRoiPjAndSpj(idSpj);
}