import * as Utils from './utils/utils';
import * as Inde from './ajax/independant_ajax';

window.addEventListener('load',function () {

    if (Utils.isMobile())
    {
        let ajout = document.querySelector('.ajouter-element');
        ajout.id='create_inde';
    }
    document.querySelectorAll(".btn-valid-sup")
        .forEach(function( btnSupElement) {
            btnSupElement.addEventListener("click",validerSuppression);
        });
    document.querySelectorAll(".btn-mdf")
        .forEach(function( btnSupElement) {
            btnSupElement.addEventListener("click",formUpdtInde);
        });
    document.getElementById("create_inde").addEventListener("click",formCreateInde);
    document.querySelectorAll('.trie').forEach(function (td) {
        td.addEventListener("click",trieInde);
    });
});

//////////////////////////////// CREATION INDEPENDANT /////////////////////////

/**Formulaire pour creer un Inde
 *
 * @param event
 */
export function formCreateInde(event)
{
    event.preventDefault();
    let btnAdd = event.currentTarget;
    //on recupere le numero de formulaire
    //qui permet les ajots multiples
    let numForm = parseInt(btnAdd.dataset.nbForm)+1;
    btnAdd.dataset.nbForm=numForm;
    //on definit l'id qui servira pour toous les elements
    let idInde = 'inde_form'+numForm;
    //on recupere le body du tableau pour lui ajouter une ligne
    let tbody = document.getElementById('tbody');
    //contruire formulaire creation
    //ligne du formulaire
    let tr = tbody.insertRow(0);
    tr.setAttribute('id',idInde);
    //cellule du nom
    let tdNom = tr.insertCell();
    tdNom.id='nom_'+idInde;
    //attribut pour l'input
    let attrNom ={
        "type":'text',
        'placeholder':'Nom',
        'id':'input_nom_'+idInde,
        'minlength':'2'
    }
    let inputNom = Utils.getInput(attrNom);
    tdNom.appendChild(inputNom);
    //cellule du prenom
    let tdPrenom= tr.insertCell();
    tdPrenom.id='prenom_'+idInde;
    //attribut pour input
    let attrPrenom ={
        'type':'text',
        'placeholder':'Prénom',
        'minlentgh':2,
        'id':'input_prenom_'+idInde
    }
    let inputPrenom =Utils.getInput(attrPrenom);
    tdPrenom.appendChild(inputPrenom);

    //cellule du salaire
    let tdSalaire = tr.insertCell();
    tdSalaire.id='salaire_'+idInde;
    //attributs du salaire
    let attrSalaire ={
        'type':'number',
        'value':'1',
        'min':1,
        'step':0.1,
        'id':'input_salaire_'+idInde,
    }
    let inputSalaire = Utils.getInput(attrSalaire);
    tdSalaire.appendChild(inputSalaire);
    //cellule des boutons
    let tdBoutons = tr.insertCell();
    //span qui contiendra les boutons
    let span = document.createElement('span');
    span.id='btns_'+idInde;
    //bouton valider ajout avec callback
    let btnCheck = Utils.getBtnCheck(ajouter);
    btnCheck.id='add_inde_'+idInde;
    //bouton annuler avec callback
    let btnCancel = Utils.getBtnCancel(cancelCreateInde);
    btnCancel.id='cancel_add_inde_'+idInde;
    span.appendChild(btnCheck);
    span.appendChild(btnCancel);
    tdBoutons.appendChild(span);
}


function cancelCreateInde(event) {
    //on supprime la ligne
    Utils.cancel(event);
    // on recupere le bouton ajouter pouor modifier le nombre de formulaires en cours
    let btnAdd = document.getElementById('create_inde');
    let nbForm = parseInt(btnAdd.dataset.nbForm);
    //on retire un formulaire
    btnAdd.dataset.nbForm = nbForm-1;
}
/**Ajouter l'independant en base, modifie la ligne si ok sinon retourne les erreurs
 * TODO: retours des erreurs
 *
 * @param event
 */
function ajouter(event)
{
    event.preventDefault();
    let idInde = Utils.getId(event.currentTarget.id);
    let tds = getInputs(idInde);

    let tr = tds.trInde;
    let span = tds.span;
    //infos de l'independant modifier
    let tdNom =tds.tdNom;
    let tdPrenom =tds.tdPrenom;
    let tdSalaire =  tds.tdSalaire;
    let inde = getFormValues(idInde);
    if (inde!==null)
    {
        Inde.insert(inde).then((idInde)=>{
            //recuperer id generer pour le donner au tr
            tr.setAttribute('id','inde_'+idInde);
            tdSalaire.setAttribute('id','salaire_inde_'+idInde);
            tdNom.setAttribute('id','nom_inde_'+idInde);
            tdPrenom.setAttribute('id','prenom_inde_'+idInde);
            span.id='btns_inde_'+idInde;
            //changement des elzments input par le texte
            Utils.inputTextToText(tdNom);
            Utils.inputTextToText(tdPrenom);
            Utils.inputNumberToTextMoney(tdSalaire);
            Utils.inverseBtns(span,idInde,'inde',formUpdtInde,validerSuppression);
        },
            (erreurs)=>{
            traitementErreurs(erreurs,idInde);
            });
    }
}


///////////////////////////// MODIFICATION INDEPENDANT /////////////////////////////////////////////////

/**Creation du formulaire pour modifier un Independant;
 *
 * @param event
 */
function  formUpdtInde(event) {
    event.preventDefault();
     //recuperer les elements
    let idInde = Utils.getId(event.currentTarget.id);
    let tds = getInputs(idInde);
    //Ajout des inputs
    //input nom
    let attrNom ={
        "type":'text',
        'value':tds.tdNom.innerText,
        'id':'input_nom_inde_'+idInde,
        'minlength':'2'
    }
    let inputNom = Utils.getInput(attrNom);
    tds.tdNom.innerText="";
    tds.tdNom.appendChild(inputNom);
    //input prénom
    let attrPrenom ={
        'type':'text',
        'value':tds.tdPrenom.innerText,
        'minlentgh':2,
        'id':'input_prenom_inde_'+idInde
    }
    let inputPrenom =Utils.getInput(attrPrenom);
    tds.tdPrenom.innerText="";
    tds.tdPrenom.appendChild(inputPrenom);

    //input salaire
    let attrSalaire ={
        'type':'number',
        'value':Utils.numberFromEuroText(tds.tdSalaire.innerText),
        'min':1,
        'step':0.1,
        'id':'input_salaire_inde_'+idInde,
    }
    let inputSalaire = Utils.getInput(attrSalaire);
    tds.tdSalaire.innerText="";
    tds.tdSalaire.appendChild(inputSalaire);
    Utils.inverseBtns(tds.span,idInde,'inde',modifier,cancelUpdtInde);
    }


    function cancelUpdtInde(event)
    {
        event.preventDefault();
        let idInde = Utils.getId(event.currentTarget.id);
        let tds = getInputs(idInde);

        Inde.loadInde(idInde).then(function (inde) {
            //changement des valeurs des input
            tds.tdNom.innerText=Utils.capitalize(inde.nom);
            tds.tdPrenom.innerText=Utils.capitalize(inde.prenom);
            tds.tdSalaire.innerText= Utils.formatEuro(inde.salaire);
            Utils.inverseBtns(tds.span,idInde,'inde',formUpdtInde,validerSuppression);
        })
    }

/**
 *
 * @param event
 */
function modifier(event) {
    event.preventDefault();
    let idInde = Utils.getId(event.currentTarget.id);

    let tds = getInputs(idInde);
    let inde = getFormValues(idInde);

    if (inde!== null)
    {
        inde.id=idInde;
        Inde.update(inde).then(() => {

            tds.tdNom.innerText=Utils.capitalize(inde.nom);
            tds.tdPrenom.innerText=Utils.capitalize(inde.prenom);
            tds.tdSalaire.innerText= Utils.formatEuro(inde.salaire);
            Utils.inverseBtns(tds.span,idInde,'inde',formUpdtInde,validerSuppression);
        });
    }

}

/////////////////////////// SUPPRESSION INDEPENDANT ///////////////////////////////////////////////////

/**
 * Demande une confirmatrion pour supprimer l'independant
 * @param event
 */
function validerSuppression(event)
{
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    let span = base.span;
    let idInde = base.id;
    Utils.inverseBtns(span,idInde,'inde',supprimer,cancelSupInde);
}

/**
 * Annulation de la suppression
 * @param event
 */
function cancelSupInde(event)
{
    event.preventDefault();
    let base = Utils.getinfosBase(event);
    Utils.inverseBtns(base.span,base.id,'inde',formUpdtInde,validerSuppression);
}

    /**
     * Supprime l'independant choisit en BDD
     * @param event
     */
    function supprimer(event)
        {
            event.preventDefault();
            let base = Utils.getinfosBase(event);
            let tr = base.tr;
            let idInde = base.id;

            Inde.remove(idInde).finally(()=>{
                tr.remove();
            })
        }




/////////////////////////////////////////// OUTILS /////////////////////////////////////////////




/**Renvoie les inputs et leurs valeurs
 *
 * @param idInde
 * @param tr
 * @returns {{tdPrenom: HTMLElement, tdSalaire: *, tdNom: HTMLElement, nom: *, prenom: *}}
 */
export  function getInputs(idInde)
{
        let tdNom =document.getElementById('nom_inde_'+idInde);
        let nom = document.getElementById('input_nom_inde_'+idInde)
        let tdPrenom =document.getElementById('prenom_inde_'+idInde);
        let prenom = document.getElementById('input_prenom_inde_'+idInde)
        let tdSalaire = document.getElementById('salaire_inde_'+idInde);
        let salaire = document.getElementById('input_salaire_inde_'+idInde)
        let span = document.getElementById('btns_inde_'+idInde);
        let trInde = document.getElementById('inde_'+idInde);
    return {
        "tdNom":tdNom,
        "nom":nom,
        "tdPrenom":tdPrenom,
        "prenom":prenom,
        "tdSalaire":tdSalaire,
        "salaire":salaire,
        "span":span,
        "trInde":trInde,
    }
}


function getFormValues(idInde)
{
    let tds =getInputs(idInde);
    let indeInputs = {
        "nom":tds.nom,
        "prenom":tds.prenom,
        "salaire":tds.salaire,
    };

    // if (validerIndeForm('inde_'+idInde,indeInputs))
    // {
        return{
            "nom":indeInputs.nom.value,
            "prenom":indeInputs.prenom.value,
            "salaire":indeInputs.salaire.value
        };
    // }else{
    //     return null;
    // }
}

function trieInde(event)
{
    event.stopPropagation();
    event.preventDefault();
    //recuperation des informations pour le tris
    let target=event.currentTarget;
    let name = target.dataset.name;
    let columnNum = parseInt(target.dataset.column);
    let type=target.dataset.type;
    let sort = parseInt(target.dataset.sort);
    let names = ['nom','prenom','salaire'];

    //on recupere les lignes des projets
    let lignes = document.querySelectorAll('.inde-ligne');
    //on recupere le body
    let tBody = document.getElementById('tbody');
    let tab=[];
    // on place les elements à trier dans un tableau
    lignes.forEach(function (ligne) {

        tab.push(ligne.children[columnNum].getAttribute('data-'+name).toLowerCase());
    })
    //on trie le tableau des elements
    // croissant
    if (type==='number')
    {
        //si les elements sont des nombres
        tab.sort(function (a,b) {
            let numA =parseFloat(a);
            let numB = parseFloat(b);
            return numA-numB;
        });
    }else {
        tab.sort();
    }
    //modifications des icones
    if (sort === 1)
    {
        //trie croissant
        target.dataset.sort =0;
        document.getElementById(name+'_down').style.display='none';
        document.getElementById(name+'_up').style.display='';
    }else{
        //on inverse le tableau

        tab.reverse();
        target.dataset.sort=1;
        document.getElementById(name+'_up').style.display='none';
        document.getElementById(name+'_down').style.display='';
    }
    for (let nameElement of names) {
        //on masque les icones des autres elemnts
        if (nameElement !== name)
        {
            document.getElementById(nameElement+'_up').style.display='none';
            document.getElementById(nameElement+'_down').style.display='none';
        }
    }
    //on les remplace par les lignes
    lignes.forEach(function (ligne) {
        let data = ligne.children[columnNum].getAttribute('data-'+name).toLowerCase();
        let index = tab.indexOf(data);
        tab[index]=ligne;
    })
    for (let projet of tab) {
        tBody.appendChild(projet);
    }
}
export function validerIndeForm(idMessage,indeInputs)
{
    let valid = true;

    let nom = indeInputs.nom.value;
    let prenom = indeInputs.prenom.value;
    let salaire = indeInputs.salaire.value;
    if (nom===null || !isNaN(nom) || nom.length<3)
    {
        Utils.setValid(indeInputs.nom.classList,false);
        indeInputs.nom.setCustomValidity('Le nom doit être renseigné et faire au moins 3 caractères');
        valid=false;
    }else{
        Utils.setValid(indeInputs.nom.classList,true);
    }

    if (prenom===null || !isNaN(prenom) || prenom.length<3)
    {
        Utils.setValid(indeInputs.prenom.classList,false);
        indeInputs.prenom.setCustomValidity('Le prénom doit être renseigné et faire au moins 3 caractères');
        valid=false
    }else{
        Utils.setValid(indeInputs.prenom.classList,true);
    }

    if (salaire===null || isNaN(salaire) || salaire<0)
    {
        Utils.setValid(indeInputs.salaire.classList,false);
        indeInputs.salaire.setCustomValidity('Le salaire doit être renseigné et être supérieur à 0');
        valid=false;
    }else{
        Utils.setValid(indeInputs.salaire.classList,true)
    }
    return valid;
}


export function traitementErreurs(erreurs,idInde)
{
    let inputs = getInputs(idInde);
    let message = Utils.traitementErreursInde(erreurs,inputs);

    Utils.afficherMessage(message,'independant','bg-danger');
}