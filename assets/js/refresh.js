import * as Utils from "./utils/utils";
import * as Heure from "./heure";
import * as Projet from './projet';
import * as SousProjet from './sousProjet';
import * as Filtre from './filtres';
import * as Inde from "./ajax/independant_ajax";
import {loadAllByFiltre} from "./ajax/sous_projet_ajax";
import {userSup} from "./projetList";


/**Recupere le cout d'un projet en fonction des saisies des prestations et des heures
 *Ajoute le cout en attrbut sur la cellule prix de vente du sous-projet 'data-cost' = coutTotal
 * @param idSpj
 * @returns {number}
 */
export function refreshCostSpjOnDom(idSpj)
{
    //recuperation des lignes presta et heures
    let pAndH = getTrPrestaAndHeureSpj(idSpj);
    let idFiltre = Filtre.getFiltreValues().independant;

    let tdsSpj = SousProjet.getTdsAndInput(idSpj);
    let costInde=0;
   let costAutre=0;
    let totalCost =0;
    //total du cout des heures
    pAndH.heures.forEach(function (heure) {
        let idHeure = Utils.getId(heure.id);
        let tds =Heure.getTdAndInputs(idHeure);
        totalCost = parseFloat(totalCost)+parseFloat(tds.tdCout.dataset.cost);
        if (idFiltre!==null)
        {
           if (tds.trHeure.dataset.idInde === idFiltre)
           {
               costInde+=parseFloat(tds.tdCout.dataset.cost);
           }else{
               costAutre+=parseFloat(tds.tdCout.dataset.cost);
           }
        }
    });
    // total du cout des prestations
    pAndH.prestas.forEach(function (presta) {
        let idPresta = Utils.getId(presta.id);
        let tdMnt = document.getElementById('mnt_presta_'+idPresta);
        totalCost = parseFloat(totalCost)+parseFloat(tdMnt.dataset.cost);

    })

    tdsSpj.tdPv.setAttribute('data-cost',totalCost);
    if (idFiltre!==null) {
        let tdHeure = tdsSpj.tdsProjet.tdHeure;
        if (tdHeure!==null)
        {
            tdsSpj.tdsProjet.tdHeure.dataset.cost = costInde;
            tdsSpj.tdsProjet.tdHeure.dataset.costAutre = costAutre;
        }
    }
}

/**Retourne lee body de chaque sous-projet d'un projet donné
 *
 * @param idPj
 * @param periodicite
 * @returns {[HTMLTableSectionElement]}
 */
function getTbodiesSpj(idPj,periodicite)
{
    let trSpj = document.querySelectorAll('.spj_'+periodicite+'_'+idPj);
    let tBodies=[];
    trSpj.forEach(function (tr) {

            let tBody = tr.getElementsByTagName('tbody');
            //recuperer le premier element de la collection
            tBodies.push(tBody[0]);

    })
    return tBodies
}

/**Recupere le cout total  et le prix de vente d'un projet
 * Ajoute un data-cost=coutTotal sur la cellule prix de vente du projet
 * et Ajoute un data-prix-vente=prixVente sur la cellule prix de vente du projet
 * @param idPj
 * @param periodicite
 * @returns {number}
 */
export function refreshCostAndPvPj(idPj,periodicite)
{
    let tBodies = getTbodiesSpj(idPj,periodicite);
    let totalCost =0;
    let pv =0;
    let costPrevi=0;
    let pvPrevi = 0;
    let costNonPrevi = 0;
    let pvNonPrevi =0;
    tBodies.forEach(function (tBody) {
        let idSpj = Utils.getId(tBody.id);
        refreshCostSpjOnDom(idSpj);
        let tdPv = document.getElementById('pv_spj_'+idSpj);
        let r = tdPv.dataset.recurrence;
        if (r== null)
        {
            r=1;
        }

        totalCost = parseFloat(totalCost)+parseFloat(tdPv.dataset.cost)*r;
        pv = parseFloat(pv)+parseFloat(tdPv.dataset.prixVente)*r;
        if (tdPv.dataset.previsionnel==='true')
        {
            costPrevi = parseFloat(costPrevi)+parseFloat(tdPv.dataset.cost)*r;
            pvPrevi = parseFloat(pvPrevi)+parseFloat(tdPv.dataset.prixVente)*r;
        }else if (tdPv.dataset.previsionnel==='false'){
            costNonPrevi = parseFloat(costNonPrevi)+parseFloat(tdPv.dataset.cost)*r;
            pvNonPrevi = parseFloat(pvNonPrevi)+parseFloat(tdPv.dataset.prixVente)*r;
        }

    })
    let tdPj = document.getElementById('pv_projet_'+periodicite+'_'+idPj);
    tdPj.setAttribute('data-cost',totalCost);
    tdPj.setAttribute('data-prix-vente',pv);
    tdPj.setAttribute('data-cost-previ',costPrevi);
    tdPj.setAttribute('data-pv-previ',pvPrevi);
    tdPj.setAttribute('data-cost-non-previ',costNonPrevi);
    tdPj.setAttribute('data-pv-non-previ',pvNonPrevi);
    tdPj.innerText=Utils.formatEuro(pv);
    return {
        "totalCost":totalCost,
        "prixVente":pv,
        "costPrevi":costPrevi,
        "pvPrevi":pvPrevi,
        "costNonPrevi":costNonPrevi,
        "pvNonPrevi":pvNonPrevi,
    };
}
export function modifierCategorie(event)
{
    let select = event.currentTarget;
    let idCate = select.value;
    let idSpj = Utils.getId(select.id);
    let tdPv = document.getElementById('pv_spj_'+idSpj);
    tdPv.dataset.categorie=idCate;
    refreshRoiPjAndSpj(idSpj);

}
function refreshPvANdCostProjet(idPj,periodicite)
{
    let tBodies = getTbodiesSpj(idPj,periodicite);
    let tdPvProjet = document.getElementById('pv_projet_'+periodicite+'_'+idPj);
    let cate={
        "pv1":0,
        "cost1":0,
        "pv2":0,
        "cost2":0,
        "pv3":0,
        "cost3":0,
        "pv4":0,
        "cost4":0,
        "pv5":0,
        "cost5":0,
        "pv6":0,
        "cost6":0,
    };

    tBodies.forEach(function (tBody) {
        let idSpj = Utils.getId(tBody.id);
        let tdPv = document.getElementById('pv_spj_'+idSpj);
        let categorie = parseInt(tdPv.dataset.categorie);
        let r = tdPv.dataset.recurrence;
        if (r== null)
        {
            r=1;
        }
        switch (categorie) {
            case 1:
                cate.pv1+=parseFloat(tdPv.dataset.prixVente)*r;
                cate.cost1+=parseFloat(tdPv.dataset.cost)*r;
                break;
            case 2:
                cate.pv2+=parseFloat(tdPv.dataset.prixVente)*r;
                cate.cost2+=parseFloat(tdPv.dataset.cost)*r;
                break;
            case 3:
                cate.pv3+=parseFloat(tdPv.dataset.prixVente)*r;
                cate.cost3+=parseFloat(tdPv.dataset.cost)*r;
                break;
            case 4:
                cate.pv4+=parseFloat(tdPv.dataset.prixVente)*r;
                cate.cost4+=parseFloat(tdPv.dataset.cost)*r;
                break;
            case 5:
                cate.pv5+=parseFloat(tdPv.dataset.prixVente)*r;
                cate.cost5+=parseFloat(tdPv.dataset.cost)*r;
                break;
            case 6:
                cate.pv6+=parseFloat(tdPv.dataset.prixVente)*r;
                cate.cost6+=parseFloat(tdPv.dataset.cost)*r;
                break;
        }
    });

    for (let [nom,value] of Object.entries(cate)) {
        tdPvProjet.setAttribute('data-'+nom,value);
    }
}




export function setRoi(id,tdRoi)
{
    let tdPv = document.getElementById(id);
    let roi = Utils.calculRoi(tdPv.dataset.cost,tdPv.dataset.prixVente);
    Utils.setClassRoi(roi,tdRoi);
}



/**Modifie la cellule du roi du pj donner
 * rafraichit les data cout et pv des sous-projets et du projet
 * @param idPj
 * @param periodicite
 */

export function refreshRoiPj(idPj,periodicite)
{
    userSup();
    refreshCostAndPvPj(idPj,periodicite);
    let tdRoi =document.getElementById('roi_projet_'+periodicite+'_'+idPj);
    let id='pv_projet_'+periodicite+'_'+idPj;
    setRoi(id,tdRoi);
    refreshGlobaux();
}

/**Modifie la cellule roi du sous-projet donner
 * Rafraichit les data cout et prixVente du sous-projet
 *
 * @param idSpj
 */
export function refreshRoiSpj(idSpj)
{
    refreshCostSpjOnDom(idSpj);
    let tdRoi = document.getElementById('roi_spj_'+idSpj);
    let id='pv_spj_'+idSpj;
    setRoi(id,tdRoi);
}




export function getTrPrestaAndHeureSpj(idSpj,justForms=false)
{
    //recuperation des lignes autres prestations et heures
    let trComplement = document.querySelectorAll('.compl_spj_'+idSpj);
    if (justForms)
    {
        trComplement = document.querySelectorAll('.form_compl_spj_'+idSpj);
    }
    const hRegex = RegExp('heure');
    const prestRegex = RegExp('presta');
    let trHeures = [];
    let trPrestas =[]
    trComplement.forEach((ele)=>{
        if (hRegex.test(ele.id))
        {
            //ligne des heures
            trHeures.push(ele);

        }else if (prestRegex.test(ele.id))
        {
            // lignes des prestations
            trPrestas.push(ele);
        }
    });
    return {
        "prestas":trPrestas,
        "heures":trHeures
    };
}


/**Modifie les cellules des roi du sous-projet de son projets et des sous-projets lier
 *
 * @param idSpj
 */
export function  refreshRoiPjAndSpj(idSpj) {
    let tds = SousProjet.getTdsAndInput(idSpj);
    let idPj = tds.projetId;
    let periodicite = tds.projetPerio;

    refreshRoiSpj(idSpj);
    refreshRoiPj(idPj,periodicite);
    refreshPvANdCostProjet(idPj,periodicite);
    refreshDataFicheCategorie();
    refreshFicheCategorie();
    refreshHeuresInde(idPj,periodicite);
}



export function refreshSalaire(idheure,idInde)
{
    return new Promise((resolve => {
        let tds = Heure.getTdAndInputs(idheure);
        Inde.loadInde(idInde).then((inde)=>{
            let salaire = inde.salaire;
            tds.tdCout.setAttribute("data-salaire",salaire);
            resolve();
        });

    }))

}

/**Actualise le dataset cost et la cellule tdCout de l'heure correspondante
 *
 * @param idHeure
 */
export function refreshCostHeure(idHeure)
{

    let tds = Heure.getTdAndInputs(idHeure);
    let heure = parseFloat(tds.tdHeure.dataset.nbHeures);
    if (!isNaN(heure))
    {
        let salaire = Utils.formatFloat(tds.tdCout.dataset.salaire);
        let cost = heure*salaire;
        tds.tdCout.dataset.cost = cost;
        tds.tdHeure.dataset.nbHeures=heure;
        tds.tdCout.innerText=Utils.formatEuro(cost);
    }
}





export function refreshGlobaux()
{
    let infos = Utils.getTotauxProjets();
    let filtres = Filtre.getFiltreValues();
    let mensuel = infos.mensuel;
    let oneshot = infos.oneshot;
    //recuperation des elements du DOM de la fiche chiffres avec previ
    let globalPerio =  document.getElementById('global-perio');
    let ligneM = document.getElementById('ligne-MENSUEL');
    let ligneTotal = document.getElementById('ligne-TOTAL');
    let ligneOS = document.getElementById('ligne-ONESHOT');
    //recuperation des elements du dom de la fiche des chiffres hors previ
    let ligneNonPreviM = document.getElementById('ligne-non-previ-MENSUEL');
    let ligneNonPreviTotal = document.getElementById('ligne-non-previ-TOTAL');
    let ligneNonPreviOS = document.getElementById('ligne-non-previ-ONESHOT');
    let globalPerioNonPrevi =  document.getElementById('global-perio-non-previ');
    if (mensuel !== undefined)
    {
        refreshLignePerio('MENSUEL',mensuel.roi,mensuel.pv,ligneM);
        refreshLignePerio('non-previ-MENSUEL',mensuel.roiNonPrevi,mensuel.pvNonPrevi,ligneNonPreviM);
    }else{
        ligneM.style.display='none';
        ligneNonPreviM.style.display='none';
    }
    if (oneshot!== undefined)
    {
        refreshLignePerio('ONESHOT',oneshot.roi,oneshot.pv,ligneOS);
        refreshLignePerio('non-previ-ONESHOT',oneshot.roiNonPrevi,oneshot.pvNonPrevi,ligneNonPreviOS);
    }else{
        ligneOS.style.display='none';
        ligneNonPreviOS.style.display='none';
    }
    if (oneshot!== undefined && mensuel !== undefined)
    {
        if (oneshot.pv>0 && mensuel.pv>0)
        {
            let pvTotal = oneshot.pv+mensuel.pv;
            let roi = Utils.calculRoi((oneshot.cout+mensuel.cout),pvTotal);
            refreshLignePerio('TOTAL',roi,pvTotal,ligneTotal);
        }else{
            ligneTotal.style.display='none';
        }
        if (oneshot.pvNonPrevi>0 && mensuel.pvNonPrevi>0)
        {
            let pvTotalNonPrevi = oneshot.pvNonPrevi+mensuel.pvNonPrevi;
            let roiNonPrevi = Utils.calculRoi((oneshot.coutNonPrevi+mensuel.coutNonPrevi),pvTotalNonPrevi);
            refreshLignePerio('non-previ-TOTAL',roiNonPrevi,pvTotalNonPrevi,ligneNonPreviTotal);
        }else{
            ligneNonPreviTotal.style.display='none';
        }
    }else{
        ligneTotal.style.display='none';
        ligneNonPreviTotal.style.display='none';
    }
    if (oneshot=== undefined && mensuel === undefined)
    {
        globalPerio.style.display='none';
        globalPerioNonPrevi.style.display='none';
    }else{
        globalPerio.style.display='';
        globalPerioNonPrevi.style.display='';
    }
    if (filtres.previsionnel==='non-previ')
    {
        globalPerio.style.display='none';
    }
    if (filtres.previsionnel==='previ')
    {
        globalPerioNonPrevi.style.display='none';
    }
}

function refreshLignePerio(id,roi,pv,ligne) {
    if (pv>0)
    {
        let celPV = document.getElementById('pv-'+id);
        let celRoi = document.getElementById('roi-'+id);
        celPV.innerText=Utils.formatEuro(pv);
        Utils.setClassRoi(roi,celRoi);
        ligne.style.display='';
    }else{
        ligne.style.display='none';
    }

}


export function refreshDatasCategoriesProjets()
{
    return new Promise(resolve => {
        let filtres = Filtre.getFiltreValues();
        loadAllByFiltre(filtres).then((liste)=>{
            let mensuel = liste.MENSUEL;
            for (let projetId in mensuel) {
                let tdProjet = document.getElementById('pv_projet_MENSUEL_'+projetId);
                let dataProjet = mensuel[projetId];
                for (const [dataName,value] of Object.entries(dataProjet)) {
                    if(tdProjet != null) tdProjet.setAttribute('data-'+dataName,value);
                }
            }
            let oneshot = liste.ONESHOT;
            for (let projetId in oneshot) {
                let tdProjet = document.getElementById('pv_projet_ONESHOT_'+projetId);
                let dataProjet = oneshot[projetId];
                for (const [dataName,value] of Object.entries(dataProjet)) {
                    if(tdProjet != null) tdProjet.setAttribute('data-'+dataName,value);
                }
            }
            resolve();
        })
    });

}


export function refreshCategorie()
{
    refreshDatasCategoriesProjets().then(()=>{
        refreshDataFicheCategorie();
        refreshFicheCategorie();
    });
}




export function refreshFicheCategorie()
{
    let display='none';
    let ficheCate = document.getElementById('global-cate');
    for (let i=1;i<7;i++)
    {
        let ligneCate = document.getElementById('ligne-'+i)
        let pvCate = document.getElementById('pv_cate_'+i);
        let roiCate = document.getElementById('roi_cate_'+i);
        if (pvCate.dataset.prixVente>0)
        {
            display='';
            let roi = Utils.calculRoi(pvCate.dataset.cost,pvCate.dataset.prixVente);
            pvCate.innerText=Utils.formatEuro(pvCate.dataset.prixVente);
            Utils.setClassRoi(roi,roiCate);
            ligneCate.style.display='';
        }else{
            ligneCate.style.display='none';
        }
    }
    ficheCate.style.display=display;
}



export function refreshDataFicheCategorie()
{
    //on recupere toutes les cellules pv des projets sur le DOM;
    let projetsListe = document.querySelectorAll('.infos_ONESHOT , .infos_MENSUEL');
    //on parcours pour chaques categories
    for (let i=1;i<7;i++)
    {

        let pvCate = document.getElementById('pv_cate_'+i);
        let cost=0;
        let prixVente=0;
        //on recupere le pv et le cout de la categorie sur chaque projet
        projetsListe.forEach(function (tdPvProjet) {
            cost += parseFloat(tdPvProjet.getAttribute('data-cost'+i));
            prixVente += parseFloat(tdPvProjet.getAttribute('data-pv'+i));
        })
        pvCate.setAttribute('data-cost',cost);
        pvCate.setAttribute('data-prix-vente',prixVente);
    }
}

export function refreshRecurrence(event)
{
    let input = event.currentTarget;
    let recurrence = (isNaN(input.value)?1:parseInt(input.value));
    let tds = SousProjet.getTdsAndInput(Utils.getId(input.id));
    tds.tdPv.setAttribute('data-recurrence',recurrence);
    refreshRoiPjAndSpj(Utils.getId(input.id));
}

export function refreshHeuresInde(idProjet,periodicite)
{
    let tdsProjet = Projet.getTdsAndInput(periodicite,idProjet);
    let idInde = Filtre.getFiltreValues().independant;

    if (tdsProjet.tdHeure!==null)
    {
    let selector = '.heures_inde_'+periodicite+'_'+idProjet+'_'+idInde;
    let tdHeures = document.querySelectorAll(selector);
    let heures =0;
    tdHeures.forEach(function (tdHeure) {
        heures+=parseFloat(tdHeure.dataset.nbHeures);
    });

        tdsProjet.tdHeure.dataset.heures=heures;
        tdsProjet.tdHeure.innerText=heures+' H';
    }
    getFicheIndeValuesOnDom();
}



export function getFicheIndeValuesOnDom()
{
    let idInde = Filtre.getFiltreValues().independant;
    //nombre d'heures
    let tdHeures = document.querySelectorAll('.heures_inde_projet_'+idInde);
    let heures=0;
    let missions = 0;
    let cout = 0;
    let costAutre = 0;
    let pv = 0;
    tdHeures.forEach(function (tdH) {
        heures+=parseFloat(tdH.dataset.heures);
        missions+=parseInt(tdH.dataset.nbSousProjetsInde);
        cout+=parseFloat(tdH.dataset.cost);
        costAutre+= parseFloat(tdH.dataset.costAutre);
    });
    let projets = document.querySelectorAll('.projet-ligne');
    projets.forEach(function (pj) {
        let idPj = Utils.getId(pj.id);
        let periodicite = Projet.getPeriodicite(pj.id);
        let tdsProjet = Projet.getTdsAndInput(periodicite,idPj);
        pv += parseFloat(tdsProjet.tdPv.dataset.prixVente);
    })
    pv = pv-costAutre;
    let ratio = cout/pv;
    let  ficheInde = {
        "nbHeures":heures,
        "nbProjets":missions,
        "totalCost":cout,
        "pv":pv,
        "ratio": ratio,
    };
    Filtre.modifierFicheInde(ficheInde);
}