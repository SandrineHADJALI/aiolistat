import * as Projet from "./projet";
import * as SousProjet from "./sousProjet";
import * as Utils from './utils/utils';
import * as Prestations from "./prestations";
import * as Heure from "./heure";
import * as Refresh from './refresh';


export function prestaForm(idPresta)
{
    let tds = Prestations.getTdAndInputs(idPresta);
    let detail = tds.inputDetail.value.trim();
    let mnt = tds.inputMnt.value.trim();
    let valid = true;
    if ( detail.length<4)
    {
        valid = false;
        Utils.setValid(tds.inputDetail.classList,false);
        Utils.afficherMessage('Le détail doit faire au moins 4 caractères','presta_'+idPresta,'error');
    }else{
        Utils.setValid(tds.inputDetail.classList,true);
    }
    if (isNaN(mnt) || mnt<=0)
    {
        valid=false;
        Utils.setValid(tds.inputMnt.classList,false);
        Utils.afficherMessage('Le montant doit être supérieur à 0','presta_'+idPresta,'error');
    }else{
        Utils.setValid(tds.inputMnt.classList,true);
    }
    if (valid)
    {
        document.getElementById('message_for_presta_'+idPresta).style.display='none';
    }
    return valid;
}



export function heureForm(idHeure)
{
    let tds = Heure.getTdAndInputs(idHeure)
    let heure = tds.inputHeure.value;
    let valid = true;
    if (isNaN(heure) || heure<=0)
    {
        valid=false;
        Utils.setValid(tds.inputHeure.classList,false);
        Utils.afficherMessage('Le nombre d\'heures doit être supérieur à 0','heure_'+idHeure,'error');
    }else{
        Utils.setValid(tds.inputHeure.classList,true);
        Utils.setValid(tds.inputCommentaire.classList,true);
    }
    let idInde = tds.selectInde.value;
    if (isNaN(idInde))
    {
           valid=false;
           Utils.setValid(tds.selectInde.classList,false);
           Utils.afficherMessage('Veuillez ajouter l\'indépendant','heure_'+idHeure,'error');
    }
    if (valid)
    {
        document.getElementById('message_for_heure_'+idHeure).style.display='none';
    }
    return valid;
}


export function projetForm(periodicite,idProjet)
{
    let tds = Projet.getTdsAndInput(periodicite,idProjet);
    let nom = tds.inputNom.value;
    let valid = true;
    if ( nom.length<4)
    {
        valid = false;
        Utils.setValid(tds.inputNom.classList,false);
    }else{
        Utils.setValid(tds.inputNom.classList,true);
    }
    return valid;
}


export function sousProjetForm(idSpj)
{
    let tds = SousProjet.getTdsAndInput(idSpj);
    let designation = tds.inputDesign.value;
    let pv = tds.inputPv.value;
    let valid =true;
    let month = parseInt(tds.selectMonth.value);
    let year = parseInt(tds.selectYear.value);
    let date = new Date();
    let idMessage='spj_'+idSpj;
    if (tds.inputRecu != null)
    {
        let recurrence = tds.inputRecu.value;
        if (recurrence<=0)
        {
            valid = false;
            Utils.setValid(tds.inputRecu.classList,false);
            Utils.afficherMessage('La récurrence ne peut pas être inférieure à 1',idMessage,'error');
        }else if (recurrence>25) {
            valid = false;
            Utils.setValid(tds.inputRecu.classList,false);
            Utils.afficherMessage('La récurrence ne peut pas être supérieure à 24',idMessage,'error');
        }else{
                Utils.setValid(tds.inputRecu.classList,true);
        }
    }
    if ( designation.length<4)
    {
        valid = false;
        Utils.setValid(tds.inputDesign.classList,false);
        Utils.afficherMessage('La désignation doit être renseignée et faire au minimum 4 caractères',idMessage,'error');
    }else{
        Utils.setValid(tds.inputDesign.classList,true);
    }
    if (pv<0)
    {
        valid =false;
        Utils.setValid(tds.inputPv.classList,false);
        Utils.afficherMessage('Le prix de vente doit être renseigné et supérieur ou égal à 0',idMessage,'error');
    }else{
        Utils.setValid(tds.inputPv.classList,true);
    }

    /*if (month<date.getMonth() && year===date.getFullYear())
    {
        valid=false;
        Utils.setValid(tds.selectMonth.classList,false);
        Utils.setValid(tds.selectYear.classList,false);
        Utils.afficherMessage('La date doit être supérieure à la date du jour',idMessage,'error');
    }else if (date.getFullYear()>year)
    {
        valid=false;
        Utils.setValid(tds.selectMonth.classList,false);
        Utils.setValid(tds.selectYear.classList,false);
        Utils.afficherMessage('La date doit être supérieure à la date du jour',idMessage,'error');
    }*/
        Utils.setValid(tds.selectMonth.classList,true);
        Utils.setValid(tds.selectYear.classList,true);

    if (valid)
    {
        document.getElementById('message_for_'+idMessage).style.display='none';
    }
    let compl = Refresh.getTrPrestaAndHeureSpj(idSpj,true);
    let heures = compl.heures;
    let prestas = compl.prestas;
    if (heures.length>0)
    {
        heures.forEach(function (trHeure) {
            let idHeure = Utils.getId(trHeure.id);
            if (!heureForm(idHeure)){
                valid=false;
            }
        });
    }
    if (prestas.length>0)
    {
        prestas.forEach(function (trPresta) {
            let idPresta = Utils.getId(trPresta.id);
            if (!prestaForm(idPresta))
            {
                valid=false;
            };
        });
    }
    return valid;
}








    export function indeForm(idMessage,indeInputs)
    {
        let valid = true;

        let nom = indeInputs.nom.value;
        let prenom = indeInputs.prenom.value;
        let salaire = indeInputs.salaire.value;
        if (nom===null || !isNaN(nom) || nom.length<3)
        {
            Utils.setValid(indeInputs.nom.classList,false);
            Utils.afficherMessage('Le nom doit être renseigné et faire au moins 3 caractères',idMessage,'error');
            valid=false;
        }else{
            Utils.setValid(indeInputs.nom.classList,true);
        }

        if (prenom===null || !isNaN(prenom) || prenom.length<3)
        {
            Utils.setValid(indeInputs.prenom.classList,false);
            Utils.afficherMessage('Le prénom doit être renseigné et faire au moins 3 caractères',idMessage,'error');
            valid=false
        }else{
            Utils.setValid(indeInputs.prenom.classList,true);
        }

        if (salaire===null || isNaN(salaire) || salaire<0)
        {
            Utils.setValid(indeInputs.salaire.classList,false);
            Utils.afficherMessage('Le salaire doit être renseigné et être supérieur à 0',idMessage,'error');
            valid=false;
        }else{
            Utils.setValid(indeInputs.salaire.classList,true)
        }
        return valid;
    }