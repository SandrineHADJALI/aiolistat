import * as Utils from "./utils/utils";
import * as Spj from './ajax/sous_projet_ajax';
import * as Prestations from "./prestations";
import * as Heure from "./heure";
import * as SousProjet from "./sousProjet";
import * as Projet from './projet';
import * as Refresh from './refresh';
import * as Filtre from './filtres';
import * as Tris from './tris';

let isAdmin = true;
let isUser = false;
window.addEventListener('load',function()
{
    let mobile = document.getElementById('div-general').dataset.mobile;
    if (mobile==='true')
    {

        let ajout = document.querySelector('.ajouter-element');
        ajout.id='add_projet';
        let filtres = document.getElementById('afficher-filtres');
        filtres.addEventListener("click",afficherElement);
        filtres.style.display='';
        let infos = document.getElementById('afficher-infos');
        infos.addEventListener("click",afficherElement);
        infos.style.display='';
    }

    document.querySelectorAll('.btn-trie-spj').forEach(function (element) {
        element.addEventListener("click",Tris.trierSousProjets);
    })

    document.querySelectorAll(".projet-ligne")
        .forEach(function( ligne) {
            if (Projet.getPeriodicite(ligne.id)!=='VIDE')
            {
                ligne.addEventListener("click",afficherMasquerSPJ);
                ligne.style.cursor='pointer';
                ligne.setAttribute('data-visibility','hidden');
            }
        });
    document.querySelectorAll(".btn-mdf")
        .forEach(function (btnMdf) {
            btnMdf.addEventListener("click",Projet.formForUpdtPj);
        })
    document.querySelectorAll(".btn-valid-sup")
        .forEach(function (btnSup) {
            btnSup.addEventListener("click",Projet.validerSupPj);
        })
    document.querySelectorAll(".btn-add")
        .forEach(function (btnAdd) {
            btnAdd.addEventListener("click",SousProjet.formAddSpj);
    })
    document.getElementById('add_projet').addEventListener("click",Projet.tableForCreatePj);
    document.getElementById('titre_VIDE').addEventListener("click", afficherListe);
    document.getElementById('titre_MENSUEL').addEventListener("click", afficherListe);
    document.getElementById('titre_ONESHOT').addEventListener("click", afficherListe);
    document.querySelectorAll('.trie').forEach(function (head) {
        head.addEventListener('click',Tris.trier);
    });
    Refresh.refreshGlobaux();
    Refresh.refreshCategorie();
});

window.addEventListener('DOMContentLoaded', function() {

    let userAdmin = document.querySelector('.js-user-admin');
    isAdmin = userAdmin.dataset.isAdmin;
    let userUser = document.querySelector('.js-user-user');
    isUser = userUser.dataset.isUser;
    userSup();


})

export function afficherListe(event)
{
    let periodicite = Utils.getId(event.currentTarget.id);
    let tab = document.getElementById('list_projet_'+periodicite);
    let display= tab.style.display;
    if (display === 'none')
    {
        display='block';
    }else{
        display='none';
    }

    tab.style.display=display;
}

export function afficherMasquerSPJ(event)
{
    event.preventDefault();
    event.stopPropagation();
    let trProjet = event.currentTarget;
    let idPj=Utils.getId(trProjet.id);
    // perdiodicite pour filtre
    let periodicite = Projet.getPeriodicite(trProjet.id);
    let btnsAfficher= document.querySelectorAll('.boutons_projet_'+periodicite+'_'+idPj);
    let visibility = 'visible';
    //recharge les sous-projet à chaque demande d'affichage
    if (trProjet.dataset.visibility === 'hidden')
    {
        afficherSpj(idPj,periodicite).then((nbSousProjets)=>{

            if (nbSousProjets>1)
            {
                // on affiche icon pour trie uniquement si plus de 1 sous-projet
                btnsAfficher.forEach(function (btn) {
                    btn.style.visibility=visibility;
                })
            }else{
                let btnAdd = document.getElementById('add_spj_'+periodicite+'_'+idPj);
                btnAdd.style.visibility=visibility;
            }
    });

    }else{
        let trSousProjets = document.querySelectorAll('.spj_'+periodicite+'_'+idPj);
        trSousProjets.forEach(function (trSpj) {
            trSpj.remove();
        });
        visibility="hidden";
        btnsAfficher.forEach(function (btn) {
            btn.style.visibility=visibility;
        })
    }

    trProjet.dataset.visibility=visibility;



}


/**Creer et complete le tableau des sous projets
 *
 * @param sousProjets
 * @param tdSpj
 * @param idProjet
 */
 function afficherSousProjets(sousProjets,tdSpj,idProjet) {
        let table = document.createElement("table");
        table.setAttribute('class',"sousProjet table table-sm table-borderless");

        table.appendChild(getHeadSpj(idProjet));
        table.appendChild(getBodySpj(sousProjets,idProjet));

        tdSpj.appendChild(table);
 }

 export function getTrSousProjet(sousProjet,idProjet,periodicite)
{
    let tr = document.createElement("tr");
    tr.setAttribute('id','ligne_spj_'+sousProjet.id);
    tr.setAttribute('class','spj_'+sousProjet.periodicite+'_'+idProjet);
    tr.setAttribute('data-id-projet',idProjet);
    tr.setAttribute('data-id-sous-projet',sousProjet.id);
    tr.setAttribute('data-periodicite',periodicite);
    tr.setAttribute('data-previsionnel',sousProjet.isPrevisionnel);
    let td = document.createElement("td");
    td.colSpan=5;
    td.id=sousProjet.id+'_spj_pj_'+sousProjet.periodicite+'_'+idProjet;
    let table = document.createElement("table");
    table.setAttribute('class',"sousProjet table table-sm table-borderless");
    if (sousProjet.isPrevisionnel)
    {
        table.classList.add('previsionnel');
    }
    tr.appendChild(td);
    td.appendChild(table);
    table.appendChild(getHeadSpj(idProjet));
    table.appendChild(getBodySpj(sousProjet,idProjet,periodicite));
    return tr;
}


/**Construits les lignes des independants
 *
 * @returns {[]}
 * @param sousProjet
 */


function getLignesHeures(sousProjet,periodicite,idProjet)
{
    // recuperation de la listes des heures du soous projet
    let heures = sousProjet.heures;
    //tableau qui va contenir toutes les lignes
    let tabTr = [];
    let trIndeHead =Heure.getTrHeadHeure(sousProjet.id);
    tabTr.push(trIndeHead);
    //si il n'y a pas d'heures ratacher au projet
    if (heures.length > 0)
    {
        //meme traitement pour chaque heure
        heures.forEach(function(heure)
        {
            let independant = heure.independant;

            let trInde = document.createElement('tr');
            trInde.id='heure_'+heure.id;
            trInde.setAttribute('class','compl_spj_'+sousProjet.id+' heure');
            trInde.setAttribute('data-id-sous-projet',sousProjet.id);
            trInde.setAttribute('data-id-inde',independant.id);
            //colonne du nom de l'independant
            let tdFullName = document.createElement("td");
            tdFullName.colSpan=2;
            tdFullName.id='full_name_heure_'+heure.id;
            tdFullName.innerText= Utils.capitalize(independant.prenom) + " "+Utils.capitalize(independant.nom);
            tdFullName.setAttribute('data-id-inde',independant.id);
            //colonne nombre d'heures
            let tdHeure = document.createElement("td");
            tdHeure.id='heure_heure_'+heure.id;
            tdHeure.setAttribute('data-nb-heures',heure.heure);
            tdHeure.innerText=heure.heure+' H';
            tdHeure.className='heures_inde_'+periodicite+'_'+idProjet+'_'+independant.id;
            //colonne du cout
            let tdCout = document.createElement("td");
            tdCout.innerText = Utils.formatEuro(heure.cout);
            tdCout.setAttribute('data-cost',heure.cout);
            tdCout.setAttribute('data-salaire',heure.independant.salaire);
            tdCout.id='cout_heure_'+heure.id;
            tdCout.classList.add('money-text');
            // colonne commentaire
            let tdCommentaire = document.createElement("td");
            tdCommentaire.id = 'commentaire_heure_'+heure.id;
            tdCommentaire.setAttribute('data-commentaire',heure.commentaire);
            tdCommentaire.innerText = heure.commentaire;
            tdCommentaire.className='commentaire_heure_';

            let tdBtns = document.createElement('td');
            tdBtns.setAttribute("colspan",2);
            let spanBtn = document.createElement('span');
            spanBtn.id ="btns_heure_"+heure.id;

            let btnMdf = Utils.getBtnMdf();
            btnMdf.setAttribute('class','btn btn-mdf button');
            btnMdf.setAttribute('id','mdf_heure_'+heure.id);
            btnMdf.addEventListener("click",Heure.formUpdateHeure);

            let btnSup = Utils.getBtnSup();
            btnSup.setAttribute('class','btn btn-sup button');
            btnSup.setAttribute('id','sup_heure_'+heure.id);
            btnSup.addEventListener("click",Heure.validSup);
            spanBtn.appendChild(btnMdf);
            spanBtn.appendChild(btnSup);
            tdBtns.appendChild(spanBtn);

            if (visiPrix.checked===true){
            trInde.appendChild(tdFullName);
            trInde.appendChild(tdHeure);
            trInde.appendChild(tdCout).style.visibility='hidden';
            trInde.appendChild(tdCommentaire);
            trInde.appendChild(tdBtns).style.display='none';
            tabTr.push(trInde);
        }
        else {
            trInde.appendChild(tdFullName);
            trInde.appendChild(tdHeure);
            trInde.appendChild(tdCout);
            trInde.appendChild(tdCommentaire);
            trInde.appendChild(tdBtns);
            tabTr.push(trInde);
        }


        })
    }
    return tabTr;
}

/**Construit les lignes des prestations
 *
 * @param sousProjet
 * @returns {[]}
 */


function getLignesPrestation(sousProjet)
{
    let prestations = sousProjet.autrePrestations;
    let tabTr = [];
    // en tete

    let trPrestaHead = Prestations.getTrPrestaHead(sousProjet.id);
    tabTr.push(trPrestaHead);
    if (prestations.length>0)
    {

        prestations.forEach(function(prestation)
        {
            // meme action pour chaque prestation
            let trPresta = document.createElement('tr');
            trPresta.id='presta_'+prestation.id;
            trPresta.setAttribute('class','compl_spj_'+sousProjet.id);
            trPresta.setAttribute('data-id-sous-projet',sousProjet.id);
            //cellule detail
            let tdDetail = document.createElement("td");
            tdDetail.colSpan=2;
            tdDetail.innerText= prestation.detail;
            tdDetail.id='detail_presta_'+prestation.id;
            //cellule montant
            let tdMnt = document.createElement("td");
            tdMnt.id='mnt_presta_'+prestation.id;
            if(isAdmin === "true"){
            tdMnt.innerText = Utils.formatEuro(prestation.montant);
            tdMnt.setAttribute('data-cost',prestation.montant);}
            tdMnt.classList.add('money-text');
            //cellules des boutons
            let tdBtns = document.createElement('td');
            tdBtns.setAttribute("colspan",2);
            let spanBtn = document.createElement('span');
            spanBtn.id="btns_presta_"+prestation.id;
            //boutons modifier prestation
            let btnMdf = Utils.getBtnMdf(Prestations.modifierPresta);
            btnMdf.setAttribute('id','mdf_presta_'+prestation.id);
            //boutons supprimer prestation
            let btnSup = Utils.getBtnSup(Prestations.validSupPresta);
            btnSup.setAttribute('id','sup_presta_'+prestation.id);

            spanBtn.appendChild(btnMdf);
            spanBtn.appendChild(btnSup);
            tdBtns.appendChild(spanBtn);

            if(visiPrix.checked===true ){

            trPresta.appendChild(tdDetail);
            trPresta.appendChild(tdMnt).style.display='none';
            trPresta.appendChild(tdBtns).style.display='none';
            tabTr.push(trPresta);
            }
            else{

                trPresta.appendChild(tdDetail);
                trPresta.appendChild(tdMnt);
                trPresta.appendChild(tdBtns);
                tabTr.push(trPresta);

            }
        })
    }
    return tabTr;
}

/**Construit une ligne de sousProjet
 *
 * @param sousProjet
 * @returns {HTMLTableRowElement}
 */


function getLigneSousProjet(sousProjet)
{
    let trSpj = document.createElement('tr');
    trSpj.id='sous_projet_'+sousProjet.id;


    //cellule designation du sous projet
    let tdDesignation = document.createElement("td");
    tdDesignation.colSpan=2;
    tdDesignation.innerText=sousProjet.designation.toUpperCase();
    tdDesignation.id='design_spj_'+sousProjet.id;


    //cellule date du sous projet
    let tdDate = document.createElement("td");
    let date = new Date(sousProjet.dateAction.date);


    tdDate.id='date_spj_'+sousProjet.id;
    let month =date.getMonth()+1;
    if (month<10)
    {
        month = '0'+month;
    }
    tdDate.innerText=month+'/'+date.getFullYear();
    tdDate.setAttribute('data-month',month);
    tdDate.setAttribute('data-year',date.getFullYear());
    //cellule categorie du sous projet
    let tdCat = document.createElement("td");
    tdCat.innerText=sousProjet.categorie.libelle;
    tdCat.id='categorie_spj_'+sousProjet.id;

    //cellule prix de vente du sous projet
    let tdPV = document.createElement("td");
    tdPV.setAttribute('data-prix-vente',sousProjet.prixVente);
    tdPV.setAttribute('data-previsionnel',sousProjet.isPrevisionnel);
    //data pour suivis categories
    tdPV.setAttribute('data-categorie',sousProjet.categorie.id);
    tdPV.innerText= Utils.formatEuro(sousProjet.prixVente);
    tdPV.id='pv_spj_'+sousProjet.id;
    tdPV.classList.add('money-text');

    //cellule du roi
    let tdROI = document.createElement("td");
    tdROI.id='roi_spj_'+sousProjet.id;
    tdROI.classList.add('roi');
    tdROI.classList.add('percent-text');
    let roi = Utils.calculRoi(sousProjet.cout,sousProjet.prixVente);
    Utils.setClassRoi(roi,tdROI)
    //initialisation a zero

    tdROI.innerText=Utils.formatPercent(roi);

    //cellule des boutons
    let tdBtns = document.createElement("td");
    tdBtns.setAttribute("colspan",2);
    let spanBtn = document.createElement('span');
    spanBtn.id = 'btns_sous_projet_'+sousProjet.id;

    //bouton modifier sous projet
    let btnMdf = Utils.getBtnMdf();
    btnMdf.setAttribute('id','mdf_spj_'+sousProjet.id);
    btnMdf.addEventListener("click",SousProjet.formUpdtSpj);

    //bouton supprimer sous projet
    let btnSup = Utils.getBtnSup();
    btnSup.setAttribute('id','sup_spj_'+sousProjet.id);
    btnSup.addEventListener("click",SousProjet.validerSupSpj);
    

    spanBtn.appendChild(btnMdf);
    spanBtn.appendChild(btnSup);
    tdBtns.appendChild(spanBtn);

    document.getElementById('visiPrix');

    if (visiPrix.checked === true)
    {
        trSpj.appendChild(tdDesignation);
        trSpj.appendChild(tdDate);
        trSpj.appendChild(tdCat);
        trSpj.appendChild(tdPV).style.display='none';
        trSpj.appendChild(tdROI).style.display='none';
        trSpj.appendChild(tdBtns).style.display='none';
    }
    else
    {
        trSpj.appendChild(tdDesignation);
        trSpj.appendChild(tdDate);
        trSpj.appendChild(tdCat);
        trSpj.appendChild(tdPV);
        trSpj.appendChild(tdROI);
        trSpj.appendChild(tdBtns);
    }

    return trSpj;
}


/**Construit le header du tableau des sous-projets
 *
 */


export function getHeadSpj(idProjet)
{
    let thead = document.createElement("thead");
    let trTH = document.createElement('tr');
    thead.id= 'thead_spjs_projet_'+idProjet;
    thead.classList.add('head-spj');
    trTH.id = 'tr_spjs_projet_'+idProjet;
    //cellule ajout sous-projet
    let thIconAdd = document.createElement('th');
    thIconAdd.colSpan = 2;
    thIconAdd.innerText="Désignation";
    // thIconAdd.className='col-3';
    let thDate =document.createElement("th");
    thDate.innerText='Date';
    // thDate.className='col-3';
    //cellule categorie
    let thCat = document.createElement("th");
    thCat.innerText="Catégorie";
    // thCat.className='col-1';
    //cellule prix de vente
    let thPV = document.createElement("th");
    document.getElementById('visiPrix');
    thPV.innerText="Prix de Vente";
    // thPV.className='col-2';
    // cellule roi
    let thROI = document.createElement("th");
    thROI.innerText="ROI";
    // thROI.className='col-1';
    let thVide = document.createElement("th");
    document.getElementById('visiPrix');
    // thVide.className='col';
    //imbrication des elements
    trTH.appendChild(thIconAdd);
    trTH.appendChild(thDate);
    trTH.appendChild(thCat);
    trTH.appendChild(thPV);
    trTH.appendChild(thROI);
    trTH.appendChild(thVide);
    thead.appendChild(trTH);
    //renvoie le thead construit
    return thead;
}

/**Construit le body du tableau des sous projets
 *
 * @param sousProjets
 * @param idProjet
 * @returns {HTMLTableSectionElement}
 */

function getBodySpj(sousProjet,idProjet,periodicite) {
    //creation du body
    let tbody = document.createElement("tbody");
    tbody.id = 'tbody_spjs_'+sousProjet.id;

        //recuperation des lignes
        let trSpj =getLigneSousProjet(sousProjet);
        let tabTrPresta = getLignesPrestation(sousProjet);
        let tabtrInde = getLignesHeures(sousProjet,periodicite,idProjet);

        //ajout des lignes au tbody
        tbody.appendChild(trSpj);
        tabTrPresta.forEach(function (trPresta) {
            tbody.appendChild(trPresta);
        })


        tabtrInde.forEach(function (trInde) {
            tbody.appendChild(trInde);
        })
    // })
        return tbody;
}

export function afficherSpj(idPj,periodicite){
    return new Promise((resolve, reject) => {
        let trSousProjets = document.querySelectorAll('.spj_'+periodicite+'_'+idPj);
        //lignes des formulaires pour ajout d'un sous-projet
        if (trSousProjets.length > 0)
        {
            trSousProjets.forEach(function (tr) {
                tr.remove();
            })
        }
        let filtres = Filtre.recupererFiltresEnCours();

        //On affiche les sous-projets sous les formulaires d'ajout de sous-projets possiblement present
        Spj.loadSousProjetByFiltre(idPj,periodicite,filtres).then((sousProjets)=>{
            let pj = Utils.getProjetElement(idPj,periodicite);
            let trPj = pj.trPj;
            let nbSousProjets = sousProjets.length;
            let visibility = trPj.dataset.visibility;
            trPj.dataset.nbSousProjets=nbSousProjets;
                sousProjets.forEach(function(sousProjet)
                {
                    let trSpj = getTrSousProjet(sousProjet,idPj,periodicite);
                    if (visibility === 'hidden')
                    {
                        trSpj.style.display='none';
                    }
                    trPj.insertAdjacentElement("afterend",trSpj);
                    Refresh.refreshCostSpjOnDom(sousProjet.id);
                });
            Filtre.needDisplayNone();
            userSup();
            resolve(nbSousProjets);
        })
    })
}

function afficherElement(event) {
    event.preventDefault();
    let target = event.currentTarget;
    let colonne = null;
    let autreColonne;
    let divGeneral = document.getElementById("div-general");
    let divCreate = document.getElementById(('create-projet'));
    //on supprime la fiche creation projet si presente
    if (divCreate !== null)
    {
        divCreate.className="";
        divCreate.innerText="";
        let btnAdd = document.getElementById('add_projet');
        btnAdd.style.display="";
        document.getElementById('creation-projet').style.display='none';
    }
    if (target.id === 'afficher-filtres') {
        colonne = document.getElementById('les-filtres');
        autreColonne = document.getElementById('les-infos');

    } else if (target.id === "afficher-infos") {
        colonne = document.getElementById('les-infos');
        autreColonne = document.getElementById('les-filtres');
    }
    if (colonne !== null) {
        let display = colonne.style.display;
        switch (display) {
            case 'none':
                display = '';
                divGeneral.style.display = 'none';
                break;
            case '':
                display = 'none'
                divGeneral.style.display = '';

                break;
        }
        colonne.style.display = display;
        autreColonne.style.display = 'none';
    }
}


//Suppression des elements si role_user

console.log('userSup');
export function userSup(event)
{
    if(isAdmin === "false")
    {
        console.log('isAdmin=false');
        document.getElementById('global-perio-non-previ').style.visibility="hidden";
        document.getElementById('global-cate').style.display="none";
        document.getElementById('global-perio').style.visibility="hidden";
        document.getElementById('ratio-periode').style.display="none";
        document.getElementById('pv-periode').style.display="none";
        //document.getElementById('filtres').style.display="none";
        document.getElementsByTagName('button').forEach(button => {
         if (button.id !== 'filtrer' && button.id !== '3months') button.style.display = "none"
                    });

        var percentList = document.getElementsByClassName('percent-text')
        var i;
        for (i = 0; i < percentList.length; i++) {
            percentList[i].style.display = 'none';
        }
        if(isUser === "false")
        {
        var moneyList = document.getElementsByClassName('money-text');
        var i;
        for (i = 0; i < moneyList.length; i++) {
            moneyList[i].style.display = '';
        }
        }   
        else{
            var moneyList = document.getElementsByClassName('money-text');
            var i;
            for (i = 0; i < moneyList.length; i++) {
            moneyList[i].style.display = 'none';
        }
        }
    }
    else
    {
        console.log('isAdmin=true');
        let visiPrix = document.getElementById('visiPrix');
        if(visiPrix.checked===false){
        document.getElementById('global-perio-non-previ').style.display="";
        document.getElementById('global-cate').style.display="";
        document.getElementById('global-perio').style.display="";
        document.getElementById('ratio-periode').style.display="";
        document.getElementById('pv-periode').style.display="";


        var percentList = document.getElementsByClassName('percent-text')
        var i;
        for (i = 0; i < percentList.length; i++) {
            percentList[i].style.display = '';
        }
        var moneyList = document.getElementsByClassName('money-text');
        var i;
        for (i = 0; i < moneyList.length; i++) {
            moneyList[i].style.display = '';
        }
        }
    }
}
