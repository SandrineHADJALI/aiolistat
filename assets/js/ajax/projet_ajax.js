import * as Ajax from './rest_ajax';
export function loadOne(idPj)
{
    return new Promise((resolve, reject) => {
        let url ='/projet/'+idPj;
        let method='GET';
        Ajax.loadOne(url,method).then(
            (projet)=>{
            resolve(projet);
        },
            (erreurs)=>{
                reject(erreurs);
            });
    })
}

export function removePj(idPj)
{ return new Promise((resolve, reject) => {
    let url ='/projet/delete/'+idPj;
    let method='POST';
    Ajax.remove(url,method).then(
        ()=>{
        resolve();
    },
        (erreurs)=>{
            reject(erreurs);
        });
})}

export function updatePJ(projet)
{
    return new Promise((resolve, reject) => {
        let url = '/projet/update';
        let method='POST';
        Ajax.update(url,method,projet).then(
            ()=>{
            resolve();
        },
            (erreurs)=>{
                reject(erreurs);
            });
    })
}


export function insertProjet(projet)
{
    return new Promise((resolve, reject) => {
        let url = '/projet/insert';
        let method = 'POST'
        Ajax.insert(url,method,projet).then((idProjet)=>{
           resolve(idProjet);
        });
    })
}


export function getProjetsVide()
{
    return new Promise((resolve, reject) => {
        let url = '/projet/vide';
        let method = 'GET'
        Ajax.loadList(url,method).then(
            (listeVide)=>{
            resolve(listeVide);
        },
            (erreurs)=>{
                reject(erreurs);
            })
    })
}



export function getProjetByFiltres(filtres)
{
    return new Promise((resolve, reject) => {
        let url = '/projet/filtre';
        let method = 'POST';
        Ajax.loadList(url,method,filtres).then(
            (projetsList)=>{
            resolve(projetsList);
        },
            (erreurs)=>{
                reject(erreurs);
            }) ;
    });
}

export function loadOneByFiltres(filtres,idProjet)
{
    return new Promise((resolve, reject) => {
        let url = '/projet/filtre/'+idProjet;
        let method = 'POST';
        Ajax.loadList(url,method,filtres).then(
            (projetsList)=>{
            resolve(projetsList);
        },
            (erreurs)=>{
                reject(erreurs);
            }) ;
    });
}