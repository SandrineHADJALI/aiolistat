import * as Ajax from './rest_ajax';
export function addHeure(heure)
{
    return new Promise((resolve, reject) => {

        let method = 'POST';
        let url = '/heure/add';
        Ajax.insert(url,method,heure).then(
            (idHeure)=>{
                resolve(idHeure);
            },
            (erreurs)=>{
                reject(erreurs);
            }
        )
    });
}


export function removeHeure(idHeure)
{
    let url = '/heure/delete/'+idHeure;
    let method='GET'

    return new Promise((resolve, reject) => {
        Ajax.remove(url,method)
            .then(
                ()=>{
                    resolve();
                }
                ,
                (erreurs)=>{
                    reject(erreurs);
                    }
                )
    });
}



export function updateHeure(heure)
{
    return new Promise((resolve, reject) => {
        let url = '/heure/update';
        let method='POST';

        Ajax.update(url,method,heure).then(
            ()=>{
                resolve();
        },
            (erreurs)=>{
            reject(erreurs);
        });
    });

}

export function loadheure(idheure)
{
     return new Promise((resolve, reject) => {
         let method= 'GET';
         let url = '/heure/'+idheure;

         Ajax.loadOne(url,method).then(
             (heure)=>{
                 resolve(heure);
             }),
             (erreurs)=>{
                 reject(erreurs);
             }

     });
}