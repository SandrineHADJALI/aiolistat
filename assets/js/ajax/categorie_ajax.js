import * as Ajax from './rest_ajax';


/**Recupere la liste de scategories en base
 *
 * @returns {Promise<unknown>}
 */
export function loadCategories()
{
    return new Promise((resolve, reject) => {
        let url="/categorie/list";
        let method ='GET';
        Ajax.loadList(url,method).then((categories)=>{
            resolve(categories);
        },
            (erreurs)=>{
            reject(erreurs);
            })
    })
}