/*TRIGGER SUR heure*/
CREATE TRIGGER `after_update_heure` 
	AFTER UPDATE ON `heure`
		FOR EACH ROW 
 	UPDATE sous_projet 
		set cout=cout+NEW.cout - OLD.cout
    			where id=OLD.sous_projet_id;
    
CREATE TRIGGER `after_delete_heure` 
	AFTER DELETE ON `heure`
		FOR EACH ROW 
 	UPDATE sous_projet 
		set cout=cout - OLD.cout
    			where id=OLD.sous_projet_id;  

    
CREATE TRIGGER `after_insert_heure` 
	AFTER INSERT ON `heure`
		FOR EACH ROW 
 	UPDATE sous_projet 
		set cout=cout+NEW.cout
    			where id=NEW.sous_projet_id;  
    			
    			  			
    			
/*TRIGGER SUR autre_prestation*/			
CREATE TRIGGER `after_update_autre_prestation` 
	AFTER UPDATE ON `autre_prestation`
		FOR EACH ROW 
 	UPDATE sous_projet 
		set cout=cout+NEW.montant - OLD.montant
    			where id=OLD.sous_projet_id;
    
CREATE TRIGGER `after_delete_autre_prestation` 
	AFTER DELETE ON `autre_prestation`
		FOR EACH ROW 
 	UPDATE sous_projet 
		set cout=cout - OLD.montant
    			where id=OLD.sous_projet_id;  

    
CREATE TRIGGER `after_insert_autre_prestation` 
	AFTER INSERT ON `autre_prestation`
		FOR EACH ROW 
 	UPDATE sous_projet 
		set cout=cout + NEW.montant
    			where id=NEW.sous_projet_id;